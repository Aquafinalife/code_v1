$(window).load(function() {

    gototop();

});

function gototop()
{
    $(".gotop").hide();
    $(window).scroll(function() {
        if ($(this).scrollTop() > 10) {
            $('.gotop').fadeIn();
        } else {
            $('.gotop').fadeOut();
        }
    });
    $('.gotop').click(function() {
        $('body,html').animate({scrollTop: 4}, 300);
        return false;
    });
}

function imagebox()
{
    $('.imagebox').fancybox({
         'autoSize' : false
    });
}

function fancyshow(id)
{
    $(".ft-"+id).fancybox({
        loop        : true,
        autoPlay    : true,
        playSpeed   : 4000,
        nextSpeed   : 500,
        prevSpeed   : 500,
        openSpeed   : 500,
        speedOut    : 500,
        openEffect  : "fade", 
        closeEffect : "fade",
        prevEffect	: 'fade', //fade none
        nextEffect	: 'fade', //fade none
        helpers	: {
            title	: {
                type: 'inside'
            },
            thumbs	: {
                width	: 50,
                height	: 50
            }
        }
    });
}

function update_cart(id,uri)
{
    var quantity = $('input[name=' + id + ']').val();
    $.ajax(
    {
        type: 'post',
        url: '/updatecart',
        data: {
            'id': id,
            'quantity': quantity,
        },
        success: function()
        {
            location.href = uri;
        }
    });
}

function remove_cart(row_id,uri,lang)
{
    if(lang == 'vi'){
        question = confirm("Bạn có muốn xóa?");
    }else{
        question = confirm("Are you sure you want delete?");
    }
    if (question)
    {
        $.ajax(
        {
            type: 'post',
            url: '/removecart',
            data: {
                //'is-ajax': 1,
                'id': row_id,
            },
            success: function(responseText)
            {
                location.href = uri;
            }
        });
    }
    else
    {
        return false;
    }
}

function sort_by()
{
    var sortby = $("select[name=sortby]").val();
    var redirect_url = $("input[name=redirect_url]").val();
    $.ajax({
        type:'post',
        url : '/products_sort_by',
        data:{
            'sortby' : sortby
        },
        success: function()
        {
            location.href = redirect_url;
        }
    });
}

function filter_add(attr,val)
{
    var redirect_url = $("input[name=redirect_url]").val();
    $.ajax({
        type:'post',
        url : '/products_filter',
        data:{
            'filter_attr' : attr,
            'filter_val'  : val
        },
        success: function()
        {
            location.href = redirect_url;
        }
    });
}

function filter_clear(attr)
{
    var redirect_url = $("input[name=redirect_url]").val();
    $.ajax({
        type:'post',
        url : '/products_filter_clear',
        data:{
            'filter_attr' : attr
        },
        success: function()
        {
            location.href = redirect_url;
        }
    });
}

function select_size()
{
    if($('span.sizebox').hasClass('sizebox_selected')) {
        $('span.sizebox').removeClass('sizebox_selected');
    }
}

function get_newsletter()
{
    var email = $('input[name=email]').val();
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
//    alert(email);return;
    if(email == '')
    {
        $('#error').html('Xin mời điền email của bạn vào đây');
    }
    else if(!emailReg.test(email))
    {
        $('#error').html('Email bạn điền không đúng');
    }
    else
    {
        $.ajax(
        {
            type:   'post',
            url:    '/news_letter',
            data:   {
                'email'        : email
            },
            success: function(responseText)
            {
                $('.news_letter').html(responseText);
                $('#error').html('Bạn đã đăng ký thành công');
            }
        });
    }
}

(function($){
    
    $('.addtocart').click(function(){
        var id = $('input[name=id]').val();
        var addUri = $('input[name=addUri]').val();
        var lang = $('input[name=lang]').val();
        var size = $('span.sizebox_selected').attr('title');
        $.ajax(
        {
            type:'post',
            url : '/addtocart',
            data:{
                'id' : id,
                'size' : size
            },
            beforeSend: function(){
                $('.addtocart').prop('disabled', true);
                if(lang == 'vi'){
                    $('.addtocart').html("Đang thêm vào giỏ...");
                }else{
                    $('.addtocart').html("Please wait...");
                }
            },
            success: function(data)
            {
                location.href = addUri;
            }
        });
    });

    jQuery(".partner_inner").owlCarousel({
        navigation : true,
        items: 8,
        itemsCustom: [[0, 1], [479, 2], [768, 5], [979, 8], [1199, 8], [1688, 8]],
        slideSpeed: 1000,
        rewindSpeed: 2000,
        paginationSpeed: 1000,
        autoPlay: 5000,
        pagination: false
    });
    
    jQuery(window).on('load', function(){
        $('.trademark').masonry({
            itemSelector: '.trademark_item',
            //columnWidth: 120
        });
    })

//    if($('span.sizebox').hasClass('sizebox_selected')) {
//        var size = $('span.sizebox_selected').attr('title');
//        $('input[name="size_selected"]').val(size);
//    }

})(jQuery);
