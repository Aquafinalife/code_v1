<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('url_title'))
{
    function url_title($str, $separator = 'dash', $lowercase = FALSE)
    {
        $separator = ($separator == 'dash') ? '-' : '_';
        $str = strip_tags($str);
        $from = array(      '—', 'À', 'Á', 'Ả', 'Ã', 'Ạ', 'Â', 'Ầ', 'Ấ', 'Ẩ', 'Ẫ', 'Ậ', 'Ă', 'Ằ', 'Ắ', 'Ẳ', 'Ẵ', 'Ặ', 'à', 'á', 'ả', 'ã', 'ạ', 'â', 'ầ', 'ấ', 'ẩ', 'ẫ', 'ậ', 'ă', 'ằ', 'ắ', 'ẳ', 'ẵ', 'ặ'
                            , 'È', 'É', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê', 'Ề', 'Ế', 'Ể', 'Ễ', 'Ệ', 'è', 'é', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ề', 'ế', 'ể', 'ễ', 'ệ'
                            , 'Ì', 'Í', 'Ỉ', 'Ĩ', 'Ị', 'ì', 'í', 'ỉ', 'ĩ', 'ị'
                            , 'Ò', 'Ó', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ồ', 'Ố', 'Ổ', 'Ỗ', 'Ộ', 'ò', 'ó', 'ỏ', 'õ', 'ọ', 'ô', 'ồ', 'ố', 'ổ', 'ỗ', 'ộ'
                            , 'Ơ', 'Ờ', 'Ớ', 'Ở', 'Ỡ', 'Ợ', 'ơ', 'ờ', 'ớ', 'ở', 'ỡ', 'ợ'
                            , 'Ù', 'Ú', 'Ủ', 'Ũ', 'Ụ', 'ù', 'ú', 'ủ', 'ũ', 'ụ'
                            , 'Ư', 'Ừ', 'Ứ', 'Ử', 'Ữ', 'Ự', 'ư', 'ừ', 'ứ', 'ử', 'ữ', 'ự'
                            , 'Ỳ', 'Ý', 'Ỷ', 'Ỹ', 'Ỵ', 'ỳ', 'ý', 'ỷ', 'ỹ', 'ỵ'
                            , 'Đ', 'đ'
                            , '(', ')', '[', ']', '{', '}', '~', '`', '!', '@'
                            , '#', '$', '%', '^', '&', '*', '-', '_', '+', '='
                            , '|', '\\', ':', ';', '"', '\'', '<', '>', ',', '.'
                            , '/', '?', '“', '”'
                            , '     ', '    ', '   ', '  ', ' '
                    );
        $to =   array(      ' ', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'
                            , 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e'
                            , 'I', 'I', 'I', 'I', 'I', 'i', 'i', 'i', 'i', 'i'
                            , 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'
                            , 'O', 'O', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o', 'o', 'o'
                            , 'U', 'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u', 'u'
                            , 'U', 'U', 'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u', 'u', 'u'
                            , 'Y', 'Y', 'Y', 'Y', 'Y', 'y', 'y', 'y', 'y', 'y'
                            , 'D', 'd'
                            , '', '', '', '', '', '', '', '', '', ''
                            , '', '', '', '', '', '', '', '', '', ''
                            , ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '
                            , ' ', ' ', ' ', ' '
                            , ' ', ' ', ' ', ' ', $separator
                    );
        $str            = trim(str_replace($from, $to, $str));

        // remove disallowed characters
        $pattern        = '/[^A-Za-z 0-9~%.:_\-]/i';
        $replacement    = '';
        $str            = preg_replace($pattern, $replacement, $str);
        
        if ($lowercase === TRUE)
          $str = strtolower($str);
        return $str;
    }
}

if(!function_exists('current_full_url'))
{
    function current_full_url()
    {
        $CI =& get_instance();

        $url = $CI->config->site_url($CI->uri->uri_string());
        return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
    }
}

if (!function_exists('get_a_customers_type')) {

    function get_a_customers_type() {
        $a = array(
            CANHAN => 'Cá nhân',
            DOANHNGHIEP => 'Doanh nghiệp',
        );
        return $a;
    }

}
if (!function_exists('get_customers_type')) {

    function get_customers_type($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case CANHAN:
                $output = 'Cá nhân';
                break;
            case DOANHNGHIEP:
                $output = 'Doanh nghiệp';
                break;
            default:
                $output = 'Cá nhân';
                break;
        }
        return $output;
    }
}

if (!function_exists('get_customers_type1')) {

    function get_customers_type1($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case VIP:
                $output = 'Vip';
                break;
            case THUONG:
                $output = 'Thường';
                break;
            default:
                $output = 'Thường';
                break;
        }
        return $output;
    }
}

if (!function_exists('get_gender')) {

    function get_gender($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case NAM:
                $output = 'Nam';
                break;
            case NU:
                $output = 'Nữ';
                break;
            case KHAC:
                $output = 'Khác';
                break;
            default:
                $output = 'Khác';
                break;
        }
        return $output;
    }
}

if (!function_exists('get_ma_benh')) {

    function get_ma_benh($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case 'TV_TTTBVV_TN':
                $output = 'Tử vong, TTTBVV(Do tai nạn)';
                break;
            case 'TCL_NN':
                $output = 'Trợ cấp lương ngày nghỉ';
                break;
            case 'CPYT_TN':
                $output = 'Chi phí y tế do tai nạn';
                break;
            case 'TV_TTTBVV_OB':
                $output = 'Tử vong, TTTBVV (Do ốm bệnh)';
                break;
            case 'DTNT_OB':
                $output = 'Điều trị nội trú (Do ốm bệnh)';
                break;
            case 'DTNGT_OB':
                $output = 'ĐIỀU TRỊ NGOẠI TRÚ (Do ốm bệnh)';
                break;
            case 'THAISAN_QLNT':
                $output = 'THAI SẢN (Trong quyền lợi nội trú)';
                break;
            case 'dtnt_cppt':
                $output = 'Chi phí phẫu thuật';
                break;
//            case 'TCL_NN':
//                $output = 'Trợ';
//                break;
//            case 'TCL_NN':
//                $output = 'Trợ';
//                break;
//            case 'TCL_NN':
//                $output = 'Trợ';
//                break;
//            case 'TCL_NN':
//                $output = 'Trợ';
//                break;
//            case 'TCL_NN':
//                $output = 'Trợ';
//                break;
//            case 'TCL_NN':
//                $output = 'Trợ';
//                break;
//            case 'TCL_NN':
//                $output = 'Trợ';
//                break;
//            case 'TCL_NN':
//                $output = 'Trợ';
//                break;
//            case 'TCL_NN':
//                $output = 'Trợ';
//                break;
//            case 'TCL_NN':
//                $output = 'Trợ';
//                break;
            default:
                $output = 'Tử vong, TTTBVV(Do tai nạn)';
                break;
        }
        return $output;
    }
}

if (!function_exists('get_bt_status')) {

    function get_bt_status($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case DA_BOI_THUONG:
                $output = 'Đã bồi thường';
                break;
            case CHO_DUYET:
                $output = 'Đang chờ duyệt';
                break;
            case KHAC:
                $output = 'Khác';
                break;
            default:
                $output = 'Đang chờ duyệt';
                break;
        }
        return $output;
    }
}
if (!function_exists('get_ndbh_quanhe')) {

    function get_ndbh_quanhe($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case BANTHAN:
                $output = 'Bản thân';
                break;
            case NGUOITHAN:
                $output = 'Người thân';
                break;
            case NHANVIEN:
                $output = 'Nhân viên';
                break;
            default:
                $output = 'Người thân';
                break;
        }
        return $output;
    }
}
if (!function_exists('get_qlbh')) {

    function get_qlbh($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case NOITRU:
                $output = 'Nội trú';
                break;
            case NGOAITRU:
                $output = 'Ngoại trú';
                break;
            default:
                $output = 'Khác';
                break;
        }
        return $output;
    }
}
if (!function_exists('get_phuongthuc_bt')) {

    function get_phuongthuc_bt($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case BANTHAN:
                $output = 'Trường hợp hoàn trả';
                break;
            case NGUOITHAN:
                $output = 'Bảo lãnh viện phí';
                break;            
            default:
                $output = 'Bảo lãnh viện phí';
                break;
        }
        return $output;
    }
}
if (!function_exists('get_hinhthuc_nhan_bt')) {

    function get_hinhthuc_nhan_bt($int = '') {
        $output = $output1 = '';
        switch ($int) {
            case TIEN_MAT:
                $output = 'Tiền mặt';
                break;
            case CHUYEN_KHOAN:
                $output = 'Chuyển khoản';
                break;            
            default:
                $output = 'Tiền mặt';
                break;
        }
        return $output;
    }
}

/* End of file PIXELPLANT_url_helper.php */
/* Location: ./application/helpers/PIXELPLANT_url_helper.php */ 