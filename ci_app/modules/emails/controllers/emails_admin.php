<?php

class Emails_Admin extends MY_Controller {

//    function __construct() {
//        parent::__construct();
//        if (!modules::run('auth/auth/has_permission', array('operation' => OPERATION_MANAGE)))
//            redirect('/');
//    }
    function __construct()
    {
        parent::__construct();
        modules::run('auth/auth/validate_login',$this->router->fetch_module());
        $this->_layout = 'admin_ui/layout/main';
        $this->_view_data['url'] = EMAILS_ADMIN_BASE_URL;
    }
    
    /**
     * @desc: Hien thi danh sach cac email
     * 
     * @param type $options 
     */
    function browse($para1=DEFAULT_LANGUAGE, $para2=1)
    {
        $options            = array('lang'=>switch_language($para1),'page'=>$para2);
        //$options            = array_merge($options, $this->_get_data_from_filter());
        $options['emails_sort_type'] = $this->phpsession->get('emails_sort_type');
        $this->phpsession->save('emails_lang', $options['lang']);

        $total_row          = $this->emails_model->get_emails_count($options);
        $total_pages        = (int)($total_row / NEWS_ADMIN_POST_PER_PAGE);
        if ($total_pages * NEWS_ADMIN_POST_PER_PAGE < $total_row) $total_pages++;
        if ((int)$options['page'] > $total_pages) $options['page'] = $total_pages;

        $options['offset']  = $options['page'] <= DEFAULT_PAGE ? DEFAULT_OFFSET : ((int)$options['page'] - 1) * NEWS_ADMIN_POST_PER_PAGE;
        $options['limit']   = NEWS_ADMIN_POST_PER_PAGE;

        $config = prepare_pagination(
            array(
                'total_rows'    => $total_row,
                'per_page'      => $options['limit'],
                'offset'        => $options['offset'],
                'js_function'   => 'change_page_admin'
            )
        );
        $this->pagination->initialize($config);

        $options['emails']                  = $this->emails_model->get_emails($options);
        
        
        $options['lang_combobox']         = $this->utility_model->get_lang_combo(array('lang' => $options['lang'], 'extra' => 'onchange="javascript:change_lang();" class="btn"'));
        $options['post_uri']              = 'emails_admin';
        $options['total_rows']            = $total_row;
        $options['total_pages']           = $total_pages;
        $options['page_links']            = $this->pagination->create_ajax_links();
        
        if($options['lang'] <> DEFAULT_LANGUAGE){
            $options['uri'] = EMAILS_ADMIN_BASE_URL . '/' . $options['lang'];
        }else{
            $options['uri'] = EMAILS_ADMIN_BASE_URL;
        }
        
        if(isset($options['error']) || isset($options['succeed']) || isset($options['warning'])) 
            $options['options'] = $options;
        
        // Chuan bi du lieu chinh de hien thi
        $this->_view_data['main_content'] = $this->load->view('admin/emails_list',$options, TRUE);
        
        // Chuan bi cac the META
        $this->_view_data['title'] = 'Quản lý emails' . DEFAULT_TITLE_SUFFIX;
        // Tra lai view du lieu cho nguoi su dung
        $this->load->view($this->_layout, $this->_view_data);
    }
    

    function dispatcher($method = '', $para1 = NULL, $para2 = NULL) {
        $menu_params = array('current_menu' => '/' . $this->uri->uri_string());
        $layout = 'powercms/admin_layout';
        $current_url = '/dashboard/emails';

        switch ($method) {
            case 'list_emails':
                $menu_params = array('page' => $para1);
                $main_content = $this->list_emails($menu_params);
                break;
        }

        $view_data = array();
        $view_data['url'] = isset($current_url) ? $current_url : '';
        $view_data['admin_menu'] = modules::run('menus/menus/get_dashboard_menus', $menu_params);
        $view_data['main_content'] = $main_content;
        // META data
        $view_data['title'] = $this->_title;
        $this->load->view($layout, $view_data);
    }

    function list_emails($options = array()) {
        $total_row      = $this->emails_model->get_emails_count($options);
        $total_pages    = (int) ($total_row / EMAIL_PER_PAGE);

        if ($total_pages * EMAIL_PER_PAGE < $total_row)
            $total_pages++;
        if ((int) $options['page'] > $total_pages)
            $options['page'] = $total_pages;

        $options['offset'] = $options['page'] <= DEFAULT_PAGE ? DEFAULT_OFFSET : ((int) $options['page'] - 1) * EMAIL_PER_PAGE;
        $options['limit'] = EMAIL_PER_PAGE;

        $config = prepare_pagination(
                array(
                    'total_rows' => $total_row,
                    'per_page' => $options['limit'],
                    'offset' => $options['offset'],
                    'js_function' => 'change_page_admin'
                )
        );
        
        $this->pagination->initialize($config);
        $options['is_admin'] = TRUE;
        $view_data = array();
        $view_data['emails'] = $this->emails_model->get_emails($options);
        $view_data['post_uri'] = 'pages';
        $view_data['e_page'] = $options['page'];
        $view_data['total_rows'] = $total_row;
        $view_data['total_pages'] = $total_pages;
        $view_data['page_links'] = $this->pagination->create_ajax_links();

        $this->_title = 'Danh sách email' . DEFAULT_TITLE_SUFFIX;
        return $this->load->view('admin/list_emails', $view_data, TRUE);
    }
    
    function download()
    {
        $query = $this->db->get('emails');
        query_to_csv($query, TRUE, 'danh_sach_emails.csv');
    }
    
    function delete($id=0)
    {
        $this->emails_model->delete_email($id);
        redirect('/dashboard/emails');
    }
}

?>
