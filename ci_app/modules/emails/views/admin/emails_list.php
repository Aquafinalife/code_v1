<?php
    $this->load->view('admin/emails_nav');
    echo form_open('', array('id' => 'emails_form'), array('id' => 0, 'from_list' => TRUE));
    echo isset ($uri) ? form_hidden('uri', $uri) : NULL;
    echo form_hidden('back_url', NEWS_ADMIN_BASE_URL);
    echo form_close();
?>
<div class="form_content">
    <?php $this->load->view('admin/filter_form'); ?>
    <table class="list" style="width: 100%; margin-bottom: 10px;">
        <?php $this->load->view('powercms/message'); ?>
        <tr>
            <th class="left">EMAILS</th>
            <th class="center" style="width: 5%">NGÀY ĐĂNG KÝ</th>
            <th class="center" style="width: 5%">CHỨC NĂNG</th>
        </tr>

        <?php 
        if(isset($emails)){
        $stt = 0;
        foreach($emails as $index => $new):
         
            $created_date   = get_vndate_string($new->registry_date);
            $style = $stt++ % 2 == 0 ? 'even' : 'odd';
            $this_lang = ($new->lang<>DEFAULT_LANGUAGE)?'/'.$new->lang:'';
            
        ?>
        <tr class="<?php echo $style ?>">
           
            <td><a href=""><?php echo $new->emails; ?></a></td>
           
            <td class="center" style="white-space:nowrap;"><?php echo $created_date; ?></td>
           
            <td class="center" style="white-space:nowrap;" class="action">
                
                <?php if($this->phpsession->get('user_id') == $new->creator || $this->phpsession->get('roles_publisher') == STATUS_ACTIVE){ ?>
                <a class="del" title="Xóa Email" href="javascript:void(0);" onclick="submit_action(<?php echo $new->id ?>,'emails','delete');"><em>&nbsp;</em></a>
                <?php } ?>
                
            </td>
        </tr>
        <?php endforeach;?>
        <?php } ?>
        <?php $left_page_links = 'Trang ' . $page . ' / ' . $total_pages . ' (<span>Tổng số: ' . $total_rows . ' Emails</span>)'; ?>
        <tr class="list-footer">
            <th colspan="9">
                <div style="float:left; margin-top: 9px;"><?php echo $left_page_links; ?></div>
                <div style="float:right;" class="pagination"><?php if(isset($page_links) && $page_links!=='') echo $page_links; ?></div>
            </th>
        </tr>
    </table>
    <br class="clear"/>&nbsp;
</div>