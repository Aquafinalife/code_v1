<?php
    $this->load->view('admin/emails_nav');
?>
<div class="form_content">
    <table class="list" style="width: 100%;">
        <tr>
            <th class="left">EMAIL</th>
            <th class="left">NGÀY ĐĂNG KÝ</th>
            <th class="right" style="width: 5%;">CHỨC NĂNG</th>
        </tr>

        <?php
        foreach($emails as $index => $email):
        $created_date   = get_vndate_string($email->registry_date);
        $style = $index % 2 == 0 ? 'even' : 'odd';
        $functions      = '<a class="del" onclick="return confirm(\'Bạn có chắc chắn muốn xóa email này không?\');" href="/dashboard/emails/delete/' . $email->id . '" title="Xóa email"><em>&nbsp;</em></a>';
        ?>
        <tr class="<?php echo $style;?>">
            <td><?php echo $email->emails;?></td>
            <td><?php echo $created_date;?></td>
            <td class="right"><?php echo $functions;?></td>
        <?php endforeach;?>
        <?php $left_page_links = 'Trang ' . $e_page . ' / ' . $total_pages . ' (<span>Tổng: ' . $total_rows . '</span>)'; ?>
        <tr class="list-footer">
            <th colspan="7" class="left"><?php echo $left_page_links; ?></th>
        </tr>
    </table>
    <br class="clear"/>&nbsp;
</div>

<?php if(isset($page_links) && $page_links!=='') echo '<div class="pagination">' . $page_links . '</div>'; ?>