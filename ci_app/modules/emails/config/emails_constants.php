<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('EMAIL_PER_PAGE', 20);
define('EMAILS_ADMIN_BASE_URL',       '/dashboard/emails');