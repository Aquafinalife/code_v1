<?php

class Customers_Model extends MY_Model {

    function __construct() {
        parent::__construct();
    }
    function count_bh($options = array()) {
        $this->get_data_options_bh($options);
        return $q = $this->db->count_all_results(TBL_PRODUCTS);
        $q->free_result();
    }

    //end
    
    function get_bh($options = array()) {
        $this->get_data_options_bh($options);
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_PRODUCTS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_PRODUCTS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset'])){
                $this->db->limit($options['limit'], $options['offset']);
            }                
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            return $q = $this->db->get(TBL_PRODUCTS)->result();
//             $q = $this->db->get(TBL_PRODUCTS)->result();
//             $a= $this->db->last_query();
//             echo '<pre>';
//             print_r($a);
//             die;
        }

        $q->free_result();
    }

    //end
    function get_data_options_bh($options = array()) {
//        ' . TBL_CUSTOMERS . '.id as KH_id,
//        ' . TBL_CUSTOMERS . '.congty_canhan,
//        ' . TBL_CUSTOMERS . '.ndbh_quanhe,
//        ' . TBL_CUSTOMERS . '.mst_cmt,
//        ' . TBL_CUSTOMERS . '.phongban,
        //$this->db->select('*');
        $this->db->select(' ' . TBL_CUSTOMERS . '.id as KH_id,
                                ' . TBL_CUSTOMERS . '.congty_canhan,
                                ' . TBL_CUSTOMERS . '.mst_cmt,
                                ' . TBL_CUSTOMERS . '.parent_id,
                                ' . TBL_CUSTOMERS . '.phongban,
                                ' . TBL_CUSTOMERS . '.ndbh_quanhe,
                                ' . TBL_PRODUCTS . '.kh_mbh,
                                ' . TBL_PRODUCTS . '.goi_bh,
                                ' . TBL_PRODUCTS . '.dn_bbh,
                                ' . TBL_PRODUCTS . '.id as BH_id,
                                ' . TBL_PRODUCTS . '.ngay_hieuluc,
                                ' . TBL_PRODUCTS . '.ngay_ketthuc,
                                ' . TBL_PRODUCTS_SIZE . '.name as goi_bh_dm,
                                ' . TBL_PRODUCTS_SIZE . '.gia_tien as goi_gia_tien,
                                ' . TBL_PRODUCTS_SIZE . '.name,
                                ' . TBL_PRODUCTS_SIZE . '.TV_TTTBVV_TN,
                                ' . TBL_PRODUCTS_SIZE . '.TCL_NN,
                                ' . TBL_PRODUCTS_SIZE . '.CPYT_TN,
                                ' . TBL_PRODUCTS_SIZE . '.TV_TTTBVV_OB,
                                ' . TBL_PRODUCTS_SIZE . '.DTNT_OB,
                                ' . TBL_PRODUCTS_SIZE . '.THAISAN_QLNT,
                                ' . TBL_PRODUCTS_SIZE . '.THAISAN_MDL,
                                ' . TBL_PRODUCTS_SIZE . '.NHAKHOA_MDL,
                                ' . TBL_DN_BBH . '.name as ten_dn_bbh,
                            ');
        // Join
        if (isset($options['join_' . TBL_CUSTOMERS.'_mbh'])) {
            $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.kh_mbh = ' . TBL_CUSTOMERS . '.id', 'left');
        }
        if (isset($options['join_' . TBL_CUSTOMERS.'_dbh'])) {
            $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_CUSTOMERS . '.id', 'left');
        }
        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $this->db->join(TBL_PRODUCTS_SIZE, TBL_PRODUCTS . '.goi_bh = ' . TBL_PRODUCTS_SIZE . '.id', 'left');
        }
        if (isset($options['join_' . TBL_DN_BBH])) {
            $this->db->join(TBL_DN_BBH, TBL_PRODUCTS . '.dn_bbh = ' . TBL_DN_BBH . '.id', 'left');
        }
        //end
        // WHERE
        if(isset($options['a_kh_mbh']) && !empty(isset($options['a_kh_mbh']))){
            $this->db->where_in(TBL_PRODUCTS.'.kh_mbh', $options['a_kh_mbh']);
        }else{
            if (isset($options['kh_mbh'])) {
            $this->db->where(TBL_PRODUCTS . '.kh_mbh', $options['kh_mbh']);
        }
        }
        
        if (isset($options['kh_dbh'])) {
            $this->db->where(TBL_PRODUCTS . '.kh_dbh', $options['kh_dbh']);
        }
        if (isset($options['pb'])) {
            $this->db->where(TBL_CUSTOMERS . '.phongban', $options['pb']);
        }
        if (isset($options['OD_kh_dbh'])) {
            $this->db->where(TBL_ORDERS . '.OD_kh_dbh', $options['OD_kh_dbh']);
        }
        if (isset($options['goi_bh'])) {
            $this->db->where(TBL_PRODUCTS_SIZE . '.id', $options['goi_bh']);
        }
        if (isset($options['csyt'])) {
            $this->db->where(TBL_PRODUCTS_STYLE . '.id', $options['csyt']);
        }
        if (isset($options['ma_benh'])) {
            $this->db->where(TBL_ORDERS . '.OD_ma_benh', $options['ma_benh']);
        }
        if (isset($options['qlbh'])) {
            $this->db->where(TBL_ORDERS . '.ql_bh', $options['qlbh']);
        }
        if (isset($options['id'])) {
            $this->db->where(TBL_PRODUCTS . '.id', $options['id']);
        }        
        if (isset($options['OD_id'])) {
            $this->db->where(TBL_ORDERS . '.id', $options['OD_id']);
        }
        //end
    }

    //end

    function count_bt($options = array()) {
        $this->get_data_options_bt($options);
        return $q = $this->db->count_all_results(TBL_PRODUCTS);
        $q->free_result();
    }

    //end
    function get_data_options_bt($options = array()) {
        //$this->db->select('*');
        $this->db->select(' ' . TBL_PRODUCTS . '.*,
                                ' . TBL_CUSTOMERS . '.id as KH_id,
                                ' . TBL_CUSTOMERS . '.congty_canhan,
                                ' . TBL_CUSTOMERS . '.ndbh_quanhe,
                                ' . TBL_CUSTOMERS . '.mst_cmt,
                                ' . TBL_CUSTOMERS . '.parent_id,
                                ' . TBL_CUSTOMERS . '.phongban,
                                ' . TBL_ORDERS . '.id as OD_id,
                                ' . TBL_ORDERS . '.OD_kh_dbh,
                                ' . TBL_ORDERS . '.loai_benh,
                                ' . TBL_ORDERS . '.so_tien_ycbt,
                                ' . TBL_ORDERS . '.so_tien_dbt,
                                ' . TBL_ORDERS . '.ngay_ycbt,
                                ' . TBL_ORDERS . '.so_ngay_ycbt,
                                ' . TBL_ORDERS . '.ql_bh,
                                ' . TBL_ORDERS . '.ngay_giaiquyet,
                                ' . TBL_ORDERS . '.ngay_xayra_tonthat,
                                ' . TBL_ORDERS . '.ngay_dieutri,
                                ' . TBL_ORDERS . '.csyt,
                                ' . TBL_ORDERS . '.order_status,
                                ' . TBL_ORDERS . '.tinhtrang_hoso,
                                ' . TBL_ORDERS . '.chungtu_bosung,
                                ' . TBL_ORDERS . '.ngay_bosung_hoso,
                                ' . TBL_ORDERS . '.ngay_duyet_bt,
                                ' . TBL_ORDERS . '.phuongthuc_bt,
                                ' . TBL_ORDERS . '.ngay_nhan_hoso,
                                ' . TBL_ORDERS . '.ngay_xuly_bt,
                                ' . TBL_ORDERS . '.ngay_thongbao_kh,
                                ' . TBL_ORDERS . '.chuandoan_benh,
                                ' . TBL_ORDERS . '.OD_ma_benh,
                                ' . TBL_ORDERS . '.so_tien_thanhtoan_csyt,
                                ' . TBL_ORDERS . '.so_tien_tuchoi,
                                ' . TBL_ORDERS . '.lydo_khong_bt,
                                ' . TBL_ORDERS . '.ghichu_thanhtoan,
                                ' . TBL_ORDERS . '.hinhthuc_nhantien_bt,
                                ' . TBL_ORDERS . '.OD_ngay_thanhtoan,
                                ' . TBL_ORDERS . '.hoso_scan,
                                ' . TBL_PRODUCTS_SIZE . '.name as goi_bh_dm,
                                ' . TBL_PRODUCTS_SIZE . '.gia_tien as goi_gia_tien,
                                ' . TBL_PRODUCTS_STYLE . '.name as ten_csyt,
                                ' . TBL_DN_BBH . '.name as ten_dn_bbh,
                            ');
        // Join
        if (isset($options['join_' . TBL_ORDERS])) {
            $this->db->join(TBL_ORDERS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_ORDERS . '.OD_kh_dbh', 'left');
        }
        if (isset($options['join_' . TBL_CUSTOMERS])) {
            $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_CUSTOMERS . '.id', 'left');
        }
        if (isset($options['join_' . TBL_PRODUCTS_SIZE])) {
            $this->db->join(TBL_PRODUCTS_SIZE, TBL_PRODUCTS . '.goi_bh = ' . TBL_PRODUCTS_SIZE . '.id', 'left');
        }
        if (isset($options['join_' . TBL_PRODUCTS_STYLE])) {
            $this->db->join(TBL_PRODUCTS_STYLE, TBL_ORDERS . '.csyt = ' . TBL_PRODUCTS_STYLE . '.id', 'left');
        }
        if (isset($options['join_' . TBL_DN_BBH])) {
            $this->db->join(TBL_DN_BBH, TBL_PRODUCTS . '.dn_bbh = ' . TBL_DN_BBH . '.id', 'left');
        }
        //end
        // WHERE
        if (isset($options['kh_mbh'])) {
            $this->db->where(TBL_PRODUCTS . '.kh_mbh', $options['kh_mbh']);
        }
        if (isset($options['kh_dbh'])) {
            $this->db->where(TBL_PRODUCTS . '.kh_dbh', $options['kh_dbh']);
        }
        if (isset($options['OD_kh_dbh'])) {
            $this->db->where(TBL_ORDERS . '.OD_kh_dbh', $options['OD_kh_dbh']);
        }
        if (isset($options['OD_id'])) {
            $this->db->where(TBL_ORDERS . '.id', $options['OD_id']);
        }
        if (isset($options['goi_bh'])) {
            $this->db->where(TBL_PRODUCTS_SIZE . '.id', $options['goi_bh']);
        }
        if (isset($options['csyt'])) {
            $this->db->where(TBL_PRODUCTS_STYLE . '.id', $options['csyt']);
        }
        if (isset($options['ma_benh'])) {
            //$this->db->where(TBL_ORDERS . '.OD_ma_benh', $options['ma_benh']);
            $this->db->where(TBL_ORDERS . '.OD_ma_benh', $options['ma_benh']);
        }
        if (isset($options['qlbh'])) {
            $this->db->where(TBL_ORDERS . '.ql_bh', $options['qlbh']);
        }
        if (isset($options['id'])) {
            $this->db->where(TBL_PRODUCTS . '.id', $options['id']);
        }        
        if (isset($options['OD_id'])) {
            $this->db->where(TBL_ORDERS . '.id', $options['OD_id']);
        }
        //end
    }

    //end
    function get_bt($options = array()) {
        $this->get_data_options_bt($options);
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_PRODUCTS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_PRODUCTS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            return $q = $this->db->get(TBL_PRODUCTS)->result();
//             $q = $this->db->get(TBL_PRODUCTS)->result();
//             $a= $this->db->last_query();
//             echo '<pre>';
//             print_r($a);
//             die;
        }

        $q->free_result();
    }

    //end

    function get_canhan_dbh($options = array()) {
        $this->db->select('*');
        // Join
        if (isset($options['join_' . TBL_PRODUCTS])) {
            //$this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.custom_duoc_id = ' . TBL_CUSTOMERS . '.id', 'left');
        }
        $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.kh_dbh = ' . TBL_CUSTOMERS . '.id', 'left');
        //end
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_PRODUCTS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['id']) || isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_PRODUCTS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            return $q = $this->db->get(TBL_PRODUCTS)->result();
        }

        $q->free_result();
    }

    //end
    function get_doanhnghiep_mbh($options = array()) {
        $this->db->select('*');
        $this->db->join(TBL_CUSTOMERS, TBL_PRODUCTS . '.kh_mbh = ' . TBL_CUSTOMERS . '.id', 'left');
        //end
        // WHERE
        if (isset($options['phanloai_kh']) && !empty($options['phanloai_kh'])) {
            $this->db->where(TBL_CUSTOMERS . '.phanloai_kh', $options['phanloai_kh']);
        }
        //end
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_PRODUCTS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['id']) || isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_PRODUCTS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            return $q = $this->db->get(TBL_PRODUCTS)->result();
        }

        $q->free_result();
    }

    //end
    function get_kh_by_options($options = array()) {
        $this->db->select('*');
        //end
        // WHERE
        // Loại kh
        if (isset($options['phanloai_kh'])) {
            $this->db->where(TBL_CUSTOMERS . '.phanloai_kh', $options['phanloai_kh']);
        }
        // lấy dn 
        if (isset($options['parent_id'])) {
            $this->db->where(TBL_CUSTOMERS . '.parent_id', $options['parent_id']);
        }
        if (isset($options['email'])) {
            $this->db->where(TBL_CUSTOMERS . '.email', $options['email']);
        }
        if (isset($options['password'])) {
            $this->db->where(TBL_CUSTOMERS . '.password', $options['password']);
        }
        if (isset($options['id'])) {
            $this->db->where(TBL_CUSTOMERS . '.id', $options['id']);
        }
        //END
        if (isset($options['per_page']) && isset($options['page'])) {
            return $q = $this->db->get(TBL_CUSTOMERS, $options['per_page'], $options['page'])->result();
        } else
        if (isset($options['id']) || isset($options['get_one']) || isset($options['get_row'])) {
            return $q = $this->db->get(TBL_CUSTOMERS)->first_row();
        } else {
            if (isset($options['limit']) && isset($options['offset']))
                $this->db->limit($options['limit'], $options['offset']);
            else if (isset($options['limit']))
                $this->db->limit($options['limit']);
            return $q = $this->db->get(TBL_CUSTOMERS)->result();
        }

        $q->free_result();
    }

    //end
    public function get_customers($options = array()) {

        $this->db->select(' customers.*,
                        (select count(*) from ' . $this->db->dbprefix . 'orders where user_id =' . $this->db->dbprefix . 'customers.id) as count_order
                    ');

//        $this->db->join('customers_categories', 'customers_categories.id = customers.cat_id', 'left');

        if (isset($options['status'])) {
            $this->db->where('customers.status', $options['status']);
        }

        if (isset($options['cmt']) && !empty($options['cmt'])) {
            $this->db->where('customers.cmt', $options['cmt']);
        }

        if (isset($options['user_id']))
            $this->db->where('customers.id', $options['user_id']);

        if (isset($options['id']))
            $this->db->where('customers.id', $options['id']);

        if (isset($options['fullname'])) {
            $this->db->where('customers.fullname', $options['fullname']);
        }

        if (isset($options['phanloai_kh'])) {
            $this->db->where('customers.phanloai_kh', $options['phanloai_kh']);
        }
        if (isset($options['parent_id'])) {
            $this->db->where('customers.parent_id', $options['parent_id']);
        }
        if (isset($options['mst_cmt'])) {
            $this->db->where('customers.mst_cmt', $options['mst_cmt']);
        }


        if (isset($options['search']) && $options['search'] != '') {
            $where = "(" . $this->db->dbprefix . "customers.congty_canhan like'%" . $this->db->escape_str($options['search']) . "%'";
            $where .= " or " . $this->db->dbprefix . "customers.didong like'%" . $this->db->escape_str($options['search']) . "%')";
            $this->db->where($where);
        }

//        if (isset($options['search']) && $options['search'] != '') {
//            $where = "(" . $this->db->dbprefix . "customers.fullname like'%" . $this->db->escape_str($options['search']) . "%'";
//            $where .= " or " . $this->db->dbprefix . "customers.phone like'%" . $this->db->escape_str($options['search']) . "%')";
//            $this->db->where($where);
//        }


        if (isset($options['email'])) {
            $this->db->where('customers.email', $options['email']);
        }

        if (isset($options['password'])) {
            $this->db->where('customers.password', $options['password']);
        }

        //
        if (isset($options['a_id'])) {
            $this->db->where_in('customers.id', $options['a_id']);
        }
        //end
        // Loc theo trang
        if (isset($options['limit']) && isset($options['offset']))
            $this->db->limit($options['limit'], $options['offset']);
        else if (isset($options['limit']))
            $this->db->limit($options['limit']);

        if (isset($options['sort_by_viewed'])) {
            $this->db->order_by('customers.viewed desc, customers.updated_date desc, customers.created_date desc');
        } elseif (isset($options['random']) && $options['random'] == TRUE) {
            $this->db->order_by('RAND()');
        } else {
            $this->db->order_by('customers.updated_date desc, customers.created_date desc');
        }

        $query = $this->db->get('customers');

        if (isset($options['last_row']))
            return $query->row(0);

        if ($query->num_rows() > 0) {
            if (isset($options['id'])) {
                if (!isset($options['flag'])) {
                    return $query->row(0);
                }
            }
            if ((isset($options['onehit']) && $options['onehit'] == TRUE) || isset($options['get_one'])) {
                return $query->row(0);
            }
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function get_customers_buy_id($options = array()) {

        $this->db->select(' customers.*,

                    ');

//        $this->db->join('customers_categories', 'customers_categories.id = customers.cat_id', 'left');



        if (isset($options['id']))
            $this->db->where('customers.id', $options['id']);

        if (isset($options['fullname'])) {
            $this->db->where('customers.fullname', $options['fullname']);
        }

        $query = $this->db->get('customers');

        if (isset($options['last_row']))
            return $query->row(0);

        if ($query->num_rows() > 0) {
            if (isset($options['id'])) {
                if (!isset($options['flag'])) {
                    return $query->row(0);
                }
            }
            if (isset($options['onehit']) && $options['onehit'] == TRUE) {
                return $query->row(0);
            }
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function get_customers_count($options = array()) {
        return count($this->get_customers($options));
    }

    public function update_customers_view($id = 0) {
        $this->db->set('viewed', 'viewed + 1', FALSE);
        $this->db->where('id', $id);
        $this->db->update('customers');
    }

    // check username khi đăng ký đã tồn tại chưa
    function is_available_username_customers($params = array()) {
        $this->db->where('email', $params['email']);

        $query = $this->db->get('customers');

        if (count($query->row()) > 0) {
            $this->_last_message = '<p>Email: <strong>' . $params['email'] . '</strong> đã tồn tại trong hệ thống.</p>';
            return FALSE;
        }
        return TRUE;
    }

    function get_last_message() {
        return $this->_last_message;
    }

    public function get_customers_buy_combo($options = array()) {
        // Default categories name
        if (!isset($options['combo_name'])) {
            $options['combo_name'] = 'custom_buy_id';
        }
        if (!isset($options['extra'])) {
            $options['extra'] = '';
        }
        if (!isset($options['is_add_edit_menu'])) {
            if (isset($options['is_add_edit_cat']))
                $categories[ROOT_CATEGORY_ID] = 'Tất cả';
            else
                $categories[DEFAULT_COMBO_VALUE] = ' - Chọn';
        }

        $this->db->where('status', STATUS_ACTIVE);
        $data = $this->db->get('customers');
        if ($data->num_rows() > 0) {
            $data = $data->result_array();
            foreach ($data as $id => $value) {
                $id = $value['id'];
                $categories[$id] = $value['fullname'];
            }
            if (!isset($options[$options['combo_name']]))
                $options[$options['combo_name']] = DEFAULT_COMBO_VALUE;
            return form_dropdown($options['combo_name'], $categories, $options[$options['combo_name']], $options['extra']);
        }else {
            return NULL;
        }
    }

    public function get_customers_duoc_combo($options = array()) {
        // Default categories name
        if (!isset($options['combo_name'])) {
            $options['combo_name'] = 'custom_duoc_id';
        }
        if (!isset($options['extra'])) {
            $options['extra'] = '';
        }
        if (!isset($options['is_add_edit_menu'])) {
            if (isset($options['is_add_edit_cat']))
                $categories[ROOT_CATEGORY_ID] = 'Tất cả';
            else
                $categories[DEFAULT_COMBO_VALUE] = ' - Chọn';
        }

        $this->db->where('status', STATUS_ACTIVE);
        $data = $this->db->get('customers');
        if ($data->num_rows() > 0) {
            $data = $data->result_array();
            foreach ($data as $id => $value) {
                $id = $value['id'];
                $categories[$id] = $value['fullname'];
            }
            if (!isset($options[$options['combo_name']]))
                $options[$options['combo_name']] = DEFAULT_COMBO_VALUE;
            return form_dropdown($options['combo_name'], $categories, $options[$options['combo_name']], $options['extra']);
        }else {
            return NULL;
        }
    }

}

?>