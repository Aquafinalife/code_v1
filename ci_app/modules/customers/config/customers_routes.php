<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// FRONT-END
$route['^thong-tin-ca-nhan$']                       = 'customers/get_infomation_customers';
$route['^quan-ly-don-hang$']                        = 'customers/manager_order/1';
$route['^quan-ly-don-hang/trang-(\d+)$']            = 'customers/manager_order/$1';
$route['^quan-ly-don-hang/chi-tiet-don-hang$']      = 'customers/detail_order_ajax';

$route['^tao-tai-khoan|sign-up$']                   = 'customers/customers_sign_up';
$route['^dang-nhap-thanh-vien|customers-login$']    = 'customers/login_customers';
$route['^dang-xuat$']                               = 'customers/logout';

$route['^get_data_auto_complate_custom$']                               = 'customers/get_data_auto_complate_custom';
$route['^get_data_auto_complate_custom_duoc$']                          = 'customers/get_data_auto_complate_custom_duoc';
// BACK-END


// Pagination
$route['^quan-tri-khach-hang']                                      = 'customers/quan_tri_khach_hang/0';
$route['^quan-tri-khach-hang/(\d+)$']                               = 'customers/quan_tri_khach_hang/$1';


// Bồi thường
$route['^boi-thuong-cn']                                            = 'customers/get_bt_cn/0';
$route['^boi-thuong-cn/(\d+)$']                                     = 'customers/get_bt_cn/$1';

$route['^boi-thuong-dn']                                            = 'customers/get_bt_dn/0';
$route['^boi-thuong-dn/(\d+)$']                                     = 'customers/get_bt_dn/$1';

// Bảo hiểm

$route['^bao-hiem-cn']                                              = 'customers/get_bh_cn/0';
$route['^bao-hiem-cn/(\d+)$']                                       = 'customers/get_bh_cn/$1';

$route['^bao-hiem-dn']                                              = 'customers/get_bh_dn/0';
$route['^bao-hiem-dn/(\d+)$']                                       = 'customers/get_bh_dn/$1';