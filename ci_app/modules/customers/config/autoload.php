<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$autoload['libraries']  = array('pagination');
$autoload['helper']     = array('text','customers');
$autoload['plugin']     = array();
$autoload['config']     = array();
$autoload['language']   = array();
$autoload['model']      = array('customers_model','orders/orders_model',
    'products/products_size_model','products/products_style_model','products/products_origin_model'
    ,'products/products_color_model');