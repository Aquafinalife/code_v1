<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
$ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
$ss_kh_phanloai_kh_txt = get_customers_type($ss_kh_phanloai_kh);
$ss_kh_fullname = $this->phpsession->get('ss_kh_fullname');
$ss_kh_mst_cmt = $this->phpsession->get('ss_kh_mst_cmt');
$ss_kh_diachi = $this->phpsession->get('ss_kh_diachi');
$ss_kh_ngaycap = $this->phpsession->get('ss_kh_ngaycap');
$ss_kh_noicap = $this->phpsession->get('ss_kh_noicap');
$ss_kh_dienthoai = $this->phpsession->get('ss_kh_dienthoai');
$ss_kh_didong = $this->phpsession->get('ss_kh_didong');
$ss_kh_phanloai_kh1 = $this->phpsession->get('ss_kh_phanloai_kh1');
$ss_kh_phanloai_kh1_txt = get_customers_type1($ss_kh_phanloai_kh1);
//end
$a_qlbh = array(NOITRU => 'Nội trú', NGOAITRU => 'Ngoại trú');
?>
<div class="box_filter" style="margin-bottom: 20px">
    <?php
    $a_kh_dbh = array();
    if (isset($bh_all) && !empty($bh_all)) {
        foreach ($bh_all as $index) {
            $KH_id = $index->KH_id;
            $ten_kh = $index->congty_canhan;
            $mst_cmt = $index->mst_cmt;
            $a_kh_dbh[$KH_id] = $ten_kh . ' - ' . $mst_cmt;
        }
    }
    ?>
    <form action="<?php echo site_url(URL_BH_DN) ?>" method="get">
        <?php
        $G_kh_dbh = isset($_GET['kh_dbh']) ? $_GET['kh_dbh'] : '';
        $G_goi_bh = isset($_GET['goi_bh']) ? $_GET['goi_bh'] : '';
        $G_cn = isset($_GET['cn']) ? $_GET['cn'] : '';
        $G_pb = isset($_GET['pb']) ? $_GET['pb'] : '';
        $G_ma_benh = isset($_GET['ma_benh']) ? $_GET['ma_benh'] : '';
        ?>
        <select name="kh_dbh" class="js-example-disabled-results">
            <option value="">-- Họ tên | CMT --</option>            
            <?php
            if (isset($a_kh_dbh) && !empty($a_kh_dbh)) {
                //$a_dn = array();
                foreach ($a_kh_dbh as $k => $v) {
                    $selected = ($k == $G_kh_dbh) ? 'selected="selected"' : '';
                    ?>
                    <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                    <?php
                    //$a_dn[] = $congty_canhan;
                }
            }
            ?>
        </select>
        <select name="cn" class="js-example-disabled-results" style="margin-left: 10px !important; padding-left: 10px !important">
            <option value="">-- Chi nhánh công ty --</option>            
            <?php
            if (isset($cn_dn) && !empty($cn_dn)) {
                //$a_dn = array();
                foreach ($cn_dn as $index) {
                    $id = $index->id;
                    $congty_canhan = $index->congty_canhan;
                    $selected = ($id == $G_cn) ? 'selected="selected"' : '';
                    ?>
                    <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $congty_canhan ?></option>
                    <?php
                    //$a_dn[] = $congty_canhan;
                }
            }
            ?>
        </select>
        <select name="pb" class="js-example-disabled-results">
            <option value="">-- Phòng ban --</option>            
            <?php
            if (isset($a_phongban) && !empty($a_phongban)) {
                //$a_dn = array();
                foreach ($a_phongban as $index) {
                    $id = $index->id;
                    $name = $index->name;
                    $selected = ($id == $G_pb) ? 'selected="selected"' : '';
                    ?>
                    <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                    <?php
                    //$a_dn[] = $congty_canhan;
                }
            }
            ?>
        </select>
        <select name="goi_bh" class="js-example-disabled-results" style="width: 200px">
            <option value="">-- Gói BH --</option>            
            <?php
            if (isset($goi_bh) && !empty($goi_bh)) {
                //$a_dn = array();
                foreach ($goi_bh as $index) {
                    $id = $index->id;
                    $name = $index->name;
                    $selected = ($id == $G_goi_bh) ? 'selected="selected"' : '';
                    ?>
                    <option <?php echo $selected ?> value="<?php echo $id ?>"><?php echo $name ?></option>
                    <?php
                    //$a_dn[] = $congty_canhan;
                }
            }
            ?>
        </select>
        <div class="col-sm-12 text-center" style="margin-top: 20px">
            <input style="margin-top: 7px;" class="btn btn btn-inline btn-sm" type="submit" value="Tìm kiếm"/>
            <input style="margin-top: -1px;margin-left: 20px;" class="btn btn btn-success btn-sm" type="button" value="Xuất execel"/>
            <div></div>
        </div>
    </form>
</div>
<table id="_table-edit" class="table table-bordered table-hover">    
    <h3>Chi tiết bảo hiểm:</h3>
    <thead>
        <tr>
            <th>Mã số KH</th>
            <th>Họ và tên KH</th>
            <th>CMT/Hộ chiếu</th>
            <!--<th>Công ty</th>-->
            <!--<th>Phòng ban</th>-->
            <th>Ngày hiệu lực</th>
            <th>Ngày hết hạn</th>
            <th>Gói BH</th>
            <th>Công ty BH</th>
            <th>Mối quan hệ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($bh) && !empty($bh)) {
            foreach ($bh as $index) {
                $KH_id = $index->KH_id;
                $ten_kh = $index->congty_canhan;
                $mst_cmt = $index->mst_cmt;
                $parent_id = $index->parent_id;
                $phongban = $index->phongban;
                $ngay_hieuluc = $index->ngay_hieuluc;
                $ngay_ketthuc = $index->ngay_ketthuc;
                $goi_bh_dm = $index->goi_bh_dm;
                $ten_dn_bbh = $index->ten_dn_bbh;
                $ndbh_quanhe = $index->ndbh_quanhe;
                $ndbh_quanhe_txt = get_ndbh_quanhe($ndbh_quanhe);
                ?>
                <tr>
                    <td><?php echo 'MSKH-' . $KH_id ?></td>
                    <td><?php echo $ten_kh ?></td>
                    <td><?php echo $mst_cmt ?></td>
                    <td><?php echo $ngay_hieuluc ?></td>
                    <td><?php echo $ngay_ketthuc ?></td>
                    <td><?php echo $goi_bh_dm ?></td>
                    <td><?php echo $ten_dn_bbh ?></td>
                    <td><?php echo $ndbh_quanhe_txt ?></td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="10" class="text-danger">Không có dữ liệu.</td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<div class="col-sm-12 text-center">
    <div class="pagination">
        <?php echo $this->pagination->create_links(); ?>
    </div>
</div>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>