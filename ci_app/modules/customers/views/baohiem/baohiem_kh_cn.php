<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
$ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
$ss_kh_phanloai_kh_txt = get_customers_type($ss_kh_phanloai_kh);
$ss_kh_fullname = $this->phpsession->get('ss_kh_fullname');
$ss_kh_mst_cmt = $this->phpsession->get('ss_kh_mst_cmt');
$ss_kh_diachi = $this->phpsession->get('ss_kh_diachi');
$ss_kh_ngaycap = $this->phpsession->get('ss_kh_ngaycap');
$ss_kh_noicap = $this->phpsession->get('ss_kh_noicap');
$ss_kh_dienthoai = $this->phpsession->get('ss_kh_dienthoai');
$ss_kh_didong = $this->phpsession->get('ss_kh_didong');
$ss_kh_phanloai_kh1 = $this->phpsession->get('ss_kh_phanloai_kh1');
$ss_kh_phanloai_kh1_txt = get_customers_type1($ss_kh_phanloai_kh1);
$ss_kh_parent_id = $this->phpsession->get('ss_kh_parent_id');
//end
$a_qlbh = array(NOITRU => 'Nội trú', NGOAITRU => 'Ngoại trú');
?>
<div class="box_filter" style="margin-bottom: 20px">
    
    <form action="<?php echo site_url(URL_BT_CN) ?>" method="get">
    </form>
</div>
<table id="_table-edit" class="table table-bordered table-hover">    
    <h3>Thông tin bảo hiểm:</h3>
    <thead>
        <tr>
            <th>Options</th>
            <th>Value</th>
            <th>Options</th>
            <th>Value</th>
        </tr>
    </thead>
    <?php
    if (isset($bh) && !empty($bh)) {
        foreach ($bh as $index) {
            $id = $index->id;
            $kh_mbh = $index->kh_mbh;
            $dn_mbh = $index->congty_canhan;
            $parent_id = $index->parent_id;
            $phongban = $index->phongban;
            $goi_bh_dm = $index->goi_bh_dm;
            $so_ngay_ycbt = $index->so_ngay_ycbt;
            $ten_dn_bbh = $index->ten_dn_bbh;
            $TV_TTTBVV_TN= $index->TV_TTTBVV_TN;
            $TCL_NN= $index->TCL_NN;
            $CPYT_TN= $index->CPYT_TN;
            $TV_TTTBVV_OB= $index->TV_TTTBVV_OB;
            $DTNT_OB= $index->DTNT_OB;
            $THAISAN_QLNT= $index->THAISAN_QLNT;
            $THAISAN_MDL= $index->THAISAN_MDL;
            $NHAKHOA_MDL= $index->NHAKHOA_MDL;
            ?>
    <tbody>
        <tr>
            <td>Gói BH </td>
            <td><?php echo $goi_bh_dm ?></td>
            <td>Trợ cấp lương,ngày nghỉ</td>
            <td><?php echo $TCL_NN ?></td>
        </tr>
        <tr>
            <td>Bên mua BH </td>
            <td><?php echo $dn_mbh ?></td>
            <td>Chi phí y tế (Do tai nạn)</td>
            <td><?php echo $CPYT_TN ?></td>
        </tr>
        <tr>
            <td>Công ty BH </td>
            <td><?php echo $ten_dn_bbh ?></td>
            <td>Điều trị nội trú ốm bệnh</td>
            <td><?php echo $DTNT_OB ?></td>
        </tr>
        <tr>
            <td>Tử vong, TTTBVV(Do tai nạn)</td>
            <td><?php echo $TV_TTTBVV_TN ?></td>
            <td>Thai sản trong QLBH</td>
            <td><?php echo $THAISAN_QLNT ?></td>
        </tr>
        <tr>
            <td>Tử vong, TTTBVV(Do ốm bệnh) </td>
            <td><?php echo $TV_TTTBVV_OB ?></td>
            <td>Thai sản mua độc lập</td>
            <td><?php echo $THAISAN_MDL ?></td>
        </tr>
        <tr>
            <td>Nha khoa</td>
            <td><?php echo $NHAKHOA_MDL ?></td>
        </tr>
    </tbody>
    <?php
        }
    }
    ?>
</table>
<div class="col-sm-12 text-center">
    <div class="pagination">
        <?php // echo $this->pagination->create_links(); ?>
    </div>
</div>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>