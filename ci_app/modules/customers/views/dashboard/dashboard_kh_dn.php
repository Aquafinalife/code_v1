<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
$ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
$ss_kh_phanloai_kh_txt = get_customers_type($ss_kh_phanloai_kh);
$ss_kh_fullname = $this->phpsession->get('ss_kh_fullname');
$ss_kh_mst_cmt = $this->phpsession->get('ss_kh_mst_cmt');
$ss_kh_diachi = $this->phpsession->get('ss_kh_diachi');
$ss_kh_ngaycap = $this->phpsession->get('ss_kh_ngaycap');
$ss_kh_noicap = $this->phpsession->get('ss_kh_noicap');
$ss_kh_dienthoai = $this->phpsession->get('ss_kh_dienthoai');
$ss_kh_didong = $this->phpsession->get('ss_kh_didong');
$ss_kh_phanloai_kh1 = $this->phpsession->get('ss_kh_phanloai_kh1');
$ss_kh_phanloai_kh1_txt = get_customers_type1($ss_kh_phanloai_kh1);
//end
?>
<table id="table-edit_" class="table table-bordered table-hover">
    <h3>Thông tin khách hàng:</h3>
    <thead>
        <tr>
            <th>Options</th>
            <th>Value</th>
            <th>Options</th>
            <th>Value</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Tên khách hàng</td>
            <td><?php echo $ss_kh_name ?></td>
            <td>ĐKKD - MST</td>
            <td><?php echo $ss_kh_mst_cmt ?></td>
        </tr>
        <tr>
            <td>Mã khách hàng</td>
            <td><?php echo 'MKH-' . $ss_kh_id ?></td>     
            <td>Loại khách hàng</td>
            <td><?php echo $ss_kh_phanloai_kh1_txt ?></td>
            <!--<td>Ngày cấp</td>-->
            <!--<td><?php // echo $ss_kh_ngaycap        ?></td>-->
        </tr>
        <tr>
            <td>Hợp đồng Bảo Hiểm</td>
            <td><?php echo 'FTC' . $ss_kh_id ?></td>
            <!--<td>Nơi cấp</td>-->
            <!--<td><?php // echo $ss_kh_noicap        ?></td>-->
        </tr>
        <tr>
            <td>Địa chỉ</td>
            <td><?php echo $ss_kh_diachi ?></td>            
        </tr>
        <tr>
            <td>Email</td>
            <td><?php echo $ss_kh_email ?></td>
        </tr>
        <tr>
            <td>Điện thoại</td>
            <td><?php echo $ss_kh_dienthoai ?></td>
        </tr>
        <tr>
            <td>Di động</td>
            <td><?php echo $ss_kh_didong ?></td>
        </tr>
    </tbody>
</table>
<br><br>
<table id="table-edit_" class="table table-bordered table-hover">
    <?php
    if (isset($bt) && !empty($bt)) {
        $so_hsbt = count($bt);
        $t_sotien_ycbt = $t_sotien_dbt = $t_sotien_bt_nv = $t_sotien_bt_nt = $t_goi_gia_tien = 0;
        foreach ($bt as $index) {
            $ndbh_quanhe = $index->ndbh_quanhe;
            $so_tien_ycbt = ($index->so_tien_ycbt);
            $so_tien_dbt = ($index->so_tien_dbt);
            $goi_gia_tien = ($index->goi_gia_tien);
            $t_sotien_ycbt = (int) ($t_sotien_ycbt) + (int) $so_tien_ycbt;
            //$t_sotien_ycbt = ($t_sotien_ycbt)+ $so_tien_ycbt;
            //echo $t_sotien_ycbt.'//';
            $t_sotien_dbt = (int) ($t_sotien_dbt) + (int) $so_tien_dbt;
            //$t_sotien_dbt = ($t_sotien_dbt)+ $so_tien_dbt;
            $t_goi_gia_tien = (int) ($t_goi_gia_tien) + (int) $goi_gia_tien;
            if ($ndbh_quanhe == NHANVIEN) {
                $t_sotien_bt_nv = (($t_sotien_bt_nv) + $so_tien_dbt);
            }
            if ($ndbh_quanhe == NGUOITHAN) {
                $t_sotien_bt_nt = (($t_sotien_bt_nt) + $so_tien_dbt);
            }
        }
    }
    ?>
    <h3>Báo cáo bồi thường:</h3>
    <thead>
        <tr>
            <th>Options</th>
            <th>Value</th>
            <th>Options</th>
            <th>Value</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Tổng số HSBT</td>
            <td><?php echo $so_hsbt ?></td>
            <td>Số tiền bồi thường NV</td>
            <td><?php echo get_price_in_vnd($t_sotien_bt_nv) ?></td>
        </tr>
        <tr>
            <td>Tổng số tiền YCBT</td>
            <td><?php echo get_price_in_vnd($t_sotien_ycbt) ?></td>     
            <td>Số tiền bồi thường NT</td>
            <td><?php echo get_price_in_vnd($t_sotien_bt_nt) ?></td>
        </tr>
        <tr>
            <td>Tổng số tiền BT</td>
            <td><?php echo get_price_in_vnd($t_sotien_dbt) ?></td>
            <td>Tỷ lệ BTUT</td>
            <td><?php // echo $ss_kh_noicap        ?></td>
        </tr>
        <tr>
            <td>Tỷ lệ BT hiện tại</td>
            <td><?php echo round((($t_sotien_dbt / $t_goi_gia_tien) * 100),2) . '%'; ?></td>            
        </tr>
    </tbody>
</table>
<br><br>

<!--<div class="table-responsive">-->
<table id="_table-edit" class="table table-bordered table-hover">    
    <h3>Chi tiết bồi thường: <a href="<?php echo site_url(URL_BT_DN) ?>" class="pull-right btn btn-primary">Xem chi tiết</a></h3>
    <thead>
        <tr>
            <th>Mã số HSBT</th>
            <th>Khách hàng được BH</th>
            <th>Công ty chi nhánh</th>
            <th>Phòng ban</th>
            <th>CMT/Hộ chiếu</th>
            <th>Tình trạng GQBT</th>
            <th>Tình trạng GQHS</th>
            <th>Chứng từ chờ BS</th>
            <th>Ngày BSHS</th>
            <th>Ngày duyệt BT</th>
            <th>Loại BT</th>
            <th>Ngày XRTT</th>
            <th>Mối quan hệ</th>
            <th>Phương thức BT</th>
            <th>Ngày nhận HS</th>
            <th>Ngày XLBT</th>
            <th>Ngày TBKH</th>
            <th>Chuẩn đoán bệnh</th>
            <th>Mã bệnh</th>
            <th>Số tiền TTTCSYT</th>
            <th>Số tiền ĐBT</th>
            <th>Số tiền từ chối</th>
            <th>Lý do KBT</th>
            <th>Hình thức NTBT</th>
            <th>Ghi chú TT</th>
            <th>Ngày TT</th>
            <th>Phí BH</th>
            <!--<th>CSYT</th>-->
            <th>Công ty BH</th>
            <th>Hồ sơ scan</th>
            <!--end-->
            <th>Ngày xảy ra tổn thất</th>
            <th>Ngày điều trị</th>
            <th>Cơ sở y tế</th>
            <th>Loại bệnh</th>
            <!--<th>Quyền lợi BH</th>-->
            <th>Ngày YCBT</th>
            <th>Số tiền YCBT</th>
            <th>Số ngày YCBT</th>
            <th>Số tiền được BT</th>
            <th>Thời gian giải quyết</th>
            <th>Xuất PDF</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($bt) && !empty($bt)) {
            foreach ($bt as $index) {
                $id = $index->id;
                $OD_id = $index->OD_id;
                $loai_benh = $index->loai_benh;
                $loai_benh_txt = get_ma_benh($loai_benh);
                $cn_dbh = $index->congty_canhan;
                $parent_id = $index->parent_id;
                $phongban = $index->phongban;
                $goi_bh_dm = $index->goi_bh_dm;
                $so_ngay_ycbt = $index->so_ngay_ycbt;
                //$price = $product->price != 0 ? get_price_in_vnd($product->price) . ' ₫' : get_price_in_vnd($product->price);        
                $so_tien_ycbt = get_price_in_vnd($index->so_tien_ycbt);
                $so_ngay_ycbt = $index->so_ngay_ycbt;
                $so_tien_dbt = get_price_in_vnd($index->so_tien_dbt);
                $ngay_xayra_tonthat = $index->ngay_xayra_tonthat;
                $ngay_dieutri = $index->ngay_dieutri;
                $ngay_ycbt = $index->ngay_ycbt;
                $ngay_ycbt_txt = date('d/m/Y', strtotime($ngay_ycbt));
                $ngay_giaiquyet = $index->ngay_giaiquyet;
                $ngay_giaiquyet_txt = date('d/m/Y', strtotime($ngay_giaiquyet));
                $ngay_dieutri = $index->ngay_dieutri;
                $ngay_dieutri_txt = date('d/m/Y', strtotime($ngay_dieutri));
                $ngay_xayra_tonthat = $index->ngay_xayra_tonthat;
                $ngay_xayra_tonthat_txt = date('d/m/Y', strtotime($ngay_xayra_tonthat));
                //end
                $mst_cmt = $index->mst_cmt;
                $order_status = $index->order_status;
                $order_status_txt = get_bt_status($order_status);
                $tinhtrang_hoso = $index->tinhtrang_hoso;
                $chungtu_bosung = $index->chungtu_bosung;
                $ngay_bosung_hoso = $index->ngay_bosung_hoso;
                $ngay_duyet_bt = $index->ngay_duyet_bt;
                $ql_bh = $index->ql_bh;
                //$ql_bh_txt = ($ql_bh == NOITRU) ? 'Nội trú' : 'Ngoại trú';
                $ql_bh_txt = get_qlbh($ql_bh);
                $ngay_xayra_tonthat = $index->ngay_xayra_tonthat;
                $ndbh_quanhe = $index->ndbh_quanhe;
                $ndbh_quanhe_txt = get_ndbh_quanhe($ndbh_quanhe);
                $phuongthuc_bt = $index->phuongthuc_bt;
                $phuongthuc_bt_txt = get_phuongthuc_bt($ndbh_quanhe);
                $ngay_nhan_hoso = $index->ngay_nhan_hoso;
                $ngay_xuly_bt = $index->ngay_xuly_bt;
                $ngay_thongbao_kh = $index->ngay_thongbao_kh;
                $chuandoan_benh = $index->chuandoan_benh;
                $OD_ma_benh = $index->OD_ma_benh;
                $so_tien_thanhtoan_csyt = $index->so_tien_thanhtoan_csyt;
                $so_tien_tuchoi = $index->so_tien_tuchoi;
                $lydo_khong_bt = $index->lydo_khong_bt;
                $ghichu_thanhtoan = $index->ghichu_thanhtoan;
                $hinhthuc_nhantien_bt = $index->hinhthuc_nhantien_bt;
                $OD_ngay_thanhtoan = $index->OD_ngay_thanhtoan;
                $goi_gia_tien = ($index->goi_gia_tien);
                $csyt = $index->csyt;
                $ten_csyt = $index->ten_csyt;
                $ten_dn_bbh = $index->ten_dn_bbh;
                $hoso_scan = $index->hoso_scan;
                ?>
                <tr>
                    <td><?php echo 'HSBT-' . $OD_id ?></td>
                    <td><?php echo $cn_dbh ?></td>
                    <td>
                        <?php
                        //echo $parent_id.'???';
                        if ($parent_id > 0) {
                            if (isset($dn) && !empty($dn)) {
                                foreach ($dn as $index) {
                                    $dn_ten = $index->congty_canhan;
                                    $dn_id = $index->id;
                                    if ($dn_id == $parent_id) {
                                        echo $dn_ten;
                                        break;
                                    }
                                }
                            }
                        } else {
                            echo '';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($parent_id > 0) {
                            if (isset($a_phongban) && !empty($a_phongban)) {
                                foreach ($a_phongban as $index) {
                                    $pb_name = $index->name;
                                    $pb_id = $index->id;
                                    if ($pb_id == $phongban) {
                                        echo $pb_name;
                                        break;
                                    }
                                }
                            }
                        } else {
                            echo '';
                        }
                        ?>
                    </td>
                    <td><?php echo $mst_cmt ?></td>
                    <td><?php echo $order_status_txt ?></td>
                    <td><?php echo $tinhtrang_hoso ?></td>
                    <td><?php echo $chungtu_bosung ?></td>
                    <td><?php echo $ngay_bosung_hoso ?></td>
                    <td><?php echo $ngay_duyet_bt ?></td>
                    <td><?php echo $ql_bh_txt ?></td>
                    <td><?php echo $ngay_xayra_tonthat ?></td>
                    <td><?php echo $ndbh_quanhe_txt ?></td>
                    <td><?php echo $phuongthuc_bt_txt ?></td>
                    <td><?php echo $ngay_nhan_hoso ?></td>
                    <td><?php echo $ngay_xuly_bt ?></td>
                    <td><?php echo $ngay_thongbao_kh ?></td>
                    <td><?php echo $chuandoan_benh ?></td>
                    <td><?php echo $OD_ma_benh ?></td>
                    <td><?php echo $so_tien_thanhtoan_csyt ?></td>
                    <td><?php echo $so_tien_dbt ?></td>
                    <td><?php echo $so_tien_tuchoi ?></td>
                    <td><?php echo $lydo_khong_bt ?></td>
                    <td><?php echo $hinhthuc_nhantien_bt ?></td>
                    <td><?php echo $ghichu_thanhtoan ?></td>
                    <td><?php echo $OD_ngay_thanhtoan ?></td>
                    <td><?php echo $goi_gia_tien ?></td>
                    <!--<td><?php // echo $ten_csyt ?></td>-->
                    <td><?php echo $ten_dn_bbh ?></td>
                    <td><?php echo $hoso_scan ?></td>
                    <!--end-->
                    <td><?php echo $ngay_xayra_tonthat_txt ?></td>
                    <td><?php echo $ngay_dieutri_txt ?></td>
                    <td><?php echo $ten_csyt ?></td>
                    <td><?php echo $loai_benh_txt ?></td>
                    <!--<td><?php // echo $ql_bh_txt ?></td>-->
                    <td><?php echo $ngay_ycbt_txt ?></td>
                    <td><?php echo $so_tien_ycbt ?></td>
                    <td><?php echo $so_ngay_ycbt ?></td>
                    <td><?php echo $so_tien_dbt ?></td>
                    <td><?php echo $ngay_giaiquyet_txt ?></td>
                    <td>
                        <a href="#">
                            Xuất PDF
                        </a>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="10" class="text-danger">Không có dữ liệu.</td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<!--</div>-->
<div class="paging">
    <?php // echo $this->pagination->create_links(); ?>
</div>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>