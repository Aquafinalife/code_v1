<?php
$ss_kh_email = $this->phpsession->get('ss_kh_email');
$ss_kh_username = $this->phpsession->get('ss_kh_username');
$ss_kh_name = $this->phpsession->get('ss_kh_name');
$ss_kh_id = $this->phpsession->get('ss_kh_id');
$ss_kh_phanloai_kh = $this->phpsession->get('ss_kh_phanloai_kh');
$ss_kh_phanloai_kh_txt = get_customers_type($ss_kh_phanloai_kh);
$ss_kh_fullname = $this->phpsession->get('ss_kh_fullname');
$ss_kh_mst_cmt = $this->phpsession->get('ss_kh_mst_cmt');
$ss_kh_diachi = $this->phpsession->get('ss_kh_diachi');
$ss_kh_ngaycap = $this->phpsession->get('ss_kh_ngaycap');
$ss_kh_noicap = $this->phpsession->get('ss_kh_noicap');
$ss_kh_dienthoai = $this->phpsession->get('ss_kh_dienthoai');
$ss_kh_didong = $this->phpsession->get('ss_kh_didong');
$ss_kh_phanloai_kh1 = $this->phpsession->get('ss_kh_phanloai_kh1');
$ss_kh_phanloai_kh1_txt = get_customers_type1($ss_kh_phanloai_kh1);
//end
?>
<table id="table-edit_" class="table table-bordered table-hover">
    <h3>Thông tin khách hàng:</h3>
    <thead>
        <tr>
            <th>Options</th>
            <th>Value</th>
            <th>Options</th>
            <th>Value</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Tên khách hàng</td>
            <td><?php echo $ss_kh_name ?></td>
            <td>CMT/Hộ chiếu</td>
            <td><?php echo $ss_kh_mst_cmt ?></td>
        </tr>
        <tr>
            <td>Mã khách hàng</td>
            <td><?php echo 'MKH-' . $ss_kh_id ?></td>            
            <td>Ngày cấp</td>
            <td><?php echo $ss_kh_ngaycap ?></td>
        </tr>
        </tr>
        <tr>
            <td>Hợp đồng Bảo Hiểm</td>
            <td><?php echo 'FTC' . $ss_kh_id ?></td>
            <td>Nơi cấp</td>
            <td><?php echo $ss_kh_noicap ?></td>
        </tr>
        </tr>
        <tr>
            <td>Địa chỉ</td>
            <td><?php echo $ss_kh_diachi ?></td>
            <td>Loại khách hàng</td>
            <td><?php echo $ss_kh_phanloai_kh1_txt ?></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><?php echo $ss_kh_email ?></td>
        </tr>
        <tr>
            <td>Điện thoại</td>
            <td><?php echo $ss_kh_dienthoai ?></td>
        </tr>
        <tr>
            <td>Di động</td>
            <td><?php echo $ss_kh_didong ?></td>
        </tr>
<!--        <tr>
            <td>AAA</td>
            <td><?php echo $ss_kh_name ?></td>
        </tr>-->
    </tbody>
</table>
<br><br>
<table id="_table-edit" class="table table-bordered table-hover">
    <h3>Chi tiết bồi thường:</h3>
    <thead>
        <tr>
            <th>Mã số HSBT</th>
            <th>Mã bệnh</th>
            <th>Ngày YCBT</th>
            <th>Số tiền YCBT</th>
            <th>Số ngày YCBT</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($bt) && !empty($bt)) {
            foreach ($bt as $index) {
                $id = $index->id;
                $loai_benh = $index->loai_benh;
                $loai_benh_txt= get_ma_benh($loai_benh);
                $ngay_yc_bt = $index->ngay_yc_bt;
                $so_tien_ycbt = $index->so_tien_ycbt;
                $so_ngay_ycbt = $index->so_ngay_ycbt;
                ?>
                <tr>
                    <td><?php echo 'HSBT-' . $id ?></td>
                    <td><?php echo $loai_benh_txt ?></td>
                    <td><?php echo $ngay_yc_bt ?></td>
                    <td><?php echo $so_tien_ycbt ?></td>
                    <td><?php echo $so_ngay_ycbt ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
<br>
<table id="table-edit" class="table table-bordered table-hover hidden"></table>