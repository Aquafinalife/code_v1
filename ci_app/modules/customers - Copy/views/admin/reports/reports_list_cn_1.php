<div class="form_content _row" style="float: left;width: 100%">
    <?php
    $key = isset($_GET['key']) ? $_GET['key'] : '';
    $GET_phanloai_kh = isset($_GET['phanloai_kh']) ? $_GET['phanloai_kh'] : '';
    $GET_dn = isset($_GET['dn']) ? $_GET['dn'] : '';
    $GET_cn = isset($_GET['cn']) ? $_GET['cn'] : '';
    $cl_dn = ($GET_dn > 0) ? 'js-example-disabled-results' : '';
    $cl_cn = ($GET_cn > 0) ? 'js-example-disabled-results' : '';
    $a_customers_type = get_a_customers_type();
    ?>
    <div class="filter_form" style="padding-top: 15px;">
        <div class="col-sm-12 _text-center">
            <form class="form-inline" method="get" action="<?php echo site_url(REPORTS_URL) ?>">
                <div class="form-group">
                    <label for="exampleInputName2">Loại KH</label>
                    <select parent_id="0" selectbox_name="dn" selectbox_id="filter_doanhnghiep_bc" name="phanloai_kh" class="filter_phanloai_kh_bc">
                        <option value="">Chọn loại khách hàng</option>
                        <?php
                        if (!empty($a_customers_type)) {
                            foreach ($a_customers_type as $k => $v) {
                                $selected = ($k == $GET_phanloai_kh) ? 'selected="selected"' : '';
                                ?>
                                <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>                
                <div class="form-group box_filter_canhan_dbh">
                    <?php
                    if ($GET_phanloai_kh == CANHAN) {
                        ?>
                        <label for="exampleInputName2">Tìm theo CMT/Hộ chiếu</label>
                        <!--<input value="<?php // echo $key    ?>" name="key" type="text" class="form-control" id="exampleInputName2" placeholder="Nhập số CMT / Hộ Chiếu">-->
                        <select name="key" class="js-example-disabled-results">
                            <option value="">Điền Tên hoặc CMT</option>            
                            <?php
                            if (isset($ca_nhan_dbh) && !empty($ca_nhan_dbh)) {
                                $a_mst_cmt = array();
                                foreach ($ca_nhan_dbh as $index) {
                                    $mst_cmt = $index->mst_cmt;
                                    $congty_canhan = $index->congty_canhan;
                                    $selected = ($key== $mst_cmt)?'selected="selected"':'';
                                    if (!in_array($mst_cmt, $a_mst_cmt)) {
                                        ?>
                                        <option <?php echo $selected?> value="<?php echo $mst_cmt ?>"><?php echo $mst_cmt . ' -- ' . $congty_canhan ?></option>
                                        <?php
                                    }
                                    $a_mst_cmt[] = $mst_cmt;
                                }
                            }
                            ?>
                        </select>
                        <?php
                    }
                    ?>
                </div>
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
            </form>
        </div>
    </div>


    <div class="page_content col-sm-12">
        <?php
        if (isset($kh_mbh) && !empty($kh_mbh)) {
            $congty_canhan = $kh_mbh->congty_canhan;
            $phanloai_kh = $kh_mbh->phanloai_kh;
            $phanloai_kh_txt = ($phanloai_kh == CANHAN) ? 'Cá nhân' : 'Doanh nghiệp';
            $hopdong_baohiem = $kh_mbh->hopdong_baohiem;
            $ngaysinh = $kh_mbh->ngaysinh;
            $diachi = $kh_mbh->diachi;
            $email = $kh_mbh->email;
            $mst_cmt = $kh_mbh->mst_cmt;
            $ngaycap = $kh_mbh->ngaycap;
            $noicap = $kh_mbh->noicap;
            $dienthoai = $kh_mbh->dienthoai;
            $didong = $kh_mbh->didong;
            $phanloai_kh1 = $kh_mbh->phanloai_kh1;
            $phanloai_kh1_txt = ($kh_mbh->phanloai_kh1 == VIP) ? 'Vip' : 'Thường';
            $gioitinh = $kh_mbh->gioitinh;
            if ($congty_canhan == CANHAN) {
                $gioitinh_txt = get_gender($gioitinh);
            }
            ?>
            <div class="col-sm-6 col-xs-12">
                <h2>Thông Tin Bên Mua Bảo Hiểm</h2>
                <div class="page_content1">
                    <div class="list_item1">
                        <label>Bên mua:</label>
                        <span><?php echo $congty_canhan ?></span>
                    </div>
                    <div class="list_item1">
                        <label>Loại khách hàng:</label>
                        <span><?php echo $phanloai_kh_txt ?></span>
                    </div>
                    <div class="list_item1">
                        <label>Hợp đồng BH:</label>
                        <span><?php echo $hopdong_baohiem ?></span>
                    </div>
                    <div class="list_item1">
                        <label>Địa chỉ:</label>
                        <span><?php echo $diachi ?></span>
                    </div>
                    <div class="list_item1">
                        <label>Email:</label>
                        <span><?php echo $email ?></span>
                    </div>
                    <div class="list_item1">
                        <label>Điện thoại:</label>
                        <span><?php echo $dienthoai ?></span>
                    </div>
                    <div class="list_item1">
                        <label>Di động:</label>
                        <span><?php echo $didong ?></span>
                    </div>
                    <div class="list_item1">
                        <label>CMT / Hộ Chiếu:</label>
                        <span><?php echo $mst_cmt ?></span>
                    </div>
                    <div class="list_item1">
                        <label>Ngày cấp:</label>
                        <span><?php echo $ngaycap ?></span>
                    </div>
                    <div class="list_item1">
                        <label>Nơi cấp:</label>
                        <span><?php echo $noicap ?></span>
                    </div>
                    <div class="list_item1">
                        <label>Phân loại KH:</label>
                        <span><?php echo $phanloai_kh1_txt ?></span>
                    </div>
                    <?php
                    if ($congty_canhan == CANHAN) {
                        ?>
                        <div class="list_item1">
                            <label>Giới tính:</label>
                            <span><?php echo $gioitinh_txt ?></span>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <?php
        } else {
            ?>
            <div class="col-sm-12 text-center" style="margin-top: 20px">
                Kết quả tìm kiếm: 0
            </div>
            <?php
        }
        ?>
        <?php
        if ($GET_phanloai_kh == CANHAN) {
            ?>
            <div class="col-sm-6 col-xs-12">
                <?php
                if (isset($kh_dbh) && !empty($kh_dbh)) {
                    $congty_canhan = $kh_dbh->congty_canhan;
                    $phanloai_kh = $kh_dbh->phanloai_kh;
                    $phanloai_kh_txt = ($phanloai_kh == CANHAN) ? 'Cá nhân' : 'Doanh nghiệp';
                    $hopdong_baohiem = $kh_dbh->hopdong_baohiem;
                    $ngaysinh = $kh_dbh->ngaysinh;
                    $diachi = $kh_dbh->diachi;
                    $email = $kh_dbh->email;
                    $mst_cmt = $kh_dbh->mst_cmt;
                    $ngaycap = $kh_dbh->ngaycap;
                    $noicap = $kh_dbh->noicap;
                    $dienthoai = $kh_dbh->dienthoai;
                    $didong = $kh_dbh->didong;
                    $phanloai_kh1 = $kh_dbh->phanloai_kh1;
                    $phanloai_kh1_txt = ($kh_dbh->phanloai_kh1 == VIP) ? 'Vip' : 'Thường';
                    $gioitinh = $kh_dbh->gioitinh;

                    if ($phanloai_kh == CANHAN) {
                        $gioitinh_txt = get_gender($gioitinh);
                    }
                    ?>
                    <h2>Thông Tin Người Được Bảo Hiểm</h2>
                    <div class="page_content1">
                        <div class="list_item1">
                            <label>Người được BH:</label>
                            <span><?php echo $congty_canhan ?></span>
                        </div>
                        <div class="list_item1">
                            <label>Loại khách hàng:</label>
                            <span><?php echo $phanloai_kh_txt ?></span>
                        </div>
                        <div class="list_item1">
                            <label>Hợp đồng BH:</label>
                            <span><?php echo $hopdong_baohiem ?></span>
                        </div>
                        <div class="list_item1">
                            <label>Địa chỉ:</label>
                            <span><?php echo $diachi ?></span>
                        </div>
                        <div class="list_item1">
                            <label>Email:</label>
                            <span><?php echo $email ?></span>
                        </div>
                        <div class="list_item1">
                            <label>Điện thoại:</label>
                            <span><?php echo $dienthoai ?></span>
                        </div>
                        <div class="list_item1">
                            <label>Di động:</label>
                            <span><?php echo $didong ?></span>
                        </div>
                        <div class="list_item1">
                            <label>CMT / Hộ Chiếu:</label>
                            <span><?php echo $mst_cmt ?></span>
                        </div>
                        <div class="list_item1">
                            <label>Ngày cấp:</label>
                            <span><?php echo $ngaycap ?></span>
                        </div>
                        <div class="list_item1">
                            <label>Nơi cấp:</label>
                            <span><?php echo $noicap ?></span>
                        </div>
                        <div class="list_item1">
                            <label>Phân loại KH:</label>
                            <span><?php echo $phanloai_kh1_txt ?></span>
                        </div>
                        <?php
                        if ($phanloai_kh == CANHAN) {
                            ?>
                            <div class="list_item1">
                                <label>Giới tính:</label>
                                <span><?php echo $gioitinh_txt ?></span>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="col-sm-12"><hr></div>
            <div class="col-sm-12">            
                <?php
                if (isset($goi_bh) && !empty($goi_bh)) {

                    $name = $goi_bh->name;
                    $gia_tien = $goi_bh->gia_tien;
                    $TV_TTTBVV_TN = $goi_bh->TV_TTTBVV_TN;
                    $TCL_NN = $goi_bh->TCL_NN;
                    $tcl_nn_sn = $goi_bh->tcl_nn_sn;
                    $CPYT_TN = $goi_bh->CPYT_TN;
                    $TV_TTTBVV_OB = $goi_bh->TV_TTTBVV_OB;
                    $DTNT_OB = $goi_bh->DTNT_OB;
                    $dtnt_dbh = $goi_bh->dtnt_dbh;
                    $dtnt_tvpn = $goi_bh->dtnt_tvpn;
                    $dtnt_tvpn_sn = $goi_bh->dtnt_tvpn_sn;
                    $dtnt_tgpn = $goi_bh->dtnt_tgpn;
                    $dtnt_tgpn_sn = $goi_bh->dtnt_tgpn_sn;
                    $dtnt_cppt = $goi_bh->dtnt_cppt;
                    $dtnt_tk_nv = $goi_bh->dtnt_tk_nv;
                    $dtnt_sk_nv = $goi_bh->dtnt_sk_nv;
                    $dtnt_cpyt_tn = $goi_bh->dtnt_cpyt_tn;
                    $dtnt_cpyt_tn_sn = $goi_bh->dtnt_cpyt_tn_sn;
                    $dtnt_tcvp = $goi_bh->dtnt_tcvp;
                    $dtnt_tcvp_sn = $goi_bh->dtnt_tcvp_sn;
                    $dtnt_tcmt = $goi_bh->dtnt_tcmt;
                    $dtnt_xct = $goi_bh->dtnt_xct;
                    $THAISAN_QLNT = $goi_bh->THAISAN_QLNT;
                    $tsqlnt_ktdk = $goi_bh->tsqlnt_ktdk;
                    $tsqlnt_st = $goi_bh->tsqlnt_st;
                    $tsqlnt_sm = $goi_bh->tsqlnt_sm;
                    $tsqlnt_dn = $goi_bh->tsqlnt_dn;
                    $DTNGT_OB = $goi_bh->DTNGT_OB;
                    $dtngt_dbh = $goi_bh->dtngt_dbh;
                    $dtngt_st1lk = $goi_bh->dtngt_st1lk;
                    $dtngt_slk = $goi_bh->dtngt_slk;
                    $dtngt_nk = $goi_bh->dtngt_nk;
                    $dtngt_cvr = $goi_bh->dtngt_cvr;
                    $THAISAN_MDL = $goi_bh->THAISAN_MDL;
                    $tsmdl_dbh = $goi_bh->tsmdl_dbh;
                    $tsmdl_ktdk = $goi_bh->tsmdl_ktdk;
                    $tsmdl_st = $goi_bh->tsmdl_st;
                    $tsmdl_sm = $goi_bh->tsmdl_sm;
                    $tsmdl_dn = $goi_bh->tsmdl_dn;
                    $NHAKHOA_MDL = $goi_bh->NHAKHOA_MDL;
                    $nkmdl_dbh = $goi_bh->nkmdl_dbh;
                    $nkmdl_cb = $goi_bh->nkmdl_cb;
                    $nkmdl_db = $goi_bh->nkmdl_db;
                    ?>
                    <div class="col-sm-12 text-center"><h2>Thông Tin Gói Bảo Hiểm</h2></div>
                    <div class="col-sm-6 col-xs-12" style="padding-left: 0">
                        <div class="page_content1">
                            <div class="list_item1">
                                <label>Tên gói BH:</label>
                                <span><?php echo $name ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Giá tiền gói BH:</label>
                                <span><?php echo $gia_tien ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Tử vong, TTTBVV (Do tai nạn):</label>
                                <span><?php echo $TV_TTTBVV_TN ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Trợ cấp lương ngày nghỉ:</label>
                                <span><?php echo $TCL_NN ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Số ngày:</label>
                                <span><?php echo $tcl_nn_sn ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Chi phí y tế do tai nạn:</label>
                                <span><?php echo $CPYT_TN ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Tử vong, TTTBVV (Do ốm bệnh):</label>
                                <span><?php echo $TV_TTTBVV_OB ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Điều trị nội trú (Do ốm bệnh):</label>
                                <span><?php echo $DTNT_OB ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Đồng bảo hiểm:</label>
                                <span><?php echo $dtnt_dbh ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Tiền viện phí ngày:</label>
                                <span><?php echo $dtnt_tvpn ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Số ngày:</label>
                                <span><?php echo $dtnt_tvpn_sn ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Tiền giường, phòng / ngày:</label>
                                <span><?php echo $dtnt_tgpn ?></span>
                            </div>                    
                            <div class="list_item1">
                                <label>Số ngày:</label>
                                <span><?php echo $dtnt_tgpn_sn ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Chi phí phẫu thuật:</label>
                                <span><?php echo $dtnt_cppt ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Quyền lợi khám trước khi nhập viện:</label>
                                <span><?php echo $dtnt_tk_nv ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Quyền lợi khám sau khi nhập viện:</label>
                                <span><?php echo $dtnt_sk_nv ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Chi phí y tá tại nhà:</label>
                                <span><?php echo $dtnt_cpyt_tn ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Số ngày:</label>
                                <span><?php echo $dtnt_cpyt_tn_sn ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Trợ cấp viện phí:</label>
                                <span><?php echo $dtnt_tcvp ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Số ngày:</label>
                                <span><?php echo $dtnt_tcvp_sn ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Trợ cấp mai táng:</label>
                                <span><?php echo $dtnt_tcmt ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Xe cứu thương:</label>
                                <span><?php echo $dtnt_xct ?></span>
                            </div>                    

                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12" style="padding-left: 0">
                        <div class="page_content1">
                            <div class="list_item1">
                                <label>THAI SẢN (Trong quyền lợi nội trú):</label>
                                <span><?php echo $THAISAN_QLNT ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Chi phí khám thai định kỳ:</label>
                                <span><?php echo $tsqlnt_ktdk ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Sinh thường:</label>
                                <span><?php echo $tsqlnt_st ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Sinh mổ:</label>
                                <span><?php echo $tsqlnt_sm ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Dưỡng nhi:</label>
                                <span><?php echo $tsqlnt_dn ?></span>
                            </div>
                            <div class="list_item1">
                                <label>ĐIỀU TRỊ NGOẠI TRÚ (Do ốm bệnh):</label>
                                <span><?php echo $DTNGT_OB ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Đồng bảo hiểm:</label>
                                <span><?php echo $dtngt_dbh ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Số tiền cho 1 lần khám:</label>
                                <span><?php echo $dtngt_st1lk ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Số lần khám:</label>
                                <span><?php echo $dtngt_slk ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Nha khoa:</label>
                                <span><?php echo $dtngt_nk ?></span>
                            </div>                    
                            <div class="list_item1">
                                <label>Cạo vôi răng:</label>
                                <span><?php echo $dtngt_cvr ?></span>
                            </div>
                            <div class="list_item1">
                                <label>THAI SẢN (Mua độc lập):</label>
                                <span><?php echo $THAISAN_MDL ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Đồng bảo hiểm:</label>
                                <span><?php echo $tsmdl_dbh ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Chi phí khám thai định kỳ:</label>
                                <span><?php echo $tsmdl_ktdk ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Sinh thường:</label>
                                <span><?php echo $tsmdl_st ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Sinh mổ:</label>
                                <span><?php echo $tsmdl_sm ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Dưỡng nhi:</label>
                                <span><?php echo $tsmdl_dn ?></span>
                            </div>
                            <div class="list_item1">
                                <label>NHA KHOA (Mua độc lập):</label>
                                <span><?php echo $NHAKHOA_MDL ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Đồng bảo hiểm:</label>
                                <span><?php echo $nkmdl_dbh ?></span>
                            </div>
                            <div class="list_item1">
                                <label>Nha khoa cơ bản :</label>
                                <span><?php echo $nkmdl_cb ?></span>
                            </div>                    
                            <div class="list_item1">
                                <label>Nha khoa đặc biệt:</label>
                                <span><?php echo $nkmdl_db ?></span>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <!--</div>-->
            </div>
            <?php
        }
        ?>
        <?php
        if ($GET_phanloai_kh == DOANHNGHIEP && $GET_dn > 0) {
            ?>
            <div class="col-sm-6 col-xs-12">
                <h2>Thông Tin Khách Hàng Được Mua BH</h2>
                <div class="list_item1">
                    <label>Tổng số người được mua BH::</label>
                    <span><?php
                        if (!empty($kh_dbh)) {
                            echo count($kh_dbh);
                        } else {
                            echo 0;
                        }
                        ?></span>
                </div>
                <div class="list_item1">
                    <label>Tổng giá trị các gói BH đã mua::</label>
                    <span><?php
                        if (!empty($goi_bh)) {
                            $price = 0;
                            foreach ($goi_bh as $index) {
                                $price += $index->gia_tien;
                            }
                            echo $price;
                        } else {
                            echo 0;
                        }
                        ?></span>
                </div>
            </div>
            <?php
        }
        ?>  
    </div>
</div>