<?php
$this->load->view('admin/orders_nav');
echo form_open('', array('id' => 'submit_form'), array('id' => 0, 'from_list' => TRUE));
echo isset($uri) ? form_hidden('uri', $uri) : NULL;
echo form_hidden('back_url', ORDER_ADMIN_BASE_URL);
echo form_close();
?>
<div class="form_content">
    <?php // $this->load->view('admin/filter_form'); ?>
    <div class="filter">
        <?php echo form_open('dashboard/orders/'); ?>
        <!--Ngôn ngữ:--> <?php // if (isset($lang_combobox)) echo $lang_combobox;  ?>
        Tên khách hàng: <?php echo form_input(array('name' => 'search', 'id' => 'search', 'maxlength' => '256', 'value' => $search, 'style' => 'width: 125px;')); ?>

        Trạng thái bồi thường: <?php if (isset($combo_order)) echo $combo_order; ?>

        Từ ngày: <?php echo form_input(array('id' => 'start_day_date', 'name' => 'start_date', 'size' => '50', 'maxlength' => '10', 'value' => $start_date, 'style' => 'width: 125px;')); ?>

        Đến ngày: <?php echo form_input(array('id' => 'end_day_date', 'name' => 'end_date', 'size' => '50', 'maxlength' => '10', 'value' => $end_date, 'style' => 'width: 125px;')); ?>

        <input type="submit" name="submit" value="Tìm kiếm" class="btn" />
        <span class="fright"><a class="button" href="/dashboard/orders/export"><em>&nbsp;</em>Xuất excel</a></span>
        <?php echo form_close(); ?>
    </div>
    <!--end-->
    <table class="list" style="width: 100%; margin-bottom: 10px;">
        <?php $this->load->view('powercms/message'); ?>
        <tr>
            <th class="left" style="width: 5%">MÃ BT</th>
            <th class="left" style="width: 20%">KHÁCH HÀNG</th>
            <th class="left" style="width: 20%">CMT</th>
            <th class="left" style="width: 20%">NGUYÊN NHÂN</th>
            <th class="center" style="width: 10%">Tiền Yêu cầu BT</th>
            <th class="center" style="width: 10%">Tiền CHẤP NHẬN BT</th>
            <th class="center" style="width: 10%">NGÀY YCBT</th>
            <th class="center" style="width: 10%">TRẠNG THÁI</th>
            <th class="center" style="width: 5%">CHỨC NĂNG</th>
        </tr>

        <?php
        if (isset($orders) && !empty($orders)) {
//            echo '<pre>';
//            print_r($orders);
//            die;
            $stt = 0;
            foreach ($orders as $index):
                $created_date = get_vndate_string($index->create_time);
                $updated_date = get_vndate_string($index->update_time);
//        $check          = $index->status == STATUS_ACTIVE ? 'checked' : '';
                $style = $stt++ % 2 == 0 ? 'even' : 'odd';
                $price = $index->total != 0 ? get_price_in_vnd($index->total) . ' đ' : get_price_in_vnd($index->total);
                $price_cnbt = $index->price_cnbt != 0 ? get_price_in_vnd($index->price_cnbt) . ' đ' : get_price_in_vnd($index->price_cnbt);
                $loai_benh = $index->loai_benh;
                if ($loai_benh == 'gh_tv_tn') {
                    $loai_benh = 'TỬ VONG, TTTBVV do tai nạn';
                } elseif ($loai_benh == 'luong_nn') {
                    $loai_benh = 'Trợ cấp lương ngày nghỉ';
                }
                //end

                $id = $index->id;
                $congty_canhan = $index->congty_canhan;
                $mst_cmt = $index->mst_cmt;
                $so_tien_ycbt = $index->so_tien_ycbt;
                $ngay_yc_bt = $index->ngay_yc_bt;
                ?>
                <tr class="<?php echo $style ?>">
                    <td><?php echo '#' . $index->id; ?></td>
                    <td><?php echo $congty_canhan ?></td>
                    <td style="white-space:nowrap;"><?php echo $mst_cmt ?></td>
                    <td style="white-space:nowrap;"><?php echo $loai_benh; ?></td>
                    <td class="center" style="white-space:nowrap;"><?php echo $so_tien_ycbt ?></td>
                    <td style="white-space:nowrap;"><?php echo $price_cnbt ?></td>
                    <td class="center" style="white-space:nowrap;"><?php echo $ngay_yc_bt ?></td>
                    <td class="center" style="white-space:nowrap;"><?php echo get_status_orders_icon($index->order_status) ?></td>

                    <td class="center" style="white-space:nowrap;" class="action">        
                        <a class="edit" title="Xem chi tiết bồi thường này" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $index->id ?>, '<?php echo ORDERS_ADMIN_EDIT_URL; ?>', 'edit');"><em>&nbsp;</em></a>
                        <a class="del" title="Xóa bồi thường" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $index->id ?>, '<?php echo ORDERS_ADMIN_DELETE_URL; ?>', 'delete');"><em>&nbsp;</em></a>
                        <!--<a class="excel" title="Xuất excel" href="javascript:void(0);" onclick="submit_action_admin(<?php // echo $index->id     ?>,'<?php // echo ORDERS_ADMIN_EXPORT;     ?>','export');"><em>&nbsp;</em></a>-->
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php } ?>
        <?php $left_page_links = 'Trang ' . $page . ' / ' . $total_pages . ' (<span>Tổng số: ' . $total_rows . ' bồi thường</span>)'; ?>
        <tr class="list-footer">
            <th colspan="8">
        <div style="float:left; margin-top: 9px;"><?php echo $left_page_links; ?></div>
        <div style="float:right;" class="pagination"><?php if (isset($page_links) && $page_links !== '') echo $page_links; ?></div>
        </th>
        </tr>
    </table>
    <br class="clear"/>&nbsp;
</div>
