<?php
$a_customers_type = get_a_customers_type();
$this->load->view('admin/products/product_nav');
echo form_open('', array('id' => 'submit_form'), array('id' => 0, 'from_list' => TRUE));
echo isset($uri) ? form_hidden('uri', $uri) : NULL;
echo form_hidden('back_url', PRODUCTS_ADMIN_BASE_URL);
echo form_close();
//    echo '<pre>';
//    var_dump($_SESSION);
//    echo '</pre>';
?>
<div class="form_content">

    <div class="filter">   
        <?php
        $a_customers_type = get_a_customers_type();
        $GET_phanloai_kh = isset($_GET['phanloai_kh']) ? $_GET['phanloai_kh'] : '';
        $GET_key = isset($_GET['key']) ? $_GET['key'] : '';
        ?>
        <table style="width: 100%;">
            <tbody>
                <tr>
                    <td style="
                        width: 700px;
                        ">
                        <form method="get" action ="<?php echo PRODUCTS_ADMIN_BASE_URL; ?>">
                            <!--<span class="display_status">Ngôn ngữ: <?php // if (isset($lang_combobox)) echo $lang_combobox;     ?></span>-->
                            <div class="list_box_filter">
                                Từ khóa: <input name="key" value="<?php echo str_replace('\\', '', $GET_key); ?>" />
                            </div>
                            <div class="list_box_filter">
                                Loại KH: 
                                <select parent_id="0" selectbox_name="dn" selectbox_id="filter_doanhnghiep" name="phanloai_kh" class="filter_phanloai_kh js-example-disabled-results">
                                    <option value="">Tất cả khách hàng</option>
                                    <?php
                                    if (!empty($a_customers_type)) {
                                        foreach ($a_customers_type as $k => $v) {
                                            $selected = ($k == $GET_phanloai_kh) ? 'selected="selected"' : '';
                                            ?>
                                            <option <?php echo $selected ?> value="<?php echo $k ?>"><?php echo $v ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="list_box_filter box_filter_doanhnghiep">
                                <?php
                                if (isset($selectbox_dn)) {
                                    echo $selectbox_dn;
                                }
                                ?>
                            </div>
                            <div class="list_box_filter box_filter_chinhanh">
                                <?php
                                if (isset($selectbox_cn)) {
                                    echo $selectbox_cn;
                                }
                                ?>
                            </div>
                            <?php // if (isset($categories_combo)) echo $categories_combo; ?>

                            <?php // echo form_input(array('id' => 'start_day_date', 'name' => 'start_date', 'size' => '50', 'maxlength' => '10', 'value' => $start_date, 'style' => 'width: 125px;')); ?>

                            <?php // echo form_input(array('id' => 'end_day_date', 'name' => 'end_date', 'size' => '50', 'maxlength' => '10', 'value' => $end_date, 'style' => 'width: 125px;')); ?>

                            <div class="list_box_filter">
                                <input type="submit" value="Tìm kiếm" class="btn"/>
                            </div>
                        </form>
                    </td>

                </tr>
            </tbody>
        </table>
    </div>


    <?php
//    if (isset($filter))
//        echo $filter;
    $this->load->view('admin/products/toggle_add_product_form');
    ?>

    <table class="list" style="width: 100%; margin-bottom: 10px;">
        <?php $this->load->view('powercms/message'); ?>
        <tr>
            <th class="left" style="width: 5%" >Số hợp đồng</th>
            <th class="center" style="width: 5%">KHÁCH MUA</th>
            <th class="center" style="width: 5%">KĐHBH</th>
            <!--<th class="right" style="width: 10%">PHÍ CHUẨN</th>-->
            <!--<th class="left" style="width: 10%" >PHÍ PHẢI THU KH</th>-->
            <!--<th class="left" style="width: 5%">THU ĐƯỢC</th>-->
            <!--<th class="center" style="width: 5%">CÒN LẠI</th>-->

            <!--<th class="left" style="width: 10%">LOẠI HÌNH BẢO HIỂM</th>-->

            <th class="center" style="width: 5%">NGÀY SỬA</th>
            <!--<th class="center" style="width: 5%">NGƯỜI BÁN HÀNG</th>-->
            <!--<th class="center" style="width: 5%">TÌNH TRẠNG</th>-->
            <th class="center" style="width: 5%">CHỨC NĂNG</th>
        </tr>
        <?php
        if (isset($products)) {
//            echo '<pre>';
//            print_r($products);
//            die;
            $stt = 0;
            foreach ($products as $index => $product):
                $product_id = $product->id;
                $product_name = $product->product_name;
                $category = $product->category;
                $options = array();
                $options['custom_duoc_id'] = $product->custom_duoc_id;
                //$custom_duoc = $this->products_model->get_name_dh($options);
//                echo '<pre>';
//                print_r($custom_duoc);
//                die;

                $warning = strlen($product->image_name) == 0 ? '<img src="/powercms/images/warning.png"' : '';
                if (SLUG_ACTIVE == 0) {
                    $uri = '/' . url_title(trim($product->product_name), 'dash', TRUE) . '-ps' . $product->id;
                } else {
                    $uri = '/' . $product->slug;
                }
                $price = $product->price != 0 ? get_price_in_vnd($product->price) . ' đ' : get_price_in_vnd($product->price);
                $price_pt = $product->price_old != 0 ? get_price_in_vnd($product->price_old) . ' đ' : get_price_in_vnd($product->price_old);
                $price_real = $product->price_real != 0 ? get_price_in_vnd($product->price_real) . ' đ' : get_price_in_vnd($product->price_real);
                $product_unit = ($product->unit != NULL && $product->unit != '') ? $product->unit : '';
                $updated_date = get_vndate_string($product->updated_date);
                $created_date = get_vndate_string($product->created_date);
                $style = ($stt++ % 2 == 0) ? 'even' : 'odd';
                $image = '/images/products/smalls/' . ((strlen($product->image_name) == 0) ? 'no-product.png' : $product->image_name);
                $image = '<img src="' . $image . '" alt="' . $product->product_name . '" style="height: 20px;" />';
                $check = $product->status == STATUS_ACTIVE ? 'checked' : '';
//            $check_home     = $product->home == STATUS_ACTIVE ? 'checked' : '';
                $check_state = $product->state_id == STATE_ACTIVE ? 'checked' : '';
                $this_lang = ($product->lang <> DEFAULT_LANGUAGE) ? '/' . $product->lang : '';
                $creator_username = !empty($product->creator_username) ? $product->creator_username : 'admin';
                //end
                $kh_mbh = $product->kh_mbh;
                $kh_dbh = $product->kh_dbh;
                
                $kh_dbh_txt = $kh_mbh_txt= '';
                if (isset($kh) && !empty($kh)) {
                    foreach ($kh as $index) {
                        $kh_id = $index->id;
                        $congty_canhan = $index->congty_canhan;
                        if ($kh_id == $kh_dbh) {
                            $kh_dbh_txt = $congty_canhan;
                        }
                        if ($kh_id == $kh_mbh) {
                            $kh_mbh_txt = $congty_canhan;
                        }
                    }
                }
                ?>
                <tr class="<?php echo $style ?>">
                    <td style="width:1%;"><?php echo $product->sohopdong; ?></td>

                    <td class="left" style="word-wrap:break-word;"><?php echo $kh_mbh_txt; ?></td>
                    <td class="left" style="word-wrap:break-word;"><?php echo $kh_dbh_txt; ?></td>
                    <!--<td class="right"><span class="red"><?php // echo $price;  ?></span></td>-->
                    <!--<td class="left"><?php // echo $price_pt;      ?></span></td>-->
                    <!--<td class="center"><?php // echo $price_real;      ?></td>-->
                    <!--<td class="left" style="word-wrap:break-word;"><?php // echo $product->re_costs;      ?></td>-->
                    <!--<td class="left" style="word-wrap:break-word;"><?php // echo $category;  ?></td>-->
                    <td class="center"><?php echo $updated_date; ?></td>
                    <!--<td class="center" style="white-space:nowrap;"><?php // echo $creator_username;  ?></td>-->
                    <?php
                    $truong_phong = $this->phpsession->get('role_id');
                    if ($check_state == '' && $truong_phong == 4 || $truong_phong == 1 || $truong_phong == 5) {
                        ?>
                                        <!--<td class="center"><input type="checkbox" <?php echo $check_state; ?> onclick="change_state(<?php echo $product_id; ?>,'products')"></td>-->
                    <?php } else { ?>
                                        <!--<td class="center"><input onclick="return false;" onkeydown="return false;" type="checkbox" <?php echo $check_state; ?> onclick="change_state(<?php echo $product_id; ?>,'products')"></td>-->
                    <?php } ?>
                    <td class="center" style="white-space:nowrap;" class="action">
                        <a class="edit" title="Sửa thông tin sản phẩm" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $product_id ?>, '<?php echo PRODUCTS_ADMIN_EDIT_URL; ?>', 'edit');"><em>&nbsp;</em></a>
                        <a class="del" title="Xóa sản phẩm" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $product_id ?>, '<?php echo PRODUCTS_ADMIN_DELETE_URL; ?>', 'delete');"><em>&nbsp;</em></a>
                        <a class="up" title="Cập nhật (up) sản phẩm lên đầu trang" href="javascript:void(0);" onclick="submit_action_admin(<?php echo $product_id ?>, '<?php echo PRODUCTS_ADMIN_UP_URL; ?>', 'up');"><em>&nbsp;</em></a>
                    </td>
                </tr>

            <?php endforeach; ?>
        <?php } ?>
        <?php $left_page_links = 'Trang ' . $page . ' / ' . $total_pages . ' (<span>Tổng số: ' . $total_rows . ' sản phẩm</span>)'; ?>
        <tr class="list-footer">
            <th colspan="11">
        <div style="float:left; margin-top: 9px;"><?php echo $left_page_links; ?></div>
        <div style="float:right;" class="pagination"><?php if (isset($page_links) && $page_links !== '') echo $page_links; ?></div>
        </th>
        </tr>
    </table>
    <br class="clear"/>&nbsp;
</div>