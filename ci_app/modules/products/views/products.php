<ol class="breadcrumb">
    <li><a href="<?php echo get_base_url(); ?>" title="<?php echo DEFAULT_COMPANY; ?>"><?php echo __('IP_home_page'); ?></a></li>
    <?php if ($category_id <> 0) { ?><li><a href="<?php echo get_url_by_lang(get_language(), 'products'); ?>" title="Sản phẩm"><?php echo __('IP_products'); ?></a></li><?php } ?>
    <li class="active"><?php echo $category; ?></li>
</ol>
<div class="clearfix"></div>
<?php if(!empty($products)) { ?>
<h2 class="page-heading">
    <span class="page-heading-title"><?php echo $category; ?></span>
</h2>
<ul class="display-product-option">
    <li class="view-as-grid selected">
        <span>grid</span>
    </li>
    <li class="view-as-list">
        <span>list</span>
    </li>
</ul>
<!-- PRODUCT LIST -->
<ul class="row product-list grid">
    <?php 
    foreach($products as $key => $value){
        if(SLUG_ACTIVE==0){
            $uri = get_base_url() . url_title(trim($value->product_name), 'dash', TRUE) . '-ps' . $value->id;
        }else{
            $uri = get_base_url() . $value->slug;
        }
        $image = is_null($value->image_name) ? base_url().'images/no-image.png' : base_url().'images/products/thumbnails/'.$value->image_name;
        $value_name = limit_text($value->product_name, 50);
        $value_category = limit_text($value->category, 100);
        $summary = limit_text($value->summary, 150);
        $price = $value->price > 0 ? get_price_in_vnd($value->price) . ' ₫' : get_price_in_vnd($value->price);    
        $price_old = $value->price_old > 0 ? get_price_in_vnd($value->price_old) . ' ₫' : 0;
        $price_count = $value->price_old - $value->price;
        $price_discount = $price_count > 0 ? get_price_in_vnd($price_count) . ' ₫' : 0;
        $saleoff = ($price_count/$value->price_old)*100;
        $saleoff = round($saleoff,0);
        $code_id = '#'.$value->id;
        $tinhtrang = $value->state_id;
        if($tinhtrang == 1){
            $tinhtrang = 'Còn hàng';
        }else{
            $tinhtrang = 'Hết hàng';
        }
    ?>
    <li class="col-sx-12 col-sm-4">
        <div class="product-container">
            <div class="left-block">
                <a href="<?php echo $uri; ?>">
                    <img class="img-responsive" alt="<?php echo $value->product_name; ?>" title="<?php echo $value->product_name; ?>" src="<?php echo $image; ?>" />
                </a>
                <div class="quick-view">
                        <a title="Add to my wishlist" class="heart" href="#"></a>
                        <a title="Add to compare" class="compare" href="#"></a>
                        <a title="Quick view" class="search" href="#"></a>
                </div>
                <div class="add-to-cart">
                    <a title="Add to Cart" onclick="fast_addtocart('<?php echo $value->id;?>');" >Mua ngay</a>
                </div>
            </div>
            <div class="right-block">
                <h5 class="product-name"><a href="<?php echo $uri; ?>"><?php echo $value->product_name; ?></a></h5>
                <div class="product-star">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                </div>
                <div class="content_price">
                    <span class="price product-price"><?php echo $price; ?></span>
                    <?php if(!empty($price_old) && $price_old <> 0){ ?>
                    <span class="price old-price"><?php echo $price_old; ?></span>
                    <?php } ?>
                    
                </div>
                <div class="info-orther">
                    <p>Item Code: <?php echo $code_id;?></p>
                    <p class="availability">Tình trạng: <span><?php echo $tinhtrang;?></span></p>
                    <div class="product-desc">
                        <?php echo $summary;?>
                    </div>
                </div>
            </div>
        </div>
    </li>
    <?php } ?>


</ul>
<!-- ./PRODUCT LIST -->
     <div class="paging">
        <?php echo $this->pagination->create_links(); ?>
    </div>               

<?php }else{echo '<h4 class="alert alert-info">' . __('IP_comming_soon') . '</h4>';} ?>