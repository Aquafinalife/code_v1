<?php $data = modules::run('news/get_news_home'); ?>
<?php if(!empty($data)) { ?>
<div class="homeservices">
    <div class="container">
        <h3><?php echo __('IP_home_services'); ?></h3>
        <div class="row">
            <?php foreach($data as $key => $value){
                if(SLUG_ACTIVE==0){
                    $uri = get_base_url() . url_title(trim($value->title), 'dash', TRUE) . '-ns' . $value->id;
                }else{
                    $uri = get_base_url() . $value->slug;
                }
                $image = isset($value->thumbnail)?$value->thumbnail:base_url().'images/no-image.png';
                $value_title = limit_text($value->title, 100);
                $value_summary = limit_text($value->summary, 175);
            ?>
            <div class="col-sm-4 hs_item">
                <a class="hs_image" href="<?php echo $uri; ?>" title="<?php echo $value->title; ?>">
                    <img alt="" src="<?php echo $image; ?>" title="<?php echo $value->title; ?>" >
                </a>
                <a class="hs_title" href="<?php echo $uri; ?>" title="<?php echo $value->title; ?>">
                    <?php echo $value_title; ?>
                </a>
                <p class="hs_summary"><?php echo $value_summary; ?></p>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>