<?php 
    $config = get_cache('configurations_' .  get_language());
    $this->load->library('Mobile_Detect');
    $detect = new Mobile_Detect();
    if($detect->isMobile() && !$detect->isTablet()){
        $isMobile = TRUE;
    } else {
        $isMobile = FALSE;
    }
?>
<div class="footer_main">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h3 class="footer_main_title">Phân theo kiểu dáng, chất liệu</h3>
                <div class="menulist">
                    <?php 
                        if(isset($active_menu)){$active_menu=$active_menu;}else{$active_menu=NULL;} 
                        if(isset($current_menu)){$current_menu=$current_menu;}else{$current_menu=NULL;}
                    ?>
                    <?php echo modules::run('menus/menus/get_main_menus',array('current_menu'=>$current_menu,'active_menu'=>$active_menu,'menu_type'=>7)); ?>
                </div>
            </div>
            <div class="col-sm-3">
                <h3 class="footer_main_title">Phân theo thương hiệu</h3>
                <div class="menulist">
                    <?php 
                        if(isset($active_menu)){$active_menu=$active_menu;}else{$active_menu=NULL;} 
                        if(isset($current_menu)){$current_menu=$current_menu;}else{$current_menu=NULL;}
                    ?>
                    <?php echo modules::run('menus/menus/get_main_menus',array('current_menu'=>$current_menu,'active_menu'=>$active_menu,'menu_type'=>8)); ?>
                </div>
            </div>
            <div class="col-sm-3">
                <h3 class="footer_main_title">Hỗ trợ mua hàng</h3>
                <div class="menulist">
                    <?php 
                        if(isset($active_menu)){$active_menu=$active_menu;}else{$active_menu=NULL;} 
                        if(isset($current_menu)){$current_menu=$current_menu;}else{$current_menu=NULL;}
                    ?>
                    <?php echo modules::run('menus/menus/get_main_menus',array('current_menu'=>$current_menu,'active_menu'=>$active_menu,'menu_type'=>6)); ?>
                </div>
            </div>
            <div class="col-sm-3">
                <h3 class="footer_main_title">Kết nối</h3>
                <div id="send_email_account" class="form-search f_search">
                    <div id="error"></div>
                    <div class="with-icon full-width">
                        <input type="text" class="input-text email_letter" name="email" placeholder="Email của bạn">
                        <button class="btn btn-success" onclick="get_newsletter();">Gửi đi</button>
                    </div>
                </div>
                <div class="connection">
                    <ul>
                        <li><a class="facebook" href="https://www.facebook.com/<?php if(!empty($config['facebook_id'])){ echo $config['facebook_id']; } ?>" target="_blank">&nbsp; </a></li>
                        <li><a class="youtube" href="http://www.youtube.com/" target="_blank">&nbsp; </a></li>
                        <li><a class="twt" href="https://twitter.com/" target="_blank">&nbsp; </a></li>
                        <li><a class="wpress" href="#" target="_blank">&nbsp; </a></li>
                        <li style="margin: 0;"><a class="google" title="Giaytot G+" href="https://plus.google.com/">&nbsp; </a></li>
                    </ul>
                    <h3><a href="http://www.online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=3083"  target="_blank" class="qrcode">&nbsp; </a></h3>
                    <p><a class="appstore">&nbsp; </a> <a class="googleplay">&nbsp; </a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container footer">
        <div class="row footer_contact">
            <div class="col-sm-12">
                <?php if(!empty($config['footer_contact'])){ ?>
                <?php echo $config['footer_contact']; ?>
                <?php } ?>
                <p><?php echo DEFAULT_FOOTER; ?>&nbsp;&VerticalLine;&nbsp;<?php echo __('IP_default_design'); ?></p>
            </div>
        </div>
    </div>
</footer>

<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>



<?php 
if(!empty($config)){
    echo (isset($config['google_tracker']))?$config['google_tracker']:'';
}
?>
<!-- Script-->
<script type="text/javascript" src="<?php echo base_url(); ?>frontend/lib/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>frontend/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>frontend/lib/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>frontend/lib/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>frontend/lib/owl.carousel/owl.carousel.min.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>frontend/lib/jquery.countdown/jquery.countdown.min.js"></script>-->
<!-- COUNTDOWN -->
<script type="text/javascript" src="<?php echo base_url(); ?>frontend/lib/countdown/jquery.plugin.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>frontend/lib/countdown/jquery.countdown.js"></script>
<!-- ./COUNTDOWN -->
<script type="text/javascript" src="<?php echo base_url(); ?>frontend/lib/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>frontend/lib/jquery.actual.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>frontend/lib/theme-script.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>frontend/lib/home.js"></script>

<?php if(empty($is_home) || $is_home <> TRUE){ ?>
<script type='text/javascript'>
    $(function () {
        if($('#menuside li').hasClass('active')) {
            $('li.active').parentsUntil('#menuside', 'li').add(this).addClass('active');
        }
	$('#menuside').on('click', 'li', function () {
	  $('#menuside li').removeClass('active');
	  $(this).parentsUntil('#menuside', 'li').add(this).addClass('active');
//	  return false;
	});
    });
</script>
<?php } ?>
<?php if(isset($scripts)){echo $scripts;} ?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php if(!empty($config['livechat'])){ ?>
<?php echo $config['livechat']; ?>
<?php } ?>
