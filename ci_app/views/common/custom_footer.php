<script src="<?php echo base_url(); ?>frontend/custom/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>frontend/custom/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url(); ?>frontend/custom/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>frontend/custom/js/plugins.js"></script>

<script src="<?php echo base_url(); ?>frontend/custom/js/lib/peity/jquery.peity.min.js"></script>
<script src="<?php echo base_url(); ?>frontend/custom/js/lib/table-edit/jquery.tabledit.min.js"></script>
<script>
    $(function () {
        $(".bar-chart").peity("bar", {
            delimiter: ",",
            fill: ["#919fa9"],
            height: 16,
            max: null,
            min: 0,
            padding: 0.1,
            width: 384
        });

        $('#table-edit').Tabledit({
            url: 'example.php',
            columns: {
                identifier: [0, 'id'],
                editable: [[1, 'name'], [2, 'description']]
            }
        });
    });
</script>

<script src="<?php echo base_url(); ?>frontend/custom/js/app.js"></script>

<script type="text/javascript" src="/plugins/select2/js/select2.min.js"></script>

<script type="text/javascript" src="/powercms/scripts/site.js"></script>
{IMPORT_JS}
<?php if(isset($scripts)) echo $scripts; ?>
<style>
    .side-menu{        
        /*opacity: 0.2;*/
    }
    /*
----------------------------------PAGINATION------------------------------------
    */
    .pagination > li > a,.pagination a, .pagination b, .pagination strong {
        background-color: #fff;
        border: 1px solid #ddd;
        color: #428bca;
        float: left;
        line-height: 1.42857;
        margin-left: -1px;
        padding: 8px 18px;
        position: relative;
        text-decoration: none;
        text-align: center;
    }
    .pagination b,.pagination strong{
        color: #333;
    }
</style>
</body>

<!-- Mirrored from themesanytime.com/startui/demo/tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 07 Oct 2016 04:55:43 GMT -->
</html>