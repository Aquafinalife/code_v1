<!DOCTYPE html>
<html>

<!-- Mirrored from themesanytime.com/startui/demo/tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 07 Oct 2016 04:55:42 GMT -->
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>StartUI - Premium Bootstrap 4 Admin Dashboard Template</title>

	<link href="<?php echo base_url(); ?>frontend/custom/img/favicon.144x144.html" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="<?php echo base_url(); ?>frontend/custom/img/favicon.114x114.html" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="<?php echo base_url(); ?>frontend/custom/img/favicon.72x72.html" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="<?php echo base_url(); ?>frontend/custom/img/favicon.57x57.html" rel="apple-touch-icon" type="image/png">
	<link href="<?php echo base_url(); ?>frontend/custom/img/favicon.html" rel="icon" type="image/png">
	<link href="<?php echo base_url(); ?>frontend/custom/img/favicon-2.html" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    <!--<link rel="stylesheet" href="<?php echo base_url(); ?>frontend/custom/css/lib/bootstrap-table/bootstrap-table.min.css">-->
    <!--<link rel="stylesheet" href="<?php echo base_url(); ?>frontend/custom/css/lib/bootstrap-table/bootstrap-table-fixed-columns.css">-->
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>frontend/custom/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>frontend/custom/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>frontend/custom/css/main.css">
    
    <link rel="stylesheet" type="text/css" href="/plugins/select2/css/select2.min.css" />
</head>
<body class="with-side-menu">