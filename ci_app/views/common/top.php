<?php 
    $config = get_cache('configurations_' .  get_language());
    if(!empty($config)){
//        $facebook_url = !empty($config['facebook_id'])?'https://www.facebook.com/'.$config['facebook_id']:'#';
        $logo = !empty($config['logo'])?base_url().UPLOAD_URL_LOGO.$config['logo']:base_url().'images/logo.png';
        $info_header = $config['company_infomation'];
    }
 
?>
<?php if(empty($isMobile)){ ?>
<!-- HEADER -->
<div id="header" class="header">
    <div class="top-header hidden-xs">
        <div class="container">
            
            
          <?php echo $info_header; ?>

            <div id="user-info-top" class="user-info pull-right">
                <div class="dropdown">
                    <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>My Account</span></a>
                    <ul class="dropdown-menu mega_dropdown" role="menu">
                        <li><a href="/login">Login</a></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--/.top-header -->
    <!-- MAIN HEADER -->
    <div class="container main-header">
        <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-3 logo">
                <a href="<?php echo get_base_url(); ?>" class="logo1" title="<?php echo __('IP_DEFAULT_TITLE'); ?>" >
                    <img src="<?php echo $logo; ?>" />
                </a>
                <div class="visible-xs" style="
    float: right;
    padding-top: 28px;
    background: url('../../images/icon-cart-round5.png') no-repeat center center;
    width: 39px;
    height: 58px;
    float: right;
    position: relative;
    margin-right: 16px;
" >
                    <a style="
    width: 100%;
    height: 100%;
    float: left;
    font-size: 0;
" href="/gio-hang" title="<?php echo __('IP_shopping_cart'); ?>" class="t_cart" rel="nofollow"><i class="fa fa-shopping-cart"></i></a>
                    
                
                   
                    <span class="notify notify-right" style="
    right: -15px;
    top: 0px;
"><?php echo $this->cart->total_items(); ?></span>
                
                </div>
            </div>
           
            <div class="col-xs-7 col-sm-8 col-md-7 header-search-box hidden-xs">
                <form class="form-inline" method="post" action="<?php echo get_form_submit_by_lang(get_language(),'searchform'); ?>">
                      
                      <div class="form-group input-serach">
                        <input type="text"  id="searchid" class="search text-field" placeholder="<?php echo __('IP_search'); ?> ..." name="keyword" value="" title="<?php echo __('IP_search_input_title'); ?>">
                      </div>
                      <button type="submit" class="pull-right btn-search"></button>
                </form>
                <div id="result">
                </div>
                <?php $page0 = modules::run('pages/get_page', '/host-keyword.html'); ?>
                <?php if (!empty($page0)) {
                    echo $page0['content'];
                } ?>
                
            </div>
            <div class="col-xs-5 col-sm-3 col-md-2 group-button-header hidden-xs">
                <a title="Compare" href="#" class="btn-compare">compare</a>
                <a title="My wishlist" href="#" class="btn-heart">wishlist</a>
                <div class="btn-cart" id="cart-block">
                    <a href="/gio-hang" title="<?php echo __('IP_shopping_cart'); ?>" class="t_cart" rel="nofollow"><i class="fa fa-shopping-cart"></i></a>
                    
                
                   
                    <span class="notify notify-right"><?php echo $this->cart->total_items(); ?></span>
                
                </div>
            </div>
        </div>
        
    </div>
    <!-- END MANIN HEADER -->
    <div id="nav-top-menu" class="nav-top-menu">
        <div class="container">
            <div class="row">
                
                <div class="col-sm-3" id="box-vertical-megamenus">
                    <div class="box-vertical-megamenus">
                    <h4 class="title">
                        <span class="btn-open-mobile"><i class="fa fa-bars"></i></span>
                    </h4>
                    <div class="vertical-menu-content is-home">
                             <?php 
                            if(isset($active_menu)){$active_menu=$active_menu;}else{$active_menu=NULL;} 
                            if(isset($current_menu)){$current_menu=$current_menu;}else{$current_menu=NULL;}
                        ?>
                        <?php echo modules::run('menus/menus/get_main_menus',array('current_menu'=>$current_menu,'active_menu'=>$active_menu,'menu_type'=>FRONT_END_MENU_TOP_CAT_ID,'menu_mega'=>true)); ?>

                            
                    </div>
                </div>
                </div>
                <div id="main-menu" class="col-sm-9 main-menu">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <a class="navbar-brand" href="#">MENU</a>
                            </div>
                            <?php $page1 = modules::run('pages/get_page', '/menu-nhe.html'); ?>
                            <?php if (!empty($page1)) {
                                echo $page1['content'];
                            } ?>
                            
                            <!--/.nav-collapse -->
                        </div>
                    </nav>
                    
                    
                    
                </div>
            </div>
            <!-- userinfo on top-->
            <div id="form-search-opntop">
            </div>
            <!-- userinfo on top-->
            <div id="user-info-opntop">
            </div>
            <!-- CART ICON ON MMENU -->
            <div id="shopping-cart-box-ontop">
                <i class="fa fa-shopping-cart"></i>
                <div class="shopping-cart-box-ontop-content"></div>
            </div>
        </div>
    </div>
</div>
<!-- end header -->

<?php }else{ ?>
<div class="row">
            <div class="col-sm-12 top_search">
                <form class="searchform" method="post" roll="form" action="<?php echo get_form_submit_by_lang(get_language(),'searchform'); ?>">
                    <input type="text" class="searchinput" placeholder="<?php echo __('IP_search'); ?>" name="keyword" value="" title="<?php echo __('IP_search_input_title'); ?>">
                    <input type="submit" class="searchsubmit" value="<?php echo __('IP_search'); ?>" title="<?php echo __('IP_search'); ?>">
                </form>
            </div>
        </div>
 <?php } ?>
