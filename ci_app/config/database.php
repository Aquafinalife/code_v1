<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the "Database Connection"
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the "default" group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = ENVIRONMENT;
$active_record = TRUE;

//$db['production']['hostname'] = 'localhost';
//$db['production']['username'] = 'infonhad_demou';
//$db['production']['password'] = 'bHFD7RvX';
//$db['production']['database'] = 'infonhad_demo';
//$db['production']['dbdriver'] = 'mysql';
//$db['production']['dbprefix'] = 'itnl_';
//$db['production']['pconnect'] = TRUE;
//$db['production']['db_debug'] = FALSE;
//$db['production']['cache_on'] = FALSE;
//$db['production']['cachedir'] = '';
//$db['production']['char_set'] = 'utf8';
//$db['production']['dbcollat'] = 'utf8_general_ci';
//$db['production']['autoinit'] = TRUE;
//$db['production']['stricton'] = FALSE;


$db['production']['hostname'] = 'localhost';
$db['production']['username'] = 'lamwebcs_bt';
$db['production']['password'] = 'Abc@123$';
$db['production']['database'] = 'lamwebcs_bt';
$db['production']['dbdriver'] = 'mysql';
$db['production']['dbprefix'] = 'itnl_';
$db['production']['pconnect'] = TRUE;
$db['production']['db_debug'] = FALSE;
$db['production']['cache_on'] = FALSE;
$db['production']['cachedir'] = '';
$db['production']['char_set'] = 'utf8';
$db['production']['dbcollat'] = 'utf8_general_ci';
$db['production']['swap_pre'] = '';
$db['production']['autoinit'] = TRUE;
$db['production']['stricton'] = FALSE;

$db['development']['hostname'] = "localhost";
$db['development']['username'] = "root";
$db['development']['password'] = "";
$db['development']['database'] = "quanlybaohiem";
$db['development']['dbdriver'] = "mysql";
$db['development']['dbprefix'] = "itnl_";
$db['development']['pconnect'] = TRUE;
$db['development']['db_debug'] = TRUE;
$db['development']['cache_on'] = FALSE;
$db['development']['cachedir'] = "";
$db['development']['char_set'] = "utf8";
$db['development']['dbcollat'] = "utf8_general_ci";
$db['development']['autoinit'] = TRUE;
$db['development']['stricton'] = FALSE;

//echo '<pre>';
// print_r($db['production']);
// echo '</pre>';
// echo 'Connecting to database: ' .$db['production']['database'];
// $dbh=mysql_connect(
//    $db['production']['hostname'],
//    $db['production']['username'],
//    $db['production']['password'])
//    or die('Cannot connect to the database because: ' . mysql_error());
//    mysql_select_db ($db['production']['database']);
// 
//    echo '<br />   Connected OK:'  ;
//     die( 'file: ' .__FILE__ . ' Line: ' .__LINE__);

/* End of file database.php */
/* Location: ./system/application/config/database.php */