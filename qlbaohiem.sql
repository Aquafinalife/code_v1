-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 26 Septembre 2016 à 10:05
-- Version du serveur :  5.6.21
-- Version de PHP :  5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `qlbaohiem`
--

-- --------------------------------------------------------

--
-- Structure de la table `itnl_advs`
--

CREATE TABLE IF NOT EXISTS `itnl_advs` (
`id` int(10) unsigned NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `image_dimension` varchar(255) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `url_path` varchar(500) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` text NOT NULL,
  `code` text NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `start_time` int(11) NOT NULL,
  `end_time` int(11) NOT NULL,
  `time_limited` tinyint(1) NOT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_advs`
--

INSERT INTO `itnl_advs` (`id`, `image_name`, `image_dimension`, `position`, `url_path`, `status`, `type`, `title`, `summary`, `code`, `created_date`, `updated_date`, `start_time`, `end_time`, `time_limited`, `lang`, `creator`, `editor`) VALUES
(74, 'ruou_2826b.jpg', '1999x748', 12, '', 1, 1, '', '', '', '2015-09-13 10:20:59', '2015-12-04 16:44:59', 0, 0, 0, 'vi', 1, 1),
(76, '1_167dc.png', '251x80', 1, '', 1, 3, '', '', '', '2016-03-31 09:21:24', '2016-03-31 09:21:24', 0, 0, 0, 'vi', 1, 1),
(77, 'dangky_167fc.png', '640x240', 2, '', 1, 3, '', '', '', '2016-03-31 09:21:56', '2016-03-31 09:21:56', 0, 0, 0, 'vi', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_advs_categories`
--

CREATE TABLE IF NOT EXISTS `itnl_advs_categories` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `dimension` varchar(255) NOT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_advs_categories`
--

INSERT INTO `itnl_advs_categories` (`id`, `title`, `dimension`, `lang`, `creator`, `editor`) VALUES
(1, 'Banner trang chủ', '900x450', 'vi', 1, 1),
(2, 'Banner trang chủ - mobile', '', 'vi', 1, 1),
(3, 'Thương hiệu nổi bật', '', 'vi', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_advs_click`
--

CREATE TABLE IF NOT EXISTS `itnl_advs_click` (
`id` int(11) NOT NULL,
  `advs_id` int(11) NOT NULL,
  `click_time` int(11) NOT NULL,
  `backlink` varchar(255) NOT NULL,
  `browser` varchar(255) NOT NULL,
  `current_ip` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_ci_sessions`
--

CREATE TABLE IF NOT EXISTS `itnl_ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_ci_sessions`
--

INSERT INTO `itnl_ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('b5eb38140888a6f6b608021fb6b668a5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36', 1474876621, ''),
('104455eafead32b00739a447c4482e79', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36', 1474875119, '');

-- --------------------------------------------------------

--
-- Structure de la table `itnl_clbh`
--

CREATE TABLE IF NOT EXISTS `itnl_clbh` (
`id` int(11) NOT NULL,
  `name_custom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmt_custom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loai_benh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `st_dbt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sn_dbt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stbh_cl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sn_cl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ma_bt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_configuration`
--

CREATE TABLE IF NOT EXISTS `itnl_configuration` (
`id` int(11) NOT NULL,
  `lang` varchar(20) NOT NULL DEFAULT 'vi',
  `contact_email` varchar(500) DEFAULT NULL,
  `order_email` varchar(500) DEFAULT NULL,
  `meta_title` varchar(500) DEFAULT NULL,
  `meta_keywords` varchar(500) DEFAULT NULL,
  `meta_description` varchar(500) DEFAULT NULL,
  `favicon` varchar(500) DEFAULT NULL,
  `logo` varchar(500) DEFAULT NULL,
  `news_per_page` smallint(6) DEFAULT NULL,
  `products_per_page` smallint(6) DEFAULT NULL,
  `number_products_per_home` smallint(6) NOT NULL,
  `number_news_per_home` smallint(6) NOT NULL,
  `number_news_per_side` smallint(6) NOT NULL,
  `products_side_per_page` smallint(6) DEFAULT NULL,
  `image_per_page` smallint(6) DEFAULT NULL,
  `google_tracker` text,
  `webmaster_tracker` text,
  `order_email_content` text,
  `footer_infomation` text NOT NULL,
  `footer_contact` text NOT NULL,
  `footer_logo` varchar(500) NOT NULL,
  `footer_link_list` text NOT NULL,
  `company_infomation` text NOT NULL,
  `contact_infomation` text NOT NULL,
  `google_map_code` text NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `number_products_per_side` int(11) NOT NULL DEFAULT '1',
  `editor` int(11) NOT NULL DEFAULT '0',
  `slogan` varchar(500) DEFAULT NULL,
  `pay_bank` longtext NOT NULL,
  `pay_people` longtext NOT NULL,
  `pay_info` longtext NOT NULL,
  `success_order` longtext NOT NULL,
  `livechat` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_configuration`
--

INSERT INTO `itnl_configuration` (`id`, `lang`, `contact_email`, `order_email`, `meta_title`, `meta_keywords`, `meta_description`, `favicon`, `logo`, `news_per_page`, `products_per_page`, `number_products_per_home`, `number_news_per_home`, `number_news_per_side`, `products_side_per_page`, `image_per_page`, `google_tracker`, `webmaster_tracker`, `order_email_content`, `footer_infomation`, `footer_contact`, `footer_logo`, `footer_link_list`, `company_infomation`, `contact_infomation`, `google_map_code`, `telephone`, `facebook_id`, `number_products_per_side`, `editor`, `slogan`, `pay_bank`, `pay_people`, `pay_info`, `success_order`, `livechat`) VALUES
(1, 'vi', 'zodinestore@gmail.com', 'zodinestore@gmail.com', 'Kênh mua hàng trực tuyến các sản phẩm tốt nhất với giá cạnh tranh nhất', 'Kênh mua hàng trực tuyến các sản phẩm tốt nhất với giá cạnh tranh nhất', 'Kênh mua hàng trực tuyến các sản phẩm tốt nhất với giá cạnh tranh nhất', 'favicon_16c47.png', 'logo25f7d_27556.gif', 20, 10, 6, 0, 0, 0, 10, '<script>\r\n  (function(i,s,o,g,r,a,m){i[''GoogleAnalyticsObject'']=r;i[r]=i[r]||function(){\r\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n  })(window,document,''script'',''//www.google-analytics.com/analytics.js'',''ga'');\r\n\r\n  ga(''create'', ''UA-68843777-1'', ''auto'');\r\n  ga(''send'', ''pageview'');\r\n\r\n</script>', '<meta name="google-site-verification" content="0AVtK2FLShiEj0rFGC8JEJ8lP0XUrdllC7kBHauwIoo" />', '<p>Xin k&iacute;nh ch&agrave;o qu&yacute; kh&aacute;ch: <strong>{ten_nguoi_dat}</strong></p>\r\n<p>Xin ch&uacute;c mừng qu&yacute; kh&aacute;ch đ&atilde; đặt h&agrave;ng th&agrave;nh c&ocirc;ng.</p>\r\n<ul>\r\n<li>M&atilde; số đơn h&agrave;ng: <strong>{ma_don_hang}</strong></li>\r\n<li>Thời gian đặt h&agrave;ng: Ng&agrave;y <strong>{ngay_dat}</strong>, v&agrave;o l&uacute;c <strong>{gio_dat}</strong></li>\r\n<li>Địa chỉ: <strong>{dia_chi}</strong></li>\r\n<li>Điện thoại li&ecirc;n hệ: <strong>{dien_thoai}</strong></li>\r\n<li>Email li&ecirc;n hệ: <strong>{email}</strong></li>\r\n</ul>\r\n<p>Đơn h&agrave;ng:</p>\r\n<p>{danh_sach_san_pham}</p>\r\n<p>Ch&uacute;ng t&ocirc;i sẽ li&ecirc;n lạc lại với bạn trong thời gian gần nhất.</p>\r\n<p><span style="color: #ff0000;"><strong>Z&Ocirc; ĐI N&Egrave;</strong></span><br /> <br /><strong>Địa chỉ :&nbsp;Tầng 4, T&ograve;a nh&agrave; 110 Nguyễn Ngọc Nại, Q. Thanh Xu&acirc;n, H&agrave; Nội.</strong></p>\r\n<p><strong>Tel : 04.6275.0383 - 04.3736.9083 - 0906.043.689</strong></p>\r\n<p><strong>Website:<a href="http://zodine.com/"> <span style="color: #ff0000;">www.zodine.com</span></a></strong></p>', '0', '<p><strong>ZODINE.COM: Tầng 4, T&ograve;a nh&agrave; 110 Nguyễn Ngọc Nại, Q. Thanh Xu&acirc;n, H&agrave; Nội</strong></p>\r\n<p><strong>Điện thoại:&nbsp;</strong>+84 979996422 -<strong> Email:</strong>&nbsp;<a href="mailto:zodinestore@gmail.com">zodinestore@gmail.com</a> -&nbsp;<strong>Website:&nbsp;</strong><a href="http://zodine.com/">http://zodine.com</a>&nbsp;- <strong>Facebook: zodine</strong></p>', '', '0', '<div class="top-bar-social"><a href="#"><span class="fa fa-facebook">&nbsp;</span></a> <a href="#"><span class="fa fa-twitter">&nbsp;</span></a> <a href="#"><span class="fa fa-pinterest">&nbsp;</span></a> <a href="#"><span class="fa fa-google-plus">&nbsp;</span></a> <a href="#"><span class="fa fa-phone">&nbsp;</span> 096768 1011</a></div>', '<p style="text-align: left;"><strong><span style="color: #ff0000; font-size: 24pt;">Z&Ocirc; ĐI N&Egrave;</span></strong></p>\r\n<p style="text-align: left;"><span style="font-size: 10pt;"><strong>Địa chỉ:&nbsp;Tầng 4, T&ograve;a nh&agrave; 110 Nguyễn Ngọc Nại, Q. Thanh Xu&acirc;n, H&agrave; Nội.</strong></span></p>\r\n<p>Điện thoại: 097 898 1797</p>\r\n<p><strong>Website:</strong> <a href="http://www.zodine.com">www.zodine.com</a></p>\r\n<p><strong>Email:</strong>&nbsp;zodinestore@gmail.com</p>', '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1862.0839299510064!2d105.82870018071584!3d21.025968487079048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1437055027307" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '04.6275.0383 - 04.3736.9083 - 0906.043.689', '', 0, 1, '', '<p class="compname">C&Aacute;C T&Agrave;I KHOẢN NG&Acirc;N H&Agrave;NG</p>\r\n<table style="height: 104px;" width="746">\r\n<tbody>\r\n<tr>\r\n<td class="imgbank" rowspan="4" valign="middle"><img src="https://www.abay.vn/images/AbayV3/Payment/bank-logo-VCB.gif" alt="" /></td>\r\n<td colspan="2">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td class="column1">T&ecirc;n t&agrave;i khoản : DỊCH THU NGUYỆT</td>\r\n<td class="Tkname">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td class="column1">Số t&agrave;i khoản : 0611000188171</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td class="column1">Chi nh&aacute;nh: KIM M&Atilde;, BA Đ&Igrave;NH, H&Agrave; NỘI</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table style="height: 104px;" width="743">\r\n<tbody>\r\n<tr>\r\n<td class="imgbank" rowspan="4" valign="middle"><img src="https://www.abay.vn/images/AbayV3/Payment/bank-logo-TCB.gif" alt="" /></td>\r\n<td colspan="2">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td class="column1">T&ecirc;n t&agrave;i khoản :</td>\r\n<td class="Tkname">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td class="column1">Số t&agrave;i khoản :</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td class="column1">Chi nh&aacute;nh:</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>', '<p>Ch&uacute;ng t&ocirc;i sẽ cho nh&acirc;n vi&ecirc;n đến tận văn ph&ograve;ng hoặc nh&agrave; bạn để thu tiền thuận tiện cho bạn</p>', '<p>Xin mời bạn đến theo địa chỉ của c&ocirc;ng ty để thanh to&aacute;n:</p>\r\n<p><br /><strong>Địa chỉ :&nbsp;Tầng 4, T&ograve;a nh&agrave; 110 Nguyễn Ngọc Nại, Q. Thanh Xu&acirc;n, H&agrave; Nội.</strong></p>\r\n<p><strong>Tel :&nbsp;097 898 1797</strong></p>', '<p><strong>Ch&uacute;c mừng bạn đ&atilde; đặt h&agrave;ng th&agrave;nh c&ocirc;ng! Ch&uacute;ng t&ocirc;i sẽ li&ecirc;n lạc với bạn trong thời gian sớm nhất.</strong></p>\r\n<p><strong>Mời bạn&nbsp;<a title="Quay lại trang chủ" href="http://zodine.com/">Click v&agrave;o đ&acirc;y</a> để quay lại trang chủ.</strong></p>', ''),
(2, 'en', '', '0', '', '', '', NULL, '', 6, 3, 3, 0, 6, 5, 10, '', '', '0', '', '', '', '', '', '', '', '', '', 0, 1, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `itnl_contact`
--

CREATE TABLE IF NOT EXISTS `itnl_contact` (
`id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `address` varchar(500) NOT NULL,
  `message` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `create_time` int(11) NOT NULL,
  `current_ip` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_contact`
--

INSERT INTO `itnl_contact` (`id`, `fullname`, `company`, `email`, `tel`, `fax`, `address`, `message`, `created_date`, `create_time`, `current_ip`, `status`, `editor`) VALUES
(4, 'Nguyễn Thị Thuỳ Trang', '', 'TrangNguyen.mk205@gmail.com', '0973577520', '', '', 'Dear công ty,\nHiện bên em đang có nhu cầu mua các nguyên liệu để phục vụ cho quán. Được biết bên quý công ty có cung cấp các sản phẩm này. Vì vậy, em muốn hỏi là phía bên công ty mình có chính sách ưu đãi đối với khách hàng lấy số lượng nhiều không ạ.', '2015-07-28 17:37:21', 1438079841, '116.104.105.203', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_customers`
--

CREATE TABLE IF NOT EXISTS `itnl_customers` (
`id` int(11) NOT NULL COMMENT 'Ma KH',
  `fullname` varchar(30) NOT NULL COMMENT 'Ten cty/ ca nhan',
  `DOB` varchar(255) DEFAULT NULL COMMENT 'Date Of Birth',
  `address` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `phone2` varchar(15) DEFAULT NULL COMMENT 'Mobile',
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL COMMENT '	',
  `company` varchar(256) DEFAULT NULL COMMENT 'Cty con | phong ban ',
  `roles_id` int(11) DEFAULT NULL,
  `active` smallint(1) DEFAULT '1',
  `alias_name` varchar(15) DEFAULT NULL,
  `cities_id` int(11) NOT NULL,
  `joined_date` datetime DEFAULT NULL COMMENT 'Ngay cap',
  `avatar` varchar(256) DEFAULT NULL,
  `is_openid` tinyint(4) DEFAULT NULL,
  `sex` tinyint(4) DEFAULT NULL COMMENT 'Giới tính.',
  `city_id` tinyint(4) DEFAULT NULL COMMENT 'thành phố',
  `district_id` tinyint(4) DEFAULT NULL COMMENT 'Quận/huyện',
  `number_house` varchar(256) DEFAULT NULL COMMENT 'Số nhà, ngõ ngách',
  `road` varchar(256) DEFAULT NULL COMMENT 'Đường',
  `level` varchar(256) DEFAULT NULL COMMENT 'Lầu',
  `type` tinyint(4) DEFAULT '1' COMMENT 'Loai kh',
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `editor` int(11) NOT NULL,
  `creator` int(11) NOT NULL,
  `so_hop_dong` varchar(255) NOT NULL COMMENT 'Hop dong BH',
  `cmt` varchar(255) NOT NULL COMMENT 'dkkd | cmt | ho chieu',
  `noi_cap` varchar(255) NOT NULL,
  `type_custom` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 thuong 1 vip'
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_customers`
--

INSERT INTO `itnl_customers` (`id`, `fullname`, `DOB`, `address`, `email`, `phone`, `phone2`, `username`, `password`, `company`, `roles_id`, `active`, `alias_name`, `cities_id`, `joined_date`, `avatar`, `is_openid`, `sex`, `city_id`, `district_id`, `number_house`, `road`, `level`, `type`, `created_date`, `updated_date`, `status`, `editor`, `creator`, `so_hop_dong`, `cmt`, `noi_cap`, `type_custom`) VALUES
(1, 'Long', '11/08/88', 'ha noi', 'long@yahoo.com', '090909090', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-04-22 17:24:00', '2016-04-22 17:30:16', 1, 1, 1, '', '', '', 0),
(2, 'Tuấn', '11/07/88', 'hà nội', 'lamwebchuanseo@gmail.com', '0979996422', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-04-24 11:33:00', '2016-04-24 11:33:13', 1, 1, 1, '', '', '', 0),
(3, 'Long123', '', 'hà nội', 'longlazada@yahoo.com', '0987987966', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01 08:00:00', '2016-04-29 08:51:05', 1, 1, 1, '', '', '', 0),
(4, 'Long456', '', 'hà nội', 'longlazada@yahoo.com', '0987987966', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01 08:00:00', '2016-04-29 09:36:07', 1, 1, 1, '', '', '', 0),
(5, 'Long2367', '', 'hà nội', 'longlazada@yahoo.com', '0987987966', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01 08:00:00', '2016-04-29 09:36:50', 1, 1, 1, '', '', '', 0),
(6, 'Minh', '11/07/88', 'Tầng 4, 110 nguyễn ngọc nại', 'minh@yahoo.com', '094250', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01 08:00:00', '2016-04-29 17:20:09', 1, 1, 1, '', '', '', 0),
(7, 'Vương', '11/08/88', 'ha noi pho', 'vuong@yahoo.com', '0454534543', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1970-01-01 08:00:00', '2016-04-29 17:24:01', 1, 1, 1, '', '', '', 0),
(8, 'Bạch cốt tinh', '11/07/88', 'hà nội', 'tinh@yahoo.com', '0987665654', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-05-02 00:00:00', '2016-05-02 16:24:24', 1, 1, 1, '', '', '', 0),
(9, 'Lưu Hùng Kiên', '11/07/1988', 'hà nội', 'kien@gmail.com', '0909090909', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-09-25 15:17:00', '2016-09-25 15:18:36', 1, 1, 1, '', '012346789', '', 0),
(10, 'Vũ Văn Thông', '11/07/1988', 'hà nội', 'thong@gmail.com', '0979435422', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-09-25 15:18:00', '2016-09-25 15:19:32', 1, 1, 1, '', '012789456', '', 0),
(11, 'Lưu Hùng Kiên Định', '', '', 'jfdsf@gmai.com', '0687456', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-09-25 15:31:00', '2016-09-25 15:32:04', 1, 1, 1, '', '01245789', '', 0),
(12, 'Cao Văn Cường', '', '', 'cuong@gmail.com', '09438477473', NULL, '', '', NULL, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-09-26 14:58:00', '2016-09-26 14:59:15', 1, 1, 1, '', '07865764533', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_download`
--

CREATE TABLE IF NOT EXISTS `itnl_download` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `ext` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(500) NOT NULL,
  `type` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_download_categories`
--

CREATE TABLE IF NOT EXISTS `itnl_download_categories` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `level` tinyint(4) NOT NULL,
  `position` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_emails`
--

CREATE TABLE IF NOT EXISTS `itnl_emails` (
`id` int(11) unsigned NOT NULL,
  `emails` varchar(100) DEFAULT NULL,
  `registry_date` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_emails`
--

INSERT INTO `itnl_emails` (`id`, `emails`, `registry_date`) VALUES
(16, 'dung@gmail.com', '2014-12-04 17:04:40'),
(15, 'chaolem17@yahoo.com', '2014-12-04 16:59:19'),
(17, 'tuyen@yahoo.com', '2015-12-05 14:39:03'),
(18, 'tuyen@yahoo.com', '2015-12-05 14:39:06'),
(19, 'tuyen@yahoo.com', '2015-12-05 14:39:11'),
(20, 'tuyen@yahoo.com', '2015-12-05 14:39:12'),
(21, 'tuyen@yahoo.com', '2015-12-05 14:39:12'),
(22, 'tuyen@yahoo.com', '2015-12-05 14:39:12'),
(23, 'tuyen@yahoo.com', '2015-12-05 14:39:12'),
(24, 'tuyen@yahoo.com', '2015-12-05 14:39:13'),
(25, 'tuyen@yahoo.com', '2015-12-05 14:39:23'),
(26, 'tuyen@yahoo.com', '2015-12-05 14:39:23'),
(27, 'tuyen@yahoo.com', '2015-12-05 14:39:23'),
(28, 'chao@yahoo.com', '2015-12-05 14:41:07');

-- --------------------------------------------------------

--
-- Structure de la table `itnl_faq`
--

CREATE TABLE IF NOT EXISTS `itnl_faq` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` varchar(1000) NOT NULL,
  `content` text NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `address` varchar(500) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `cat_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `viewed` int(11) NOT NULL,
  `thumbnail` varchar(500) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `tags` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `lang` varchar(20) DEFAULT 'vi',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_faq_categories`
--

CREATE TABLE IF NOT EXISTS `itnl_faq_categories` (
`id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `level` tinyint(4) NOT NULL,
  `position` int(11) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_faq_professionals`
--

CREATE TABLE IF NOT EXISTS `itnl_faq_professionals` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` varchar(1000) NOT NULL,
  `content` text NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `thumbnail` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `lang` varchar(20) NOT NULL,
  `creator` int(11) NOT NULL,
  `editor` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_gallery`
--

CREATE TABLE IF NOT EXISTS `itnl_gallery` (
`id` int(11) NOT NULL,
  `gallery_name` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `summary` varchar(500) NOT NULL,
  `content` text NOT NULL,
  `cat_id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `position` int(11) NOT NULL,
  `viewed` int(11) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_gallery_categories`
--

CREATE TABLE IF NOT EXISTS `itnl_gallery_categories` (
`id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `level` tinyint(4) NOT NULL,
  `position` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_gallery_images`
--

CREATE TABLE IF NOT EXISTS `itnl_gallery_images` (
`id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `caption` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_menus`
--

CREATE TABLE IF NOT EXISTS `itnl_menus` (
`id` int(11) NOT NULL,
  `caption` varchar(256) NOT NULL,
  `url_path` varchar(512) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `level` tinyint(4) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `active` tinyint(1) DEFAULT '1',
  `css` varchar(50) DEFAULT NULL,
  `thumbnail` varchar(500) NOT NULL,
  `icon` varchar(500) NOT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0',
  `private` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=906 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_menus`
--

INSERT INTO `itnl_menus` (`id`, `caption`, `url_path`, `parent_id`, `level`, `cat_id`, `position`, `active`, `css`, `thumbnail`, `icon`, `lang`, `creator`, `editor`, `private`) VALUES
(1, 'Dashboard', '/dashboard', 0, 0, 2, 2, 1, 'fa-dashboard', '', '', 'vi', 0, 0, 0),
(3, 'Thêm đơn bảo hiểm', '/dashboard/products/add', 55, 1, 2, 2, 1, '', '', '', 'vi', 0, 1, 0),
(4, 'Tất cả bảo hiểm', '/dashboard/products', 55, 1, 2, 1, 1, '', '', '', 'vi', 0, 1, 0),
(11, 'Loại hình bảo hiểm', '/dashboard/products/cat', 55, 1, 2, 3, 1, '', '', '', 'vi', 0, 1, 0),
(14, 'Bài viết', '/dashboard/news', 0, 0, 2, 3, 1, 'fa-file-text', '', '', 'vi', 0, 0, 0),
(38, 'Hệ thống Menu', '/dashboard/menus', 101, 1, 2, 4, 1, '', '', '', 'vi', 0, 1, 0),
(43, 'Hỗ trợ trực tuyến', '/dashboard/supports', 101, 1, 2, 3, 0, '', '', '', 'vi', 0, 0, 0),
(45, 'Phân loại bài viết', '/dashboard/news/cat', 14, 1, 2, 3, 1, '', '', '', 'vi', 0, 0, 0),
(46, 'Banner quảng cáo', '/dashboard/advs', 101, 1, 2, 2, 0, '', '', '', 'vi', 0, 1, 0),
(50, 'Trang', '/dashboard/pages', 0, 0, 2, 11, 0, 'fa-puzzle-piece', '', '', 'vi', 0, 0, 0),
(55, 'Bảo hiểm', '/dashboard/products', 0, 0, 2, 10, 1, 'fa-gift', '', '', 'vi', 0, 1, 0),
(62, 'Thêm bài viết', '/dashboard/news/add', 14, 1, 2, 2, 1, '', '', '', 'vi', 0, 0, 0),
(87, 'Thay đổi mật khẩu', '/dashboard/auth/change_password', 100, 1, 2, 1, 1, '', '', '', 'vi', 0, 0, 0),
(99, 'Cấu hình chung', '/dashboard/system_config', 100, 1, 2, 2, 0, '', '', '', 'vi', 0, 0, 0),
(100, 'Cài đặt', '#', 0, 0, 2, 20, 1, 'fa-cog', '', '', 'vi', 0, 0, 0),
(101, 'Giao diện', '#', 0, 0, 2, 19, 1, 'fa-pencil', '', '', 'vi', 0, 0, 0),
(152, 'Trang chủ', '/', 0, 0, 2, 1, 1, 'fa-home', '', '', 'vi', 0, 1, 0),
(151, 'Tất cả liên hệ', '/dashboard/contact', 150, 1, 2, 1, 1, '', '', '', 'vi', 0, 0, 0),
(150, 'Liên hệ/Phản hồi', '/dashboard/contact', 0, 0, 2, 13, 0, 'fa-phone', '', '', 'vi', 0, 1, 0),
(224, 'Phân loại tranh ảnh', '/dashboard/gallery/cat', 221, 1, 2, 3, 1, '', '', '', 'vi', 0, 0, 0),
(223, 'Thêm tranh ảnh', '/dashboard/gallery/add', 221, 1, 2, 2, 1, '', '', '', 'vi', 0, 0, 0),
(123, 'Đơn vị', '/dashboard/products/units', 55, 1, 2, 4, 0, '', '', '', 'vi', 0, 0, 0),
(126, 'Tất cả bài viết', '/dashboard/news', 14, 1, 2, 1, 1, '', '', '', 'vi', 0, 0, 0),
(127, 'Tất cả trang tin', '/dashboard/pages', 50, 1, 2, 1, 1, '', '', '', 'vi', 0, 0, 0),
(128, 'Thêm trang tin', '/dashboard/pages/add', 50, 1, 2, 2, 1, '', '', '', 'vi', 0, 0, 0),
(129, 'Tất cả menu', '/dashboard/menus', 38, 2, 2, 1, 1, '', '', '', 'vi', 0, 0, 0),
(130, 'Thêm menu', '/dashboard/menus/add', 38, 2, 2, 2, 1, '', '', '', 'vi', 0, 0, 0),
(131, 'Phân loại menu', '/dashboard/menus/cat', 38, 2, 2, 3, 0, '', '', '', 'vi', 0, 0, 0),
(132, 'Tất cả banner', '/dashboard/advs', 46, 2, 2, 4, 1, '', '', '', 'vi', 0, 0, 0),
(133, 'Thêm banner', '/dashboard/advs/add', 46, 2, 2, 5, 1, '', '', '', 'vi', 0, 0, 0),
(134, 'Phân loại banner', '/dashboard/advs/cat', 46, 2, 2, 6, 0, '', '', '', 'vi', 0, 0, 0),
(135, 'Tất cả hỗ trợ', '/dashboard/supports', 43, 2, 2, 1, 1, '', '', '', 'vi', 0, 0, 0),
(136, 'Thêm hỗ trợ', '/dashboard/supports/add', 43, 2, 2, 2, 1, '', '', '', 'vi', 0, 0, 0),
(175, 'Tài liệu', '/dashboard/download', 0, 0, 2, 14, 0, 'fa-download', '', '', 'vi', 0, 1, 0),
(222, 'Tất cả tranh ảnh', '/dashboard/gallery', 221, 1, 2, 1, 1, '', '', '', 'vi', 0, 0, 0),
(221, 'Tranh ảnh', '/dashboard/gallery', 0, 0, 2, 15, 0, 'fa-dashboard', '', '', 'vi', 0, 0, 0),
(199, 'Tất cả Tài liệu', '/dashboard/download', 175, 1, 2, 1, 1, '', '', '', 'vi', 0, 1, 0),
(200, 'Phân loại Tài liệu', '/dashboard/download/cat', 175, 1, 2, 3, 1, '', '', '', 'vi', 0, 1, 0),
(225, 'Thêm Tài liệu', '/dashboard/download/add', 175, 1, 2, 2, 1, '', '', '', 'vi', 0, 1, 0),
(311, 'Hỏi đáp', '/dashboard/faq', 0, 0, 2, 12, 0, 'fa-question', '', '', 'vi', 0, 0, 0),
(312, 'Tất cả hỏi đáp', '/dashboard/faq', 311, 1, 2, 1, 1, '', '', '', 'vi', 0, 0, 0),
(313, 'Thêm hỏi đáp', '/dashboard/faq/add', 311, 1, 2, 2, 1, '', '', '', 'vi', 0, 0, 0),
(314, 'Phân loại hỏi đáp', '/dashboard/faq/cat', 311, 1, 2, 3, 1, '', '', '', 'vi', 0, 0, 0),
(632, 'Videos', '/dashboard/videos', 0, 0, 2, 16, 0, 'fa-youtube', '', '', 'vi', 0, 0, 0),
(633, 'Tất cả Videos', '/dashboard/videos', 632, 1, 2, 1, 1, '', '', '', 'vi', 0, 0, 0),
(634, 'Thêm Videos', '/dashboard/videos/add', 632, 1, 2, 2, 1, '', '', '', 'vi', 0, 0, 0),
(635, 'Phân loại Videos', '/dashboard/videos/cat', 632, 1, 2, 3, 1, '', '', '', 'vi', 0, 0, 0),
(636, 'Người dùng', '/dashboard/auth', 0, 0, 2, 18, 1, 'fa-users', '', '', 'vi', 0, 0, 0),
(637, 'Tất cả người dùng', '/dashboard/auth', 636, 1, 2, 1, 1, '', '', '', 'vi', 0, 0, 0),
(638, 'Thêm người dùng', '/dashboard/auth/add', 636, 1, 2, 2, 1, '', '', '', 'vi', 0, 0, 0),
(674, 'Vai trò', '/dashboard/auth/roles', 636, 1, 2, 3, 1, '', '', '', 'vi', 1, 1, 0),
(683, 'Màu sắc', '/dashboard/products/color', 55, 1, 2, 7, 0, '', '', '', 'vi', 1, 1, 0),
(712, 'Chuyên gia tư vấn', '/dashboard/faq/pro', 311, 1, 2, 4, 1, '', '', '', 'vi', 1, 1, 0),
(713, 'Nhà cung cấp', '/dashboard/products/trademark', 55, 1, 2, 5, 1, '', '', '', 'vi', 1, 1, 0),
(714, 'Nguồn khách hàng', '/dashboard/products/origin', 55, 1, 2, 6, 1, '', '', '', 'vi', 1, 1, 0),
(880, 'Dụng Cụ Nhỏ Thuốc', '/dung-cu-nho-thuoc', 846, 1, 1, 5, 1, '', '', '', 'vi', 1, 1, 0),
(879, 'Chăm Sóc Móng', '/cham-soc-mong', 846, 1, 1, 4, 1, '', '', '', 'vi', 1, 1, 0),
(878, 'Bàn Chải Răng', '/ban-chai-rang', 846, 1, 1, 3, 1, '', '', '', 'vi', 1, 1, 0),
(877, 'Túi Nhai Silicone', '/tui-nhai-silicone', 846, 1, 1, 2, 1, '', '', '', 'vi', 1, 1, 0),
(875, 'Tư Vấn Sức Khỏe', '/tu-van-suc-khoe', 853, 1, 1, 4, 1, '', '', '', 'vi', 1, 1, 0),
(874, 'Thẻ Hội Viên Vinmec Child', '/the-hoi-vien-vinmec-child', 872, 2, 1, 2, 1, '', '', '', 'vi', 1, 1, 0),
(873, 'Xác Nghiệm ADN', '/xac-nghiem-adn', 872, 2, 1, 1, 1, '', '', '', 'vi', 1, 1, 0),
(728, 'Bồi thường', '/dashboard/orders', 0, 0, 2, 17, 1, 'fa-bar-chart-o', '', '', 'vi', 1, 1, 0),
(729, 'Khách hàng', '/dashboard/customers', 0, 0, 2, 4, 1, 'fa-users', '', '', 'vi', 1, 1, 0),
(771, 'Tất cả mã coupon', '/dashboard/products/coupon_item', 745, 2, 2, 1, 1, '', '', '', 'vi', 1, 1, 0),
(742, 'Gói bảo hiểm', '/dashboard/products/size', 55, 1, 2, 8, 1, '', '', '', 'vi', 1, 1, 0),
(743, 'Hoa hồng', '/dashboard/products/material', 55, 1, 2, 9, 1, '', '', '', 'vi', 1, 1, 0),
(744, 'Hình thức thanh toán', '/dashboard/products/style', 55, 1, 2, 10, 1, '', '', '', 'vi', 1, 1, 0),
(745, 'Mã coupon', '/dashboard/products/coupon', 55, 1, 2, 11, 0, '', '', '', 'vi', 1, 1, 0),
(748, 'Thanh toán linh hoạt', '/thanh-toan-linh-hoat.html', 0, 0, 6, 3, 1, '', '', '', 'vi', 1, 1, 0),
(872, 'Gói Khám Sức Khỏe', '/goi-kham-suc-khoe', 853, 1, 1, 3, 1, '', '', '', 'vi', 1, 1, 0),
(753, 'Giao hàng toàn quốc', '/giao-hang-toan-quoc.html', 0, 0, 6, 8, 1, '', '', '', 'vi', 1, 1, 0),
(871, 'Fitness', '/fitness', 869, 2, 1, 2, 0, '', '', '', 'vi', 1, 1, 0),
(876, 'Dụng Cụ Cho Bé Ăn', '/dung-cu-cho-be-an', 846, 1, 1, 1, 1, '', '', '', 'vi', 1, 1, 0),
(870, 'Spa', '/spa', 869, 2, 1, 1, 0, '', '', '', 'vi', 1, 1, 0),
(869, 'Dịch Vụ Spa & Fitness', '/dich-vu-spa-fitness', 853, 1, 1, 2, 0, '', '', '', 'vi', 1, 1, 0),
(867, 'Thực Phẩm Chức Năng', '/thuc-pham-chuc-nang', 852, 1, 1, 6, 0, '', '', '', 'vi', 1, 1, 0),
(866, 'Tinh Dầu', '/tinh-dau', 852, 1, 1, 5, 0, '', '', '', 'vi', 1, 1, 0),
(865, 'Chăm Sóc Da', '/cham-soc-da', 852, 1, 1, 4, 1, '', '', '', 'vi', 1, 1, 0),
(864, 'Chăm Sóc Tóc', '/cham-soc-toc', 852, 1, 1, 3, 0, '', '', '', 'vi', 1, 1, 0),
(862, 'Sữa Tắm', '/sua-tam', 852, 1, 1, 1, 1, '', '', '', 'vi', 1, 1, 0),
(863, 'Nước Hoa', '/nuoc-hoa', 852, 1, 1, 2, 0, '', '', '', 'vi', 1, 1, 0),
(861, 'Máy Massage', '/may-massage', 851, 1, 1, 3, 1, '', '', '', 'vi', 1, 1, 0),
(860, 'Máy Xông Mũi Họng', '/may-xong-mui-hong', 851, 1, 1, 2, 1, '', '', '', 'vi', 1, 1, 0),
(859, 'Máy Khử Độc', '/may-khu-doc', 851, 1, 1, 1, 1, '', '', '', 'vi', 1, 1, 0),
(858, 'Bảo Hiểm Khác', '/bao-hiem-khac', 847, 1, 1, 5, 1, '', '', '', 'vi', 1, 1, 0),
(857, 'Bảo Hiểm Chung Cư', '/chung-cu', 847, 1, 1, 4, 1, '', '', '', 'vi', 1, 1, 0),
(856, 'Bảo Hiểm Gia Đình', '/gia-dinh', 847, 1, 1, 3, 1, '', '', '', 'vi', 1, 1, 0),
(855, 'Bảo Hiểm Thai Sản', '/thai-san', 847, 1, 1, 2, 1, '', '', '', 'vi', 1, 1, 0),
(854, 'Bảo Hiểm Trẻ Em', '/tre-em', 847, 1, 1, 1, 1, '', '', '', 'vi', 1, 1, 0),
(853, 'Dịch Vụ Sức Khỏe', '/dich-vu-suc-khoe', 0, 0, 1, 8, 1, '', '', '/images/source/icon_menu/1.png', 'vi', 1, 1, 0),
(852, 'Mỹ Phẩm Làm Đẹp', '/my-pham-lam-dep', 0, 0, 1, 7, 1, '', '', '/images/source/icon_menu/15.png', 'vi', 1, 1, 0),
(851, 'Thiết Bị Y Tế Gia Đình', '/thiet-bi-y-te-gia-dinh', 0, 0, 1, 6, 1, '', '', '/images/source/icon_menu/4.png', 'vi', 1, 1, 0),
(850, 'Nhiệt Kế', '/nhiet-ke', 0, 0, 1, 5, 1, '', '', '/images/source/icon_menu/9.png', 'vi', 1, 1, 0),
(849, 'Máy Đo Đường Huyết', '/may-do-duong-huyet', 0, 0, 1, 4, 1, '', '', '/images/source/icon_menu/12.png', 'vi', 1, 1, 0),
(848, 'Máy Đo Huyết Áp', '/may-do-huyet-ap', 0, 0, 1, 3, 1, '', '', '/images/source/icon_menu/14.png', 'vi', 1, 1, 0),
(847, 'Bảo Hiểm Sức Khỏe', '/bao-hiem-suc-khoe', 0, 0, 1, 2, 1, '', '', '/images/source/icon_menu/21.png', 'vi', 1, 1, 0),
(846, 'Sản Phẩm Cho Bé', '/san-pham-cho-be', 0, 0, 1, 1, 1, '', '/images/source/banner_sp/banner-product1.jpg', '/images/source/icon_menu/17.png', 'vi', 1, 1, 0),
(881, 'Bộ Tiện Ích Du Lịch', '/bo-tien-ich-du-lich', 846, 1, 1, 6, 1, '', '', '', 'vi', 1, 1, 0),
(882, 'Địu Trẻ Em', '/diu-tre-em', 846, 1, 1, 7, 1, '', '', '', 'vi', 1, 1, 0),
(883, 'Medisana', '/medisana-huyet-ap', 848, 1, 1, 1, 1, '', '', '', 'vi', 1, 1, 0),
(884, 'Omron', '/omron-huyet-ap', 848, 1, 1, 2, 1, '', '', '', 'vi', 1, 1, 0),
(885, 'Laica', '/laica-huyet-ap', 848, 1, 1, 3, 1, '', '', '', 'vi', 1, 1, 0),
(886, 'Medisana', '/medisana-duong-huyet', 849, 1, 1, 1, 1, '', '', '', 'vi', 1, 1, 0),
(887, 'Omron', '/omron-duong-huyet', 849, 1, 1, 2, 1, '', '', '', 'vi', 1, 1, 0),
(888, 'Medisana', '/medisana-nhiet-ke', 850, 1, 1, 1, 1, '', '', '', 'vi', 1, 1, 0),
(889, 'Omron', '/omron-nhiet-ke', 850, 1, 1, 2, 1, '', '', '', 'vi', 1, 1, 0),
(890, 'Laica', '/laica-nhiet-ke', 850, 1, 1, 3, 1, '', '', '', 'vi', 1, 1, 0),
(891, 'Hannox', '/hannox-nhiet-ke', 850, 1, 1, 4, 1, '', '', '', 'vi', 1, 1, 0),
(892, 'Loại Khác', '/loai-khac', 850, 1, 1, 5, 1, '', '', '', 'vi', 1, 1, 0),
(893, 'Máy Xông Mặt', '/may-xong-mat', 851, 1, 1, 4, 1, '', '', '', 'vi', 1, 1, 0),
(894, 'Máy Tạo Độ Ẩm', '/may-tao-do-am', 851, 1, 1, 5, 1, '', '', '', 'vi', 1, 1, 0),
(895, 'Máy Đo Cơ Thể', '/may-do-co-the', 851, 1, 1, 6, 1, '', '', '', 'vi', 1, 1, 0),
(896, 'Cân Sức Khỏe', '/can-suc-khoe', 851, 1, 1, 7, 1, '', '', '', 'vi', 1, 1, 0),
(897, 'Chăm Sóc Móng', '/cham-soc-mong-tay-chan', 851, 1, 1, 8, 1, '', '', '', 'vi', 1, 1, 0),
(898, 'Bình Lọc Nước', '/binh-loc-nuoc', 851, 1, 1, 9, 1, '', '', '', 'vi', 1, 1, 0),
(899, 'Hướng Dẫn Mua Hàng', 'huong-dan-mua-hang', 0, 0, 1, 9, 1, '', '', '/images/source/icon_menu/5.png', 'vi', 1, 1, 0),
(900, 'Hướng Dẫn Thanh Toán', 'huong-dan-thanh-toan', 0, 0, 1, 10, 1, '', '', '/images/source/icon_menu/cart-icon4.png', 'vi', 1, 1, 0),
(901, 'Hướng Dẫn Mua Hàng', 'huong-dan-mua-hang', 0, 0, 6, 9, 1, '', '', '', 'vi', 1, 1, 0),
(902, 'Hướng Dẫn Thanh Toán', 'huong-dan-thanh-toan', 0, 0, 6, 10, 1, '', '', '', 'vi', 1, 1, 0),
(903, 'Liên hệ - Phản hồi', '/lien-he', 0, 0, 1, 11, 1, '', '', '/images/source/icon_menu/18.png', 'vi', 1, 1, 0),
(904, 'Email', '/dashboard/emails', 0, 0, 2, 21, 0, '', '', '', 'vi', 1, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_menus_categories`
--

CREATE TABLE IF NOT EXISTS `itnl_menus_categories` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_menus_categories`
--

INSERT INTO `itnl_menus_categories` (`id`, `name`, `status`, `creator`, `editor`) VALUES
(1, 'Menu trang chủ - Phía trên', 1, 0, 0),
(2, 'Menu quản trị', 1, 0, 0),
(3, 'Menu Trang chủ - Bên trái', 1, 0, 0),
(4, 'Menu Trang chủ - Bên phải', 0, 0, 0),
(5, 'Menu Trang chủ - Phía dưới', 1, 0, 1),
(6, 'Menu Hỗ trợ mua hàng', 1, 1, 1),
(7, 'Menu trang chủ', 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_news`
--

CREATE TABLE IF NOT EXISTS `itnl_news` (
`id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `summary` varchar(1000) DEFAULT NULL,
  `content` text,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `viewed` int(11) DEFAULT '0',
  `thumbnail` varchar(500) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `meta_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `meta_keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `tags` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `lang` varchar(20) DEFAULT 'vi',
  `status` tinyint(1) NOT NULL,
  `home` tinyint(1) NOT NULL,
  `startups` tinyint(1) NOT NULL,
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Contenu de la table `itnl_news`
--

INSERT INTO `itnl_news` (`id`, `title`, `summary`, `content`, `created_date`, `updated_date`, `cat_id`, `position`, `viewed`, `thumbnail`, `city_id`, `meta_title`, `meta_keywords`, `meta_description`, `tags`, `lang`, `status`, `home`, `startups`, `creator`, `editor`) VALUES
(28, 'thu nghiem', 'dfsdfsdf', '<p>dfsdfsdf&nbsp;dfsdfsdf&nbsp;dfsdfsdf&nbsp;dfsdfsdf&nbsp;dfsdfsdf&nbsp;dfsdfsdf&nbsp;dfsdfsdf&nbsp;dfsdfsdf&nbsp;</p>', '2016-04-25 09:13:00', '2016-04-25 11:03:41', 1, 1, 6, '/images/source/banner_sp/thumb/banner-topmenu.jpg', NULL, '', '', '', '', 'vi', 1, 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_news_categories`
--

CREATE TABLE IF NOT EXISTS `itnl_news_categories` (
`id` int(11) NOT NULL,
  `category` varchar(256) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `level` tinyint(4) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `thumbnail` varchar(500) NOT NULL,
  `summary` text NOT NULL,
  `content` text NOT NULL,
  `meta_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `meta_keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `lang` varchar(20) DEFAULT 'vi',
  `status` tinyint(1) NOT NULL,
  `home` tinyint(1) NOT NULL,
  `grid` tinyint(1) NOT NULL DEFAULT '0',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0',
  `private` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Contenu de la table `itnl_news_categories`
--

INSERT INTO `itnl_news_categories` (`id`, `category`, `parent_id`, `level`, `position`, `thumbnail`, `summary`, `content`, `meta_title`, `meta_keywords`, `meta_description`, `lang`, `status`, `home`, `grid`, `creator`, `editor`, `private`) VALUES
(1, 'Tin tức', 0, 0, 2, '', '', '', '', '', '', 'vi', 1, 1, 0, 1, 1, 0),
(2, 'Khuyến mại', 0, 0, 3, '', '', '', '', '', '', 'vi', 1, 1, 0, 1, 1, 0),
(3, 'Công thức', 10, 1, 1, '', '', '', '', '', '', 'vi', 1, 1, 0, 1, 1, 0),
(10, 'Kinh nghiệm', 0, 0, 5, '', '', '', '', '', '', 'vi', 1, 1, 0, 1, 1, 0),
(11, 'Kinh nghiệm nguyên liệu', 10, 1, 2, '', '', '', '', '', '', 'vi', 1, 0, 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_orders`
--

CREATE TABLE IF NOT EXISTS `itnl_orders` (
`id` int(11) unsigned NOT NULL,
  `reserve_time` varchar(255) DEFAULT NULL COMMENT 'Ngay giao hang',
  `kind_pay` int(11) DEFAULT NULL COMMENT 'Hinh thuc thanh toan 1 la truc tiep 2 la chuyen khoan',
  `user_id` int(11) DEFAULT NULL,
  `fullname` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `sale_date` datetime DEFAULT NULL,
  `payment` tinyint(4) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `total_discount` double NOT NULL COMMENT 'total sau khi nhap ma giam gia',
  `coupon_code` varchar(255) NOT NULL,
  `order_status` tinyint(4) DEFAULT '0',
  `receiver` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `tel` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `message` varchar(255) NOT NULL,
  `other` text,
  `city_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `current_ip` varchar(255) NOT NULL,
  `lang` varchar(20) NOT NULL DEFAULT 'vi',
  `str_created_order` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_orders`
--

INSERT INTO `itnl_orders` (`id`, `reserve_time`, `kind_pay`, `user_id`, `fullname`, `company`, `sale_date`, `payment`, `total`, `total_discount`, `coupon_code`, `order_status`, `receiver`, `address`, `phone`, `tel`, `fax`, `email`, `message`, `other`, `city_id`, `district_id`, `create_time`, `update_time`, `current_ip`, `lang`, `str_created_order`) VALUES
(17, NULL, 1, 0, 'Hoàng Thanh Hiều', '', '2015-07-30 16:37:01', NULL, 1596000, 0, '', 3, NULL, '162A- Đường Trần Phú - Phường Minh Khai - TP Hà Giang - Tỉnh Hà Giang', NULL, '0946540099', '', 'hth.3490@gmail.com', 'Bên bạn còn thiếu của mình 1 chai Blue Curacao từ đơn hàng trước, lần này chuyển luôn giùm mình nhé', NULL, NULL, NULL, 1438249021, 1438249021, '116.111.89.250', 'vi', '1438189200000'),
(16, NULL, 2, 0, 'Đoàn Thị Thu Thanh', '', '2015-07-28 16:53:39', NULL, 202000, 0, '', 2, NULL, 'số 14 tổ 5 Thị trấn Sông Mã tỉnh Sơn La', NULL, '0969141800', '', 'ronina.kitten@gmail.com', '', NULL, NULL, NULL, 1438077219, 1438077219, '113.181.210.71', 'vi', '1438016400000'),
(20, NULL, 1, 0, 'Hoàng Thanh Hiều', '', '2015-08-11 16:18:22', NULL, 1686000, 0, '', 3, NULL, 'Số 162A- Đường Trần Phú - Phường Minh Khai - Thành phố Hà Giang - Tỉnh Hà Giang', NULL, '0946540099', '', 'hth.3490@gmail.com', 'Bên bạn gửi xe Bằng Phấn như mọi khi giúp mình nhé', NULL, NULL, NULL, 1439284702, 1439284702, '116.111.16.146', 'vi', '1439226000000'),
(19, NULL, 2, 0, 'Nguyễn Thùy Dung', '', '2015-08-05 09:26:25', NULL, 331500, 0, '', 3, NULL, 'Liên minh HTX tỉnh Bắc Giang, số 33A Vương Văn Trà, TP Bắc Giang', NULL, '0972574555', '', 'nguyenthuydung.lmhtx@gmail.com', '', NULL, NULL, NULL, 1438741585, 1438741585, '1.55.144.223', 'vi', '1438707600000'),
(24, NULL, 2, 0, 'Nguyễn Xuân Cường', '', '2015-08-22 15:24:21', NULL, 1247000, 0, '', 0, NULL, '17.01 T1 Chung cư TSQ, Mỗ Lao, Hà Đông', NULL, '0988878533', '', 's3426544@rmit.edu.vn', 'Ship trong ngày hnay', NULL, NULL, NULL, 1440231861, 1440231861, '58.187.115.164', 'vi', '1440176400000'),
(25, NULL, 1, 0, 'Nguyen Ngoc Son', '', '2016-04-22 16:02:05', NULL, 10, 0, '', 0, NULL, '187 A Minh Khai', NULL, '097 999 64 22', '', 'ngocson1188@gmail.com', '', NULL, NULL, NULL, 1461315725, 1461315725, '127.0.0.1', 'vi', '1461258000000');

-- --------------------------------------------------------

--
-- Structure de la table `itnl_orders_details`
--

CREATE TABLE IF NOT EXISTS `itnl_orders_details` (
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `size` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_orders_details`
--

INSERT INTO `itnl_orders_details` (`order_id`, `product_id`, `quantity`, `price`, `size`) VALUES
(1, 12, 1, 120000, '0'),
(1, 2, 1, 80000, '0'),
(2, 12, 1, 120000, '0'),
(2, 2, 1, 80000, '0'),
(2, 4, 1, 85000, '0'),
(3, 2, 1, 80000, '0'),
(3, 4, 1, 85000, '0'),
(3, 5, 1, 40000, '0'),
(4, 2, 1, 80000, '0'),
(5, 2, 1, 80000, '0'),
(6, 2, 1, 80000, '0'),
(7, 2, 1, 80000, '0'),
(0, 2, 1, 80000, '0'),
(8, 23, 1, 42000, '0'),
(9, 16, 1, 134000, '0'),
(10, 56, 1, 134000, '0'),
(11, 28, 1, 140000, '0'),
(12, 28, 1, 140000, '0'),
(12, 35, 1, 1750000, '0'),
(13, 73, 1, 77000, '0'),
(14, 34, 2, 22000, '0'),
(15, 51, 4, 65000, '0'),
(15, 38, 5, 37000, '0'),
(15, 110, 1, 349000, '0'),
(15, 88, 1, 120000, '0'),
(16, 26, 2, 80000, '0'),
(16, 82, 1, 42000, '0'),
(17, 222, 6, 85000, '0'),
(17, 34, 8, 22000, '0'),
(17, 176, 2, 115000, '0'),
(17, 266, 1, 170000, '0'),
(17, 245, 1, 170000, '0'),
(17, 238, 1, 170000, '0'),
(17, 29, 2, 85000, '0'),
(18, 28, 1, 140000, '0'),
(19, 28, 1, 140000, '0'),
(19, 33, 1, 61500, '0'),
(19, 51, 2, 65000, '0'),
(20, 33, 6, 61500, '0'),
(20, 51, 4, 65000, '0'),
(20, 232, 1, 352000, '0'),
(20, 224, 1, 113000, '0'),
(20, 170, 1, 92000, '0'),
(20, 169, 3, 130000, '0'),
(20, 34, 5, 22000, '0'),
(21, 26, 1, 80000, '0'),
(21, 28, 1, 140000, '0'),
(22, 27, 50, 160000, '0'),
(22, 26, 50, 80000, '0'),
(22, 184, 5, 210000, '0'),
(22, 271, 5, 210000, '0'),
(22, 29, 5, 85000, '0'),
(22, 217, 5, 140000, '0'),
(22, 308, 5, 210000, '0'),
(22, 178, 10, 210000, '0'),
(0, 27, 50, 160000, '0'),
(0, 26, 50, 80000, '0'),
(0, 184, 5, 210000, '0'),
(0, 271, 5, 210000, '0'),
(0, 29, 5, 85000, '0'),
(0, 217, 5, 140000, '0'),
(0, 308, 5, 210000, '0'),
(0, 178, 10, 210000, '0'),
(23, 40, 2, 37000, '0'),
(24, 64, 3, 99000, '0'),
(24, 73, 2, 77000, '0'),
(24, 68, 2, 96000, '0'),
(24, 66, 2, 69000, '0'),
(24, 69, 2, 73000, '0'),
(24, 72, 2, 96000, '0'),
(24, 70, 2, 64000, '0'),
(25, 408, 1, 10, 'S');

-- --------------------------------------------------------

--
-- Structure de la table `itnl_pages`
--

CREATE TABLE IF NOT EXISTS `itnl_pages` (
`id` int(11) NOT NULL,
  `title` varchar(256) DEFAULT NULL,
  `uri` varchar(255) NOT NULL,
  `content` text,
  `created_date` datetime DEFAULT NULL,
  `viewed` int(11) DEFAULT '0',
  `summary` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `meta_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `meta_keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `tags` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` tinyint(4) DEFAULT '1',
  `lang` varchar(20) DEFAULT 'vi',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_pages`
--

INSERT INTO `itnl_pages` (`id`, `title`, `uri`, `content`, `created_date`, `viewed`, `summary`, `meta_title`, `meta_keywords`, `meta_description`, `tags`, `status`, `lang`, `creator`, `editor`) VALUES
(1, 'Giới thiệu', '/gioi-thieu.html', '<p><strong>TRUMNGUYENLIEU</strong>&nbsp;c&oacute; mặt tr&ecirc;n thị trường từ đầu năm 2015, l&agrave; đại diện v&agrave; l&agrave; nh&agrave; ph&acirc;n phối cho nhiều h&atilde;ng lớn, chuy&ecirc;n ph&acirc;n phối tất cả nguy&ecirc;n liệu đồ uống v&agrave; nguy&ecirc;n liệu sản xuất cho ng&agrave;nh thực phẩm của nhiều nh&atilde;n h&agrave;ng nổi tiếng tr&ecirc;n thế giới.</p>\n<p><strong>TRUMNGUYENLIEU</strong> ra đời nhằm đem đến cho c&aacute;c kh&aacute;ch h&agrave;ng:</p>\n<ul>\n<li>Giải ph&aacute;p nguồn nguy&ecirc;n liệu tập trung, ổn định, chất lượng.</li>\n<li>C&ugrave;ng đi giải quyết c&aacute;c vấn đề kh&oacute; khăn của kh&aacute;ch h&agrave;ng.</li>\n<li>Dịch vụ chuyển h&agrave;ng h&oacute;a đến tận nơi cho kh&aacute;ch h&agrave;ng.</li>\n</ul>\n<p>Ch&uacute;ng t&ocirc;i lu&ocirc;n đề ra&nbsp; phương ch&acirc;m&nbsp;<strong>C&Ugrave;NG PH&Aacute;T TRIỂN VỚI KH&Aacute;CH H&Agrave;NG</strong>&nbsp;l&agrave; nền tảng x&acirc;y dựng <strong>TRUMNGUYENLIEU</strong>&nbsp;đến ng&agrave;y nay v&agrave; tương lai.</p>\n<p><strong><span style="text-decoration: underline;">TRỤ SỞ CH&Iacute;NH :</span></strong><br /> Số 53 ng&aacute;ch 1, ng&otilde; 168, phố H&agrave;o Nam, TP H&agrave; Nội.<br /> ĐT: +04 6275 0383</p>\n<p>Email :&nbsp;<a href="mailto:trumnguyenlieu@gmail.com">trumnguyenlieu@gmail.com</a></p>\n<p>Website: <a href="http://www.trumnguyenlieu.vn">www.trumnguyenlieu.vn</a></p>\n<p>Fanpage: <a href="http://facebook.com/trumnguyenlieu">http://facebook.com/trumnguyenlieu</a></p>', '2015-02-06 13:43:18', 288, '', '', '', '', '', 1, 'vi', 1, 1),
(5, 'Thanh toán linh hoạt', '/thanh-toan-linh-hoat.html', '<p>Thanh to&aacute;n linh hoạt</p>', '2015-05-23 22:01:21', 52, 'Thanh toán linh hoạt', '', '', '', '', 1, 'vi', 1, 1),
(6, 'Giao hàng toàn quốc', '/giao-hang-toan-quoc.html', '<p>Giao h&agrave;ng to&agrave;n quốc</p>', '2015-05-23 13:56:09', 51, 'Giao hàng toàn quốc', '', '', '', '', 1, 'vi', 1, 1),
(14, 'Hướng Dẫn Mua Hàng', '/huong-dan-mua-hang.html', '<p>xyz</p>', '2015-10-11 22:29:13', 0, 'abc', '', '', '', '', 1, 'vi', 1, 1),
(15, 'Hướng Dẫn Thanh Toán', '/huong-dan-thanh-toan.html', '<p>xyz</p>', '2015-10-11 22:31:13', 3, 'abc', '', '', '', '', 1, 'vi', 1, 1),
(16, 'Liên Hệ', '/lien-he.html', '<p>avvv</p>', '2015-10-11 22:34:21', 0, 'đsd', '', '', '', '', 1, 'vi', 1, 1),
(17, 'Thảnh thơi mua sắm', '/thanh-thoi-mua-sam.html', '<div class=" item policy">\r\n<h5 class="title">Thảnh thơi mua sắm</h5>\r\n<ul>\r\n<li><a href="#"> <img class="icon1" src="http://eshop.de/images/safe1.png" alt="Icon" /> <img class="icon2" src="http://eshop.de/images/safe2.png" alt="Icon" /> Safe Payment </a></li>\r\n<li><a href="#"> <img class="icon1" src="http://eshop.de/images/icon2.png" alt="Icon" /> <img class="icon2" src="http://eshop.de/images/icon2-1.png" alt="Icon" /> Worldwide Delivery</a></li>\r\n<li><a href="#"> <img class="icon1" src="http://eshop.de/images/icon3.png" alt="Icon" /> <img class="icon2" src="http://eshop.de/images/icon3-1.png" alt="Icon" /> Change Pay</a></li>\r\n<li><a href="#"> <img class="icon1" src="http://eshop.de/images/icon4.png" alt="Icon" /> <img class="icon2" src="http://eshop.de/images/icon4-1.png" alt="Icon" /> Customer Service</a></li>\r\n<li><a href="#"> <img class="icon1" src="http://eshop.de/images/icon5.png" alt="Icon" /> <img class="icon2" src="http://eshop.de/images/icon5-1.png" alt="Icon" /> Sales Of Prestige</a></li>\r\n</ul>\r\n</div>\r\n<div class="item banner-opacity"><a href="#"><img src="http://eshop.de/images/banner12.jpg" alt="Banner" /></a></div>\r\n<div class="banner-opacity"><a href="#"><img src="http://eshop.de/images/banner13.jpg" alt="Banner" /></a></div>', '2015-11-11 15:23:42', 0, '......', '', '', '', '', 1, 'vi', 1, 1),
(18, 'Chăm sóc khách hàng', '/cham-soc-khach-hang.html', '<p>&nbsp;</p>\r\n<!-- Baner bottom -->\r\n<div class="row banner-bottom">\r\n<div class="col-sm-6 item-left">\r\n<div class="banner-boder-zoom"><a href="#"><img class="img-responsive" src="http://eshop.de/images/data/banner-botom1.jpg" alt="ads" width="586" height="120" /></a></div>\r\n</div>\r\n<div class="col-sm-6 item-right">\r\n<div class="banner-boder-zoom"><a href="#"><img class="img-responsive" src="http://eshop.de/images/data/banner-bottom2.jpg" alt="ads" width="583" height="120" /></a></div>\r\n</div>\r\n</div>\r\n<!-- end banner bottom -->\r\n<p>&nbsp;</p>\r\n<!-- service 2 -->\r\n<div class="services2">\r\n<ul>\r\n<li class="col-xs-12 col-sm-6 col-md-4 services2-item">\r\n<div class="service-wapper">\r\n<div class="row">\r\n<div class="col-sm-6 image">\r\n<div class="icon"><img src="http://eshop.de/images/data/icon-s1.png" alt="service" width="64" height="64" /></div>\r\n<h3 class="title"><a href="#">Great Value</a></h3>\r\n</div>\r\n<div class="col-sm-6 text">We offer competitive prices on our 100 million plus product range.</div>\r\n</div>\r\n</div>\r\n</li>\r\n<li class="col-xs-12 col-sm-6 col-md-4 services2-item">\r\n<div class="service-wapper">\r\n<div class="row">\r\n<div class="col-sm-6 image">\r\n<div class="icon"><img src="http://eshop.de/images/data/icon-s2.png" alt="service" width="64" height="64" /></div>\r\n<h3 class="title"><a href="#">Worldwide Delivery</a></h3>\r\n</div>\r\n<div class="col-sm-6 text">With sites in 5 languages, we ship to over 200 countries &amp; regions.</div>\r\n</div>\r\n</div>\r\n</li>\r\n<li class="col-xs-12 col-sm-6 col-md-4 services2-item">\r\n<div class="service-wapper">\r\n<div class="row">\r\n<div class="col-sm-6 image">\r\n<div class="icon"><img src="http://eshop.de/images/data/icon-s3.png" alt="service" width="64" height="64" /></div>\r\n<h3 class="title"><a href="#">Safe Payment</a></h3>\r\n</div>\r\n<div class="col-sm-6 text">Pay with the world&rsquo;s most popular and secure payment methods.</div>\r\n</div>\r\n</div>\r\n</li>\r\n<li class="col-xs-12 col-sm-6 col-md-4 services2-item">\r\n<div class="service-wapper">\r\n<div class="row">\r\n<div class="col-sm-6 image">\r\n<div class="icon"><img src="http://eshop.de/images/data/icon-s4.png" alt="service" width="64" height="64" /></div>\r\n<h3 class="title"><a href="#">Shop with Confidence</a></h3>\r\n</div>\r\n<div class="col-sm-6 text">Our Buyer Protection covers your purchase from click to delivery.</div>\r\n</div>\r\n</div>\r\n</li>\r\n<li class="col-xs-12 col-sm-6 col-md-4 services2-item">\r\n<div class="service-wapper">\r\n<div class="row">\r\n<div class="col-sm-6 image">\r\n<div class="icon"><img src="http://eshop.de/images/data/icon-s5.png" alt="service" width="64" height="64" /></div>\r\n<h3 class="title"><a href="#">24/7 Help Center</a></h3>\r\n</div>\r\n<div class="col-sm-6 text">Round-the-clock assistance for a smooth shopping experience.</div>\r\n</div>\r\n</div>\r\n</li>\r\n<li class="col-xs-12 col-sm-6 col-md-4 services2-item">\r\n<div class="service-wapper">\r\n<div class="row">\r\n<div class="col-sm-6 image">\r\n<div class="icon"><img src="http://eshop.de/images/data/icon-s6.png" alt="service" width="64" height="64" /></div>\r\n<h3 class="title"><a href="#">Shop On-The-Go</a></h3>\r\n</div>\r\n<div class="col-sm-6 text">Download the app and get the world of Newshop at your fingertips.</div>\r\n</div>\r\n</div>\r\n</li>\r\n</ul>\r\n</div>\r\n<!-- ./service 2 -->', '2015-11-11 15:29:26', 0, '....', '', '', '', '', 1, 'vi', 1, 1),
(19, 'Host keyword', '/host-keyword.html', '<div class="keyword">\r\n<p class="lebal">Keywords:</p>\r\n<p><a href="#">T-shirt, </a> <a href="#">Leggings, </a> <a href="#">Cotton, ...</a></p>\r\n</div>', '2015-11-11 15:50:06', 0, '....', '', '', '', '', 1, 'vi', 1, 1),
(20, 'Menu nhe', '/menu-nhe.html', '<div id="navbar" class="navbar-collapse collapse">\r\n<ul class="nav navbar-nav">\r\n<li class="active"><a href="#">Home</a></li>\r\n<li class="dropdown"><a class="dropdown-toggle" href="http://eshop.de/dashboard/pages/category.html">Fashion</a>\r\n<ul class="dropdown-menu mega_dropdown" style="width: 830px;">\r\n<li class="block-container col-sm-3">\r\n<ul class="block">\r\n<li class="img_container"><a href="#"> <img class="img-responsive" src="http://eshop.de/dashboard/pages/assets/data/men.png" alt="sport" /> </a></li>\r\n<li class="link_container group_header"><a href="#">MEN''S</a></li>\r\n<li class="link_container"><a href="#">Skirts</a></li>\r\n<li class="link_container"><a href="#">Jackets</a></li>\r\n<li class="link_container"><a href="#">Tops</a></li>\r\n<li class="link_container"><a href="#">Scarves</a></li>\r\n<li class="link_container"><a href="#">Pants</a></li>\r\n</ul>\r\n</li>\r\n<li class="block-container col-sm-3">\r\n<ul class="block">\r\n<li class="img_container"><a href="#"> <img class="img-responsive" src="http://eshop.de/dashboard/pages/assets/data/women.png" alt="sport" /> </a></li>\r\n<li class="link_container group_header"><a href="#">WOMEN''S</a></li>\r\n<li class="link_container"><a href="#">Skirts</a></li>\r\n<li class="link_container"><a href="#">Jackets</a></li>\r\n<li class="link_container"><a href="#">Tops</a></li>\r\n<li class="link_container"><a href="#">Scarves</a></li>\r\n<li class="link_container"><a href="#">Pants</a></li>\r\n</ul>\r\n</li>\r\n<li class="block-container col-sm-3">\r\n<ul class="block">\r\n<li class="img_container"><a href="#"> <img class="img-responsive" src="http://eshop.de/dashboard/pages/assets/data/kid.png" alt="sport" /> </a></li>\r\n<li class="link_container group_header"><a href="#">Kids</a></li>\r\n<li class="link_container"><a href="#">Shoes</a></li>\r\n<li class="link_container"><a href="#">Clothing</a></li>\r\n<li class="link_container"><a href="#">Tops</a></li>\r\n<li class="link_container"><a href="#">Scarves</a></li>\r\n<li class="link_container"><a href="#">Accessories</a></li>\r\n</ul>\r\n</li>\r\n<li class="block-container col-sm-3">\r\n<ul class="block">\r\n<li class="img_container"><a href="#"> <img class="img-responsive" src="http://eshop.de/dashboard/pages/assets/data/trending.png" alt="sport" /> </a></li>\r\n<li class="link_container group_header"><a href="#">TRENDING</a></li>\r\n<li class="link_container"><a href="#">Men''s Clothing</a></li>\r\n<li class="link_container"><a href="#">Kid''s Clothing</a></li>\r\n<li class="link_container"><a href="#">Women''s Clothing</a></li>\r\n<li class="link_container"><a href="#">Accessories</a></li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</li>\r\n<li><a class="dropdown-toggle" href="http://eshop.de/dashboard/pages/category.html" data-toggle="dropdown">Sports</a></li>\r\n<li class="dropdown"><a class="dropdown-toggle" href="http://eshop.de/dashboard/pages/category.html">Foods</a>\r\n<ul class="mega_dropdown dropdown-menu" style="width: 830px;">\r\n<li class="block-container col-sm-3">\r\n<ul class="block">\r\n<li class="link_container group_header"><a href="#">Asian</a></li>\r\n<li class="link_container"><a href="#">Vietnamese Pho</a></li>\r\n<li class="link_container"><a href="#">Noodles</a></li>\r\n<li class="link_container"><a href="#">Seafood</a></li>\r\n<li class="link_container group_header"><a href="#">Sausages</a></li>\r\n<li class="link_container"><a href="#">Meat Dishes</a></li>\r\n<li class="link_container"><a href="#">Desserts</a></li>\r\n<li class="link_container"><a href="#">Tops</a></li>\r\n<li class="link_container"><a href="#">Tops</a></li>\r\n</ul>\r\n</li>\r\n<li class="block-container col-sm-3">\r\n<ul class="block">\r\n<li class="link_container group_header"><a href="#">European</a></li>\r\n<li class="link_container"><a href="#">Greek Potatoes</a></li>\r\n<li class="link_container"><a href="#">Famous Spaghetti</a></li>\r\n<li class="link_container"><a href="#">Famous Spaghetti</a></li>\r\n<li class="link_container group_header"><a href="#">Chicken</a></li>\r\n<li class="link_container"><a href="#">Italian Pizza</a></li>\r\n<li class="link_container"><a href="#">French Cakes</a></li>\r\n<li class="link_container"><a href="#">Tops</a></li>\r\n<li class="link_container"><a href="#">Tops</a></li>\r\n</ul>\r\n</li>\r\n<li class="block-container col-sm-3">\r\n<ul class="block">\r\n<li class="link_container group_header"><a href="#">FAST</a></li>\r\n<li class="link_container"><a href="#">Hamberger</a></li>\r\n<li class="link_container"><a href="#">Pizza</a></li>\r\n<li class="link_container"><a href="#">Noodles</a></li>\r\n<li class="link_container group_header"><a href="#">Sandwich</a></li>\r\n<li class="link_container"><a href="#">Salad</a></li>\r\n<li class="link_container"><a href="#">Paste</a></li>\r\n<li class="link_container"><a href="#">Tops</a></li>\r\n<li class="link_container"><a href="#">Tops</a></li>\r\n</ul>\r\n</li>\r\n<li class="block-container col-sm-3">\r\n<ul class="block">\r\n<li class="img_container"><img src="http://eshop.de/dashboard/pages/assets/data/banner-topmenu.jpg" alt="Banner" /></li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</li>\r\n<li class="dropdown"><a class="dropdown-toggle" href="http://eshop.de/dashboard/pages/category.html">Digital<span class="notify notify-right">new</span></a>\r\n<ul class="dropdown-menu container-fluid">\r\n<li class="block-container">\r\n<ul class="block">\r\n<li class="link_container"><a href="#">Mobile</a></li>\r\n<li class="link_container"><a href="#">Tablets</a></li>\r\n<li class="link_container"><a href="#">Laptop</a></li>\r\n<li class="link_container"><a href="#">Memory Cards</a></li>\r\n<li class="link_container"><a href="#">Accessories</a></li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</li>\r\n<li><a href="http://eshop.de/dashboard/pages/category.html">Furniture</a></li>\r\n<li><a href="http://eshop.de/dashboard/pages/category.html">Jewelry</a></li>\r\n<li><a href="http://eshop.de/dashboard/pages/category.html">Blog</a></li>\r\n</ul>\r\n<ul class="nav navbar-nav navbar-right">\r\n<li><a class="link-buytheme" href="#"> Buy This Template</a></li>\r\n</ul>\r\n</div>', '2015-12-01 10:38:26', 0, '...', '', '', '', '', 1, 'vi', 1, 1),
(21, 'quang cao side left', '/quang-cao-side-left.html', '<div class=""><a href="#"> <img src="http://eshop.de/images/slide-left.jpg" alt="" /> </a></div>', '2015-12-02 11:29:15', 0, '', '', '', '', '', 1, 'vi', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_products`
--

CREATE TABLE IF NOT EXISTS `itnl_products` (
`id` int(11) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL COMMENT 'Cộng tác  viên',
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `update_time` int(11) NOT NULL,
  `price` double DEFAULT NULL,
  `price_old` double NOT NULL DEFAULT '0',
  `summary` text,
  `description` text,
  `code` varchar(255) DEFAULT NULL,
  `manufacturer` text NOT NULL COMMENT 'nha che tao',
  `specifications` text NOT NULL COMMENT 'thong so ky thuat',
  `guarantee` varchar(255) NOT NULL COMMENT 'bao hanh',
  `quantum` varchar(255) NOT NULL COMMENT 'luong tu',
  `origin_id` int(11) NOT NULL,
  `trademark_id` int(11) NOT NULL,
  `state_id` tinyint(1) NOT NULL,
  `colors` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `material_id` int(11) NOT NULL,
  `style_id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `categories_id` int(11) DEFAULT NULL,
  `viewed` int(11) DEFAULT '0',
  `position` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT '1',
  `home` tinyint(1) NOT NULL DEFAULT '0',
  `top_seller` tinyint(4) DEFAULT NULL,
  `typical` tinyint(1) NOT NULL DEFAULT '0',
  `new` tinyint(1) NOT NULL DEFAULT '0',
  `tags` text,
  `unit_id` tinyint(4) DEFAULT NULL,
  `unit_id_old` tinyint(4) NOT NULL,
  `unit_id_real` tinyint(4) NOT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `meta_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `meta_keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `meta_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0',
  `link_demo` varchar(500) NOT NULL DEFAULT '#',
  `custom_buy_id` int(11) NOT NULL,
  `buy_name` varchar(255) NOT NULL,
  `custom_duoc_id` int(11) NOT NULL,
  `custom_duoc_name` varchar(255) NOT NULL,
  `cmt_custom_duoc` varchar(255) NOT NULL,
  `contract_date` datetime DEFAULT NULL,
  `commission` varchar(255) NOT NULL,
  `price_real` double NOT NULL,
  `date_of_payment` datetime DEFAULT NULL,
  `re_costs` double DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=517 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Contenu de la table `itnl_products`
--

INSERT INTO `itnl_products` (`id`, `product_name`, `created_date`, `updated_date`, `update_time`, `price`, `price_old`, `summary`, `description`, `code`, `manufacturer`, `specifications`, `guarantee`, `quantum`, `origin_id`, `trademark_id`, `state_id`, `colors`, `size`, `material_id`, `style_id`, `users_id`, `categories_id`, `viewed`, `position`, `status`, `home`, `top_seller`, `typical`, `new`, `tags`, `unit_id`, `unit_id_old`, `unit_id_real`, `lang`, `meta_title`, `meta_keywords`, `meta_description`, `creator`, `editor`, `link_demo`, `custom_buy_id`, `buy_name`, `custom_duoc_id`, `custom_duoc_name`, `cmt_custom_duoc`, `contract_date`, `commission`, `price_real`, `date_of_payment`, `re_costs`) VALUES
(515, '', NULL, '2016-09-25 17:10:34', 1474798234, 0, 0, NULL, NULL, '', '', '', '', '', -1, -1, 0, '', '1', 0, -1, NULL, -1, 0, 1, 1, 0, NULL, 0, 0, NULL, 0, 0, 0, 'vi', '', '', NULL, 0, 1, '#', 8, 'Bạch cốt tinh', 9, 'Lưu Hùng Kiên', '012346789', '2030-04-17 08:00:00', '', 0, '2030-04-17 08:00:00', 0),
(516, '', NULL, '2016-09-25 17:10:52', 1474798252, 0, 0, NULL, NULL, '', '', '', '', '', -1, -1, 0, '', '1', 0, -1, NULL, -1, 0, 2, 1, 0, NULL, 0, 0, NULL, 0, 0, 0, 'vi', '', '', NULL, 0, 1, '#', 8, 'Bạch cốt tinh', 11, 'Lưu Hùng Kiên Định', '01245789', '2033-04-20 08:00:00', '', 0, '2033-04-20 08:00:00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_products_categories`
--

CREATE TABLE IF NOT EXISTS `itnl_products_categories` (
`id` int(11) NOT NULL,
  `category` varchar(256) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `level` tinyint(4) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `thumbnail` varchar(500) NOT NULL,
  `avatar` varchar(500) NOT NULL,
  `summary` text NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `meta_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `meta_keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `home` tinyint(1) NOT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0',
  `link` varchar(500) NOT NULL,
  `css` varchar(255) NOT NULL,
  `highlight` tinyint(1) NOT NULL DEFAULT '0',
  `trade_id` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_products_categories`
--

INSERT INTO `itnl_products_categories` (`id`, `category`, `parent_id`, `level`, `position`, `thumbnail`, `avatar`, `summary`, `content`, `status`, `meta_title`, `meta_keywords`, `meta_description`, `home`, `lang`, `creator`, `editor`, `link`, `css`, `highlight`, `trade_id`) VALUES
(95, 'AND', 0, 0, 10, '', '', '', '', 1, '0', '0', '0', 1, 'vi', 1, 1, '0', '0', 0, 28),
(94, 'Dịch vụ Visa', 0, 0, 9, '', '', '', '', 1, '', '', '', 1, 'vi', 1, 1, '', '', 0, NULL),
(93, 'Nhân thọ', 0, 0, 8, '', '', '', '', 1, '0', '0', '0', 1, 'vi', 1, 1, '0', '0', 0, 29),
(92, 'Tái tục BHSK', 0, 0, 7, '', '', '', '', 1, '0', '0', '0', 1, 'vi', 1, 1, '0', '0', 0, 29),
(91, 'Tai nạn', 0, 0, 6, '', '', '', '', 1, '', '', '', 1, 'vi', 1, 1, '', '', 0, NULL),
(90, 'Cháy nổ', 0, 0, 5, '', '', '', '', 1, '', '', '', 1, 'vi', 1, 1, '', '', 0, NULL),
(89, 'Hàng hóa', 0, 0, 4, '', '', '', '', 1, '', '', '', 1, 'vi', 1, 1, '', '', 0, NULL),
(88, 'Du lịch', 0, 0, 3, '', '', '', '', 1, '0', '0', '0', 1, 'vi', 1, 1, '0', '0', 0, 29),
(86, 'Sức khỏe', 0, 0, 11, '', '', '', '', 1, '0', '0', '0', 1, 'vi', 1, 1, '0', '0', 0, 29),
(87, 'Ô tô', 0, 0, 2, '', '', '', '', 1, '0', '0', '0', 1, 'vi', 1, 1, '0', '0', 0, 29),
(98, 'Chi nhánh đông đô', 0, 0, 12, '', '', '', '', 1, '0', '0', '0', 1, 'vi', 1, 1, '', '', 0, 29);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_products_color`
--

CREATE TABLE IF NOT EXISTS `itnl_products_color` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `creator` int(11) NOT NULL,
  `editor` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_products_coupon`
--

CREATE TABLE IF NOT EXISTS `itnl_products_coupon` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `discount_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'kiểu giảm giá',
  `discount` double NOT NULL COMMENT 'số lượng giảm',
  `price_min` double NOT NULL DEFAULT '0',
  `end_date` datetime NOT NULL,
  `number` int(11) NOT NULL DEFAULT '1' COMMENT 'số lần sử dụng',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT 'số lượng mã',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `creator` int(11) NOT NULL DEFAULT '1',
  `editor` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_products_coupon`
--

INSERT INTO `itnl_products_coupon` (`id`, `name`, `discount_type`, `discount`, `price_min`, `end_date`, `number`, `count`, `status`, `creator`, `editor`) VALUES
(7, 'khuyen mai thang 9', 1, 10, 0, '2015-10-07 16:45:00', 1, 3, 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_products_coupon_item`
--

CREATE TABLE IF NOT EXISTS `itnl_products_coupon_item` (
  `code` varchar(255) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_products_coupon_item`
--

INSERT INTO `itnl_products_coupon_item` (`code`, `coupon_id`, `status`) VALUES
('T16PS156WE', 7, 1),
('687I8FKVIP', 7, 1),
('CE7UHAU1Q6', 7, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_products_material`
--

CREATE TABLE IF NOT EXISTS `itnl_products_material` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `creator` int(11) NOT NULL,
  `editor` int(11) NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `trade_id` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_products_material`
--

INSERT INTO `itnl_products_material` (`id`, `name`, `status`, `creator`, `editor`, `cat_id`, `trade_id`) VALUES
(7, '1.2', 1, 1, 1, 88, 29),
(5, '3.5', 1, 1, 1, 87, 29),
(6, '0.45', 1, 1, 1, 86, 29);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_products_origin`
--

CREATE TABLE IF NOT EXISTS `itnl_products_origin` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_products_origin`
--

INSERT INTO `itnl_products_origin` (`id`, `name`, `status`, `creator`, `editor`) VALUES
(22, 'Hotline công ty', 1, 1, 1),
(21, 'Khách BV Nhi', 1, 1, 1),
(20, 'Tự tìm kiếm', 1, 1, 1),
(23, 'Live chat công ty', 1, 1, 1),
(24, 'Khác', 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_products_size`
--

CREATE TABLE IF NOT EXISTS `itnl_products_size` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `creator` int(11) NOT NULL,
  `editor` int(11) NOT NULL,
  `gh_tv_tn` varchar(255) NOT NULL,
  `tt_bp` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_products_size`
--

INSERT INTO `itnl_products_size` (`id`, `name`, `status`, `creator`, `editor`, `gh_tv_tn`, `tt_bp`) VALUES
(1, 'Gói bảo hiểm A', 1, 1, 1, '100000000', '10'),
(2, 'Gói bảo hiểm B', 1, 1, 1, '', ''),
(3, 'Gói bảo hiểm C', 1, 1, 1, '', ''),
(4, 'Gói bảo hiểm D', 1, 1, 1, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `itnl_products_state`
--

CREATE TABLE IF NOT EXISTS `itnl_products_state` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `creator` int(11) NOT NULL,
  `editor` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_products_state`
--

INSERT INTO `itnl_products_state` (`id`, `name`, `status`, `creator`, `editor`) VALUES
(1, 'Chưa duyệt', 1, 1, 1),
(2, 'Đã duyệt', 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_products_style`
--

CREATE TABLE IF NOT EXISTS `itnl_products_style` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `creator` int(11) NOT NULL,
  `editor` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_products_style`
--

INSERT INTO `itnl_products_style` (`id`, `name`, `status`, `creator`, `editor`) VALUES
(1, 'TM', 1, 1, 1),
(2, 'VCB', 1, 1, 1),
(3, 'VCB_VVT', 1, 1, 1),
(4, 'VCB_LHK', 1, 1, 1),
(5, 'BIDV', 1, 1, 1),
(6, 'SACOMBANK_EBH', 1, 1, 1),
(7, 'SACOMBANK_LHK', 1, 1, 1),
(8, 'CK_NHÀ BH GỐC', 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_products_trademark`
--

CREATE TABLE IF NOT EXISTS `itnl_products_trademark` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_products_trademark`
--

INSERT INTO `itnl_products_trademark` (`id`, `name`, `slug`, `status`, `creator`, `editor`) VALUES
(29, 'Bảo việt Hà Nội - D39', 'bao-viet-ha-noi-d39', 1, 1, 1),
(28, 'Bảo việt Đông Đô', 'bao-viet-dong-do', 1, 1, 1),
(30, 'Bảo việt Hà Nội - D26', 'bao-viet-ha-noi-d26', 1, 1, 1),
(31, 'PVI Hà Nội', 'pvi-ha-noi', 1, 1, 1),
(32, 'PVI Đông Đô', 'pvi-dong-do', 1, 1, 1),
(33, 'Pvi Sông Hồng', 'pvi-song-hong', 1, 1, 1),
(34, 'VNI Hà Nội', 'vni-ha-noi', 1, 1, 1),
(35, 'Blue Cross', 'blue-cross', 1, 1, 1),
(36, 'Liberty', 'liberty', 1, 1, 1),
(37, 'UIC', 'uic', 1, 1, 1),
(38, 'Bảo Minh Đông Đô', 'bao-minh-dong-do', 1, 1, 1),
(39, 'PJICO', 'pjico', 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_product_images`
--

CREATE TABLE IF NOT EXISTS `itnl_product_images` (
`id` int(11) NOT NULL,
  `image_name` varchar(256) NOT NULL,
  `position` tinyint(1) DEFAULT '0',
  `products_id` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=533 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Contenu de la table `itnl_product_images`
--

INSERT INTO `itnl_product_images` (`id`, `image_name`, `position`, `products_id`) VALUES
(492, 'diuembemaudendo_351_29daa.jpg', 2, 351),
(454, '34916331.jpg', 1, 349),
(455, '35016b66.jpg', 1, 350),
(451, '3462d44f.png', 1, 346),
(453, '34727ecd.jpg', 1, 347),
(448, '3441ba54.jpg', 1, 344),
(450, '3451bf7d.gif', 1, 345),
(487, 'tuinhaichonghoc_325_2a90c.jpg', 2, 325),
(517, 'bomuongbaonhiet2cai_333_3372a.jpg', 2, 333),
(485, 'bomuongvadiatapan_329_29d46.jpg', 1, 329),
(527, 'bobatmuongchobe_330_3473f.jpg', 1, 330),
(503, 'keocatvadamthucan_342_31003.jpg', 3, 342),
(500, 'tuinhaisiliconroithaythe_339_2f3b0.jpg', 2, 339),
(495, 'hopchiasua3ngan_343_2ee67.jpg', 1, 343),
(494, 'dungcunghienthucanbangtay_341_2576d.jpg', 1, 341),
(512, 'bochamsocmongtaychobe_336_319e3.jpg', 1, 336),
(507, 'bonhogiotvacocdungthuoc_337_315a0.jpg', 2, 337),
(522, 'banchaidanhrangcoluclac_334_34161.jpg', 3, 334),
(513, 'tuidulich9mon_335_32010.jpg', 2, 335),
(526, 'bobanchaitapdanhrang_331_34421.jpg', 1, 331),
(417, '32822f03.jpg', 1, 328),
(416, '3271d8b0.jpg', 1, 327),
(411, '3021be18.png', 1, 302),
(412, '3011be50.jpg', 1, 301),
(409, '2681bcf0.JPG', 1, 268),
(410, '2701bd0e.jpg', 1, 270),
(406, '2751bc46.jpg', 1, 275),
(408, '2711bca0.jpg', 1, 271),
(404, '2741bb96.jpg', 1, 274),
(405, '2721bbf2.jpg', 1, 272),
(402, '2731bb31.jpg', 1, 273),
(403, '2691bb78.jpg', 1, 269),
(400, '2781baca.jpg', 2, 278),
(401, '2761bb18.jpg', 1, 276),
(398, '2771ba9d.jpg', 1, 277),
(399, '2781bac1.jpg', 1, 278),
(396, '2801ba3c.jpg', 1, 280),
(397, '2791ba56.jpg', 1, 279),
(394, '2821b9da.jpg', 1, 282),
(395, '2811b9f9.jpg', 1, 281),
(392, '2831b976.jpg', 1, 283),
(393, '2841b994.jpg', 1, 284),
(390, '2861b912.jpg', 1, 286),
(391, '2851b931.jpg', 1, 285),
(388, '2881b8ac.jpg', 1, 288),
(389, '2871b8ce.jpg', 1, 287),
(386, '2911b80b.jpg', 1, 291),
(387, '2891b85a.jpg', 1, 289),
(383, '2921b711.jpg', 1, 292),
(385, '2901b78a.jpg', 1, 290),
(381, '2951b66c.jpg', 1, 295),
(382, '2931b6cb.jpg', 1, 293),
(379, '2961b585.jpg', 1, 296),
(380, '2941b60b.jpg', 1, 294),
(377, '2981b4d1.jpg', 1, 298),
(490, 'kimlaymauhealnc30_297_25937.jpg', 1, 297),
(375, '3001b426.jpg', 1, 300),
(376, '2991b475.jpg', 1, 299),
(373, '3041b350.jpg', 1, 304),
(374, '3031b3a1.png', 1, 303),
(371, '3061b263.jpg', 1, 306),
(372, '3051b2f0.jpg', 1, 305),
(369, '3081b1a2.jpeg', 1, 308),
(370, '3071b1ba.jpeg', 1, 307),
(367, '3101b10a.jpg', 1, 310),
(368, '3091b15a.jpeg', 1, 309),
(365, '3121b08c.png', 1, 312),
(366, '3111b0d9.jpg', 1, 311),
(363, '3141b010.png', 1, 314),
(364, '3131b039.jpg', 1, 313),
(361, '3151afbe.jpg', 1, 315),
(362, '3151afc2.png', 2, 315),
(351, '26516e6f.jpg', 1, 265),
(352, '26619d4a.JPG', 1, 266),
(349, '2632d4d4.jpg', 1, 263),
(350, '2642e9c4.jpg', 1, 264),
(347, '2612a43a.jpg', 1, 261),
(348, '2622d2b6.jpg', 1, 262),
(345, '259211de.jpg', 1, 259),
(346, '2602610c.jpg', 1, 260),
(343, '25718f52.jpg', 1, 257),
(344, '25820f18.jpg', 1, 258),
(341, '255188b6.jpg', 1, 255),
(342, '25618bc2.jpg', 1, 256),
(339, '25317138.jpg', 1, 253),
(340, '254172d8.jpg', 1, 254),
(337, '2512aa39.jpg', 1, 251),
(338, '25216daf.jpg', 1, 252),
(335, '24922684.jpg', 1, 249),
(336, '2502a7e1.jpg', 1, 250),
(333, '2472129d.jpg', 1, 247),
(334, '2482248e.jpg', 1, 248),
(331, '24520ca4.jpg', 1, 245),
(332, '246210ea.png', 1, 246),
(328, '2431b8a0.jpg', 1, 243),
(330, '24420b45.jpg', 1, 244),
(325, '242284a7.jpg', 1, 242),
(326, '2302a12e.jpg', 1, 230),
(323, '24027ae4.jpg', 1, 240),
(324, '24127ed5.jpg', 1, 241),
(321, '23825add.jpg', 1, 238),
(322, '23925ed1.jpg', 1, 239),
(319, '23622d39.jpg', 1, 236),
(320, '23725690.jpg', 1, 237),
(316, '23520cc1.jpg', 1, 235),
(318, '20321246.jpg', 1, 203),
(314, '20518c53.jpg', 1, 205),
(315, '20618fb7.jpg', 1, 206),
(311, '22916456.jpg', 1, 229),
(312, '20416a2a.jpg', 1, 204),
(309, '2332c1aa.jpg', 1, 233),
(310, '2342c2c3.jpg', 1, 234),
(307, '2312aceb.jpg', 1, 231),
(308, '2322bfbb.jpg', 1, 232),
(302, '2232c69e.jpg', 1, 223),
(305, '207237b7.jpg', 1, 207),
(300, '2022c145.gif', 1, 202),
(301, '2242c3dc.jpg', 1, 224),
(298, '2222aa91.gif', 1, 222),
(299, '2212ad9c.gif', 1, 221),
(296, '22827890.jpg', 1, 228),
(297, '2202994a.gif', 1, 220),
(294, '22625d81.jpg', 1, 226),
(295, '2272761f.jpg', 1, 227),
(292, '2131b734.jpg', 1, 213),
(293, '22525b36.jpg', 1, 225),
(290, '2141b680.jpg', 1, 214),
(291, '2121b71a.jpg', 1, 212),
(289, '2151b654.jpg', 1, 215),
(288, '21925628.jpg', 1, 219),
(287, '2182535d.jpg', 1, 218),
(286, '21722da1.jpg', 1, 217),
(285, '21622a0a.jpg', 1, 216),
(280, '211209fb.jpg', 1, 211),
(279, '21020982.jpg', 1, 210),
(278, '20920633.jpg', 1, 209),
(277, '208362f4.jpg', 1, 208),
(271, '20134042.jpg', 1, 201),
(270, '20033e7a.jpg', 1, 200),
(269, '19933c8e.jpg', 1, 199),
(268, '19833a46.jpg', 1, 198),
(267, '1973360b.jpg', 1, 197),
(265, '1818422.jpg', 1, 181),
(266, '196322c3.jpg', 1, 196),
(264, '19418854.jpg', 1, 194),
(263, '19317667.jpg', 1, 193),
(262, '1921734f.jpg', 1, 192),
(261, '191170dc.jpg', 1, 191),
(259, '189163f6.jpg', 1, 189),
(260, '19016ef3.jpg', 1, 190),
(257, '18714e22.jpg', 1, 187),
(258, '188160e6.jpg', 1, 188),
(255, '1848690.gif', 1, 184),
(253, '182facd.jpg', 1, 182),
(254, '183fdb4.jpg', 1, 183),
(250, '179cd98.gif', 1, 179),
(251, '180da31.jpg', 1, 180),
(249, '146cbed.gif', 1, 146),
(244, '1741c039.jpg', 1, 174),
(243, '1731bef6.jpg', 1, 173),
(222, '15536032.gif', 1, 155),
(219, '15234b7b.jpg', 1, 152),
(218, '15134526.jpg', 1, 151),
(217, '150340e8.jpg', 1, 150),
(214, '148cf3f.gif', 1, 148),
(216, '144d2c3.gif', 1, 144),
(205, '145382d5.jpg', 1, 145),
(203, '14336f4d.jpg', 1, 143),
(199, '13916c2.jpg', 1, 139),
(197, '137eea.jpg', 1, 137),
(196, '136c49.jpg', 1, 136),
(194, '1348a7.jpg', 1, 134),
(192, '13333798.jpg', 1, 133),
(193, '128602.jpg', 1, 128),
(187, '1261e368.jpg', 1, 126),
(191, '13233676.jpg', 1, 132),
(185, '1241dfc6.jpg', 1, 124),
(186, '1251e287.jpg', 1, 125),
(184, '1231dcc3.jpg', 1, 123),
(174, '1096165.gif', 1, 109),
(179, '1187ad0.gif', 1, 118),
(144, '983704b.gif', 1, 98),
(143, '9736cb5.gif', 1, 97),
(486, 'bomuongvadiatapan_329_29d4f.jpg', 2, 329),
(488, 'tuinhaichonghoc_325_2a90e.jpg', 1, 325),
(489, 'tuinhaichonghoc_325_2a90f.jpg', 3, 325),
(491, 'diuembemaudendo_351_29c65.jpg', 1, 351),
(493, 'diuembemaudendo_351_29dab.jpg', 3, 351),
(496, 'hopchiasua3ngankidsme_343_2ef60.jpg', 2, 343),
(501, 'tuinhaisiliconroithaythe_339_2f3b1.jpg', 3, 339),
(498, 'hopchiasua3ngankidsme_343_2ef63.jpg', 3, 343),
(499, 'hopchiasua3ngankidsme_343_2ef65.jpg', 4, 343),
(502, 'tuinhaisiliconroithaythe_339_2f3b3.jpg', 1, 339),
(504, 'keocatvadamthucan_342_31005.jpg', 1, 342),
(505, 'keocatvadamthucan_342_31006.jpg', 4, 342),
(506, 'keocatvadamthucan_342_31008.jpg', 2, 342),
(508, 'bonhogiotvacocdungthuoc_337_315a1.jpg', 3, 337),
(509, 'bonhogiotvacocdungthuoc_337_315a3.jpg', 1, 337),
(510, 'bonhogiotvacocdungthuoc_337_315a4.jpg', 4, 337),
(511, 'bonhogiotvacocdungthuoc_337_315a6.jpg', 5, 337),
(514, 'tuidulich9mon_335_32012.jpg', 3, 335),
(515, 'tuidulich9mon_335_32013.jpg', 1, 335),
(516, 'tuidulich9mon_335_32015.jpg', 4, 335),
(518, 'bomuongbaonhiet2cai_333_3372b.jpg', 1, 333),
(519, 'bomuongbaonhiet2cai_333_3372d.jpg', 3, 333),
(520, 'bomuongbaonhiet2cai_333_3372e.jpg', 5, 333),
(521, 'bomuongbaonhiet2cai_333_33730.jpg', 4, 333),
(523, 'banchaidanhrangcoluclac_334_34162.jpg', 2, 334),
(524, 'banchaidanhrangcoluclac_334_3416a.jpg', 4, 334),
(525, 'banchaidanhrangcoluclac_334_3416c.jpg', 1, 334),
(528, 'bobatmuongchobe_330_34740.jpg', 3, 330),
(529, 'bobatmuongchobe_330_34742.jpg', 2, 330),
(530, 'thunghiem_352_2a403.jpg', 1, 352),
(531, 'aosomichonam_407_1b3b3.jpg', 1, 407),
(532, 'aosomicotau_408_22c0f.jpg', 1, 408);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_roles`
--

CREATE TABLE IF NOT EXISTS `itnl_roles` (
`id` int(11) NOT NULL,
  `roles` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `publisher` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `creator` int(11) NOT NULL,
  `editor` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Contenu de la table `itnl_roles`
--

INSERT INTO `itnl_roles` (`id`, `roles`, `name`, `publisher`, `status`, `creator`, `editor`) VALUES
(1, 'all', 'Quản trị hệ thống', 1, 1, 1, 1),
(2, '["2"]', 'Đăng bài viết', 0, 1, 1, 1),
(3, '["3"]', 'Seller', 0, 1, 1, 1),
(4, '["3"]', 'Trưởng phòng', 0, 1, 1, 1),
(5, '["3"]', 'Đầu mối', 0, 1, 1, 1),
(6, 'false', 'Cộng tác viên', 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_roles_menus`
--

CREATE TABLE IF NOT EXISTS `itnl_roles_menus` (
`id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `url_path` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_roles_menus`
--

INSERT INTO `itnl_roles_menus` (`id`, `label`, `module`, `url_path`, `status`) VALUES
(1, 'Người dùng', 'auth', '/dashboard/auth', 1),
(2, 'Bài viết', 'news', '/dashboard/news', 1),
(3, 'Sản phẩm', 'products', '/dashboard/products', 1),
(4, 'Trang', 'pages', '/dashboard/pages', 1),
(5, 'Liên hệ', 'contact', '/dashboard/contact', 1),
(6, 'Tải files', 'download', '/dashboard/download', 0),
(7, 'Tranh ảnh', 'gallery', '/dashboard/gallery', 0),
(8, 'Videos', 'videos', '/dashboard/videos', 0),
(9, 'Banner', 'advs', '/dashboard/advs,/dashboard/advs/add,/dashboard/advs/cat', 1),
(10, 'Hỗ trợ trực tuyến', 'supports', '/dashboard/supports', 1),
(11, 'Hệ thống menu', 'menus', '/dashboard/menus', 1),
(12, 'Cấu hình chung', 'configurations', '/dashboard/system_config', 1),
(13, 'Hỏi đáp', 'faq', '/dashboard/faq', 0),
(14, 'Đơn hàng', 'orders', '/dashboard/orders', 1),
(15, 'Khách hàng', 'customers', '/dashboard/customers', 0);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_slug`
--

CREATE TABLE IF NOT EXISTS `itnl_slug` (
`id` int(11) NOT NULL,
  `slug` varchar(1000) NOT NULL,
  `type` int(11) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2145 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_slug`
--

INSERT INTO `itnl_slug` (`id`, `slug`, `type`, `type_id`) VALUES
(2052, 'may-dem-buoc-di-hj325-trang-hong.html', 3, 315),
(2051, 'can-suc-khoe-hn286.html', 3, 314),
(2049, 'can-suc-khoe-hn289-den.html', 3, 312),
(2050, 'can-suc-khoe-hn283.html', 3, 313),
(2048, 'may-do-luong-mo-co-the-hbf375.html', 3, 311),
(2047, 'may-do-luong-mo-co-the-hbf212.html', 3, 310),
(2046, 'dien-cuc-massage-hvjpad.html', 3, 309),
(2045, 'dien-cuc-massage-hvllpad.html', 3, 308),
(2043, 'may-massage-dien-tu-hvf127.html', 3, 306),
(2044, 'may-massage-dien-tu-hvf128.html', 3, 307),
(2042, 'bau-loc-nhiet-ke-omron-90636779.html', 3, 305),
(2041, 'nhiet-ke-dien-tu-mc272l.html', 3, 304),
(2040, 'nhiet-ke-dien-tu-mc246.html', 3, 303),
(2038, 'bau-xong-99567354-neb-kid-set.html', 3, 301),
(2039, 'bau-xong-99567355-neb-kid-set.html', 3, 302),
(2037, 'may-xong-mui-hong-neu22.html', 3, 300),
(2036, 'may-xong-mui-hong-nec25s.html', 3, 299),
(2034, 'kim-lay-mau-healnc30.html', 3, 297),
(2035, 'may-xong-mui-hong-nec802s.html', 3, 298),
(2033, 'may-do-duong-huyet-hgmstp1ab25.html', 3, 296),
(2032, 'vong-bit-do-huyet-ap-bap-tay-medium-cuff.html', 3, 295),
(2031, 'may-do-huyet-ap-bap-tay-ac-adapter.html', 3, 294),
(2030, 'may-do-huyet-ap-bap-tay-hem7300-sieu-cao-cap.html', 3, 293),
(2029, 'coc-uong-nuoc-laica-hi6012.html', 3, 292),
(2028, 'binh-loc-nuoc-than-hoat-tinh-m3m.html', 3, 291),
(2027, 'binh-loc-nuoc-series-3000-laica-j947.html', 3, 290),
(2026, 'binh-loc-nuoc-series-3000-laica-j431j435.html', 3, 289),
(2025, 'binh-loc-nuoc-series-1000-laica-j406.html', 3, 288),
(2024, 'bo-lam-dep-mong-tay-chan-laica-md6044.html', 3, 287),
(2023, 'may-tao-do-am-laica-hi3013.html', 3, 286),
(2022, 'may-tao-do-am-laica-hi3006.html', 3, 285),
(2021, 'may-xong-lam-dep-da-mat-laica-md6062.html', 3, 284),
(2020, 'may-xong-khi-dung-laica-md6026.html', 3, 283),
(2019, 'can-nha-bep-dien-tu-cam-ung-laica-ks1023.html', 3, 282),
(2018, 'can-nha-bep-dien-tu-laica-ks1016.html', 3, 281),
(2017, 'can-dien-tu-suc-khoe-laica-ps6013.html', 3, 280),
(2016, 'can-dien-tu-suc-khoe-laica-ps1016.html', 3, 278),
(2015, 'can-suc-khoe-dien-tu-laica-ps1050.html', 3, 277),
(2014, 'can-suc-khoe-co-hoc-laica-ps2007.html', 3, 276),
(2013, 'nhiet-ke-do-nuoc-tam-dien-tu-laica-th4006.html', 3, 275),
(2012, 'nhiet-ke-hong-ngoai-do-tai-laica-th2002.html', 3, 274),
(2011, 'can-tre-so-sinh-dien-tu-laica-ps3001.html', 3, 273),
(2010, 'can-tre-em-dien-tu-laica-bf2051.html', 3, 272),
(2009, 'may-do-huyet-ap-bap-tay-laica-bm2001.html', 3, 271),
(2008, 'may-do-huyet-ap-dien-tu-co-tay-laica-bm1004.html', 3, 270),
(2007, 'may-do-nong-do-oxy-trong-mau-spo2.html', 3, 269),
(2006, 'bon-massage-chan-lam-nong-nuoc-cai-dat-thoi-gian-co-hn-fs-885.html', 3, 268),
(2005, 'bon-massage-chan-medisabon-massage-chan-giu-nhiet-ecomedna.html', 3, 266),
(2004, 'may-xong-mui-hong-omron-ne-c30.html', 3, 264),
(2003, 'may-xong-mui-hong-omron-ne-c29.html', 3, 263),
(2002, 'may-xong-mui-hong-omron-ne-c28.html', 3, 262),
(2001, 'may-xong-mui-hong-omron-nec801.html', 3, 261),
(2000, 'may-xong-mui-hong-omron-nec802s.html', 3, 260),
(1999, 'may-do-duong-huyet-omron-hgm-111.html', 3, 259),
(1998, 'may-do-duong-huyet-omron-hmg-112.html', 3, 258),
(1997, 'may-do-huyet-ap-co-tay-omron-hem-6221.html', 3, 257),
(1996, 'may-do-huyet-ap-co-tay-omron-hem-6131.html', 3, 256),
(1995, 'may-do-huyet-ap-co-tay-omron-hem-6121.html', 3, 255),
(1994, 'may-do-huyet-ap-omron-hem-7120.html', 3, 254),
(1993, 'may-do-huyet-ap-bap-tay-omron-hem-8712.html', 3, 253),
(1992, 'may-do-huyet-ap-bap-tay-omron-hem4030.html', 3, 252),
(1991, 'may-massage-dien-tu-omron-f128.html', 3, 250),
(1990, 'may-do-luong-mo-co-the-omron-hbf-214.html', 3, 248),
(1989, 'may-hut-sua-breast-pump-flaem.html', 3, 243),
(1988, 'dai-massage-giam-beo-medisana.html', 3, 242),
(1987, 'may-xong-mat-lam-da-mat-medisana.html', 3, 241),
(1986, 'may-cham-soc-mong-chan-tay-medisana.html', 3, 240),
(1985, 'may-xong-mui-hong-condor-medisana.html', 3, 239),
(1984, 'may-xong-mui-hong-ideal-plus-medisana.html', 3, 238),
(1983, 'nhiet-ke-do-tai-tran-medisana-tm-750.html', 3, 237),
(1982, 'nhiet-ke-hong-ngoai-do-tai-medisana-fto.html', 3, 236),
(1981, 'nhiet-ke-do-than-nhiet-tran-medisana-tm-65e.html', 3, 235),
(1980, 'may-do-huyet-ap-bap-tay-bu510.html', 3, 234),
(1979, 'may-do-huyet-ap-tu-dong-omron-hem-7113.html', 3, 232),
(1978, 'may-do-duong-huyet-meditouch.html', 3, 231),
(1977, 'nhiet-ke-hong-ngoai-hannox-wireless.html', 3, 229),
(1976, 'goi-tu-van-suc-khoe-toan-dien-ca-nhan-edoctor.html', 3, 227),
(1975, 'may-massage-rung-ivw-medisana.html', 3, 224),
(1974, 'massage-dam-2-dau-co-hong-ngoai-medisana.html', 3, 223),
(1973, 'may-do-duong-huyet-medisana-meditouch-2.html', 3, 222),
(1972, 'may-do-huyet-ap-bap-tay-medisana-mts.html', 3, 221),
(1971, 'may-do-huyet-ap-co-tay-medisana-hgn.html', 3, 220),
(1970, 'the-hoi-vien-vinmec-child-cho-be-tu-15-ngay-tuoi.html', 3, 215),
(1969, 'may-xong-mui-hong-omron-nec801kd.html', 3, 208),
(1968, 'nhiet-ke-dien-tu-do-tai-omron-th839s.html', 3, 206),
(1967, 'nhiet-ke-dien-tu-do-tran-omron-mc-720.html', 3, 205),
(1966, 'nhiet-ke-do-tai-va-tran-hannox-lct300.html', 3, 204),
(1965, 'may-hut-dich-mui-hannox-me8202.html', 3, 203),
(1963, 'may-do-huyet-ap-omron-jpn1.html', 3, 201),
(1964, 'may-do-huyet-ap-medisana-mtp-plus.html', 3, 202),
(1962, 'may-do-huyet-ap-omron-hem-7130.html', 3, 200),
(1961, 'may-do-huyet-ap-omron-hem-7121.html', 3, 199),
(1960, 'bao-hiem-suc-khoe-gia-dinh-liberty-medicare-m3.html', 3, 98),
(1959, 'bao-hiem-suc-khoe-gia-dinh-liberty-medicare-m4.html', 3, 118),
(1958, 'bao-hiem-suc-khoe-gia-dinh-prevoir-m2.html', 3, 143),
(1957, 'bao-hiem-suc-khoe-thai-san-prevoir-h1.html', 3, 144),
(1956, 'bao-hiem-suc-khoe-gia-dinh-prevoir-h2.html', 3, 145),
(1955, 'bao-hiem-suc-khoe-cho-be-pvi-care.html', 3, 146),
(1954, 'bao-hiem-suc-khoe-thai-san-prevoir-h2.html', 3, 148),
(1953, 'goi-triet-long-vinh-vien-bh-tron-doi-tai-tmv-thuy-quynh.html', 3, 150),
(1952, 'bao-hiem-suc-khoe-cho-bo-me-prevoir.html', 3, 155),
(1951, 'edoctor-the-tu-van-suc-khoe-01-nam.html', 3, 179),
(1950, 'may-suc-ozone-lino-lin8-pro.html', 3, 180),
(1949, 'may-khu-doc-rau-qua-lino-ep9.html', 3, 181),
(1948, 'may-khu-mui-oto.html', 3, 182),
(1947, 'may-xong-khi-dung-omron.html', 3, 188),
(1946, 'may-xong-mui-hong-omron.html', 3, 187),
(1945, 'may-do-huyet-ap-tu-dong-omron.html', 3, 189),
(1944, 'may-do-huyet-ap-omron-hem-7300.html', 3, 196),
(1943, 'may-do-huyet-ap-omron-hem-7320.html', 3, 197),
(1941, 'tu-van-suc-khoe', 4, 34),
(1942, 'may-do-huyet-ap-omron-hem7322.html', 3, 198),
(1940, 'goi-kham-suc-khoe', 4, 19),
(1935, 'may-xong-mui-hong', 4, 35),
(1934, 'may-khu-doc', 4, 33),
(1930, 'the-hoi-vien-vinmec-child', 4, 29),
(1929, 'xac-nghiem-adn', 4, 15),
(1928, 'bao-hiem-khac', 4, 9),
(1926, 'gia-dinh', 4, 6),
(1927, 'chung-cu', 4, 8),
(1925, 'thai-san', 4, 7),
(2053, 'tui-nhai-chong-hoc.html', 3, 325),
(1922, 'tre-em', 4, 5),
(1919, 'cham-soc-da', 4, 18),
(2069, 'tui-nhai-silicone', 4, 60),
(1916, 'sua-tam', 4, 13),
(1915, 'dich-vu-suc-khoe', 4, 37),
(1914, 'my-pham-lam-dep', 4, 10),
(1912, 'nhiet-ke', 4, 23),
(1913, 'thiet-bi-y-te-gia-dinh', 4, 21),
(1911, 'duong-huyet', 4, 16),
(1910, 'may-do-huyet-ap', 4, 25),
(1909, 'san-pham-cho-be', 4, 36),
(1908, 'bao-hiem-suc-khoe', 4, 14),
(2054, 'icy-moo-moo-num-nhai-gia.html', 3, 327),
(2055, 'bo-dung-cu-du-lich-tien-ich-danh-cho-be.html', 3, 328),
(2056, 'bo-muong-va-dia-tap-an.html', 3, 329),
(2057, 'bo-bat-chong-truot-kem-muong-bao-nhiet-kidsme.html', 3, 330),
(2058, 'bo-2-ban-chai-danh-rang-tap-cam-kidsme.html', 3, 331),
(2059, 'bo-2-muong-bao-nhiet-kidsme.html', 3, 333),
(2060, 'ban-chai-danh-rang-co-luc-lac-kidsme.html', 3, 334),
(2061, 'tui-du-lich-9-san-pham-kidsme.html', 3, 335),
(2062, 'bo-cham-soc-mong-cho-be-kidsme.html', 3, 336),
(2063, 'bo-nho-giot-va-coc-dung-thuoc-kidsme.html', 3, 337),
(2064, 'keo-cat-dam-thuc-an-da-nang-kidsme.html', 3, 342),
(2065, 'bo-2-tui-nhai-silicone-roi-thay-the-kidsme.html', 3, 339),
(2066, 'hop-chia-sua-3-ngan-kidsme.html', 3, 343),
(2067, 'dung-cu-nghien-thuc-an-bang-tay-kidsme.html', 3, 341),
(2068, 'tui-nhai-chong-hoc-plus.html', 3, 344),
(2070, 'dung-cu-cho-be-an', 4, 61),
(2071, 'ban-chai-rang', 4, 62),
(2072, 'cham-soc-mong', 4, 63),
(2073, 'bo-tien-ich-du-lich', 4, 64),
(2074, 'dung-cu-nho-thuoc', 4, 65),
(2075, 'medisana-huyet-ap', 4, 66),
(2076, 'omron-huyet-ap', 4, 67),
(2077, 'laica-huyet-ap', 4, 68),
(2078, 'medisana-duong-huyet', 4, 69),
(2079, 'omron-duong-huyet', 4, 70),
(2080, 'omron-nhiet-ke', 4, 71),
(2081, 'medisana-nhiet-ke', 4, 72),
(2082, 'laica-nhiet-ke', 4, 73),
(2083, 'hannox-nhiet-ke', 4, 74),
(2084, 'loai-khac', 4, 75),
(2085, 'may-massage', 4, 76),
(2086, 'cham-soc-mong-tay-chan', 4, 77),
(2087, 'may-xong-mat', 4, 78),
(2088, 'can-suc-khoe', 4, 79),
(2089, 'binh-loc-nuoc', 4, 80),
(2090, 'diu-tre-em', 4, 81),
(2091, 'diu-em-be-mau-nau.html', 3, 349),
(2092, 'diu-em-be-mau-xanh-lo.html', 3, 350),
(2093, 'diu-em-be-mau-den-do.html', 3, 351),
(2094, 'may-tao-do-am', 4, 82),
(2095, 'may-do-co-the', 4, 83),
(2096, 'huong-dan-mua-hang.html', 11, 14),
(2097, 'huong-dan-thanh-toan.html', 11, 15),
(2098, 'lien-he.html', 11, 16),
(2100, 'thanh-thoi-mua-sam.html', 11, 17),
(2101, 'cham-soc-khach-hang.html', 11, 18),
(2102, 'host-keyword.html', 11, 19),
(2103, 'menu-nhe.html', 11, 20),
(2104, 'quang-cao-side-left.html', 11, 21),
(2105, 'thu-nghiem.html', 3, 352),
(2106, 'thoi-trang', 4, 85),
(2107, 'ao-so-mi-cho-nam.html', 3, 407),
(2108, '.html', 3, 408),
(2109, 'suc-khoe', 4, 86),
(2110, 'o-to', 4, 87),
(2111, 'du-lich', 4, 88),
(2112, 'hang-hoa', 4, 89),
(2113, 'chay-no', 4, 90),
(2114, 'tai-nan', 4, 91),
(2115, 'tai-tuc-bhsk', 4, 92),
(2116, 'nhan-tho', 4, 93),
(2117, 'dich-vu-visa', 4, 94),
(2118, 'and', 4, 95),
(2119, '.html-1', 3, 409),
(2120, '.html-1', 3, 411),
(2121, '.html-1', 3, 410),
(2122, '.html-1', 3, 415),
(2123, '.html-1', 3, 416),
(2124, '.html-1', 3, 417),
(2125, '.html-1', 3, 418),
(2126, '.html-1', 3, 419),
(2127, '.html-1', 3, 420),
(2128, '.html-1', 3, 421),
(2129, '.html-1', 3, 422),
(2130, '.html-1', 3, 428),
(2131, 'thu-nghiem-1.html', 1, 28),
(2132, '.html-1', 3, 429),
(2133, '.html-1', 3, 430),
(2134, '.html-1', 3, 432),
(2142, '.html-1', 3, 464),
(2136, '.html-1', 3, 436),
(2137, '.html-1', 3, 437),
(2138, '.html-1', 3, 438),
(2139, '.html-1', 3, 0),
(2143, '.html-1', 3, 493),
(2144, '.html-1', 3, 514);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_supports`
--

CREATE TABLE IF NOT EXISTS `itnl_supports` (
`id` int(11) unsigned NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` tinyint(4) DEFAULT NULL,
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `creator` int(11) NOT NULL,
  `editor` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='User Editable Pages';

--
-- Contenu de la table `itnl_supports`
--

INSERT INTO `itnl_supports` (`id`, `title`, `type`, `content`, `position`, `create_time`, `update_time`, `status`, `lang`, `creator`, `editor`) VALUES
(2, 'Hotline', 3, '097 898 1797', 2, 1435826547, 1444142419, 1, 'vi', 1, 1),
(4, 'Mở cửa', 3, 'Từ Thứ 2 đến Chủ Nhật', 4, 1437324970, 1444142434, 1, 'vi', 1, 1),
(5, 'Giờ giao hàng buổi sáng', 3, '8:00 - 12:00', 5, 1437325080, 1437325202, 1, 'vi', 1, 1),
(6, 'Giờ giao hàng buổi chiều', 3, '13:00 - 21:00', 6, 1437325097, 1444142459, 1, 'vi', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_units`
--

CREATE TABLE IF NOT EXISTS `itnl_units` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `itnl_units`
--

INSERT INTO `itnl_units` (`id`, `name`, `position`, `lang`, `creator`, `editor`) VALUES
(1, 'Hộp', 1, 'vi', 1, 1),
(3, 'Bao', 3, 'vi', 1, 1),
(4, 'Túi', 4, 'vi', 1, 1),
(5, 'Khay', 5, 'vi', 1, 1),
(6, 'Chai', 6, 'vi', 1, 1),
(7, 'Lọ', 7, 'vi', 1, 1),
(10, 'Viên', 8, 'vi', 1, 1),
(11, 'Chiếc', 9, 'vi', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_users`
--

CREATE TABLE IF NOT EXISTS `itnl_users` (
`id` int(11) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `DOB` datetime DEFAULT NULL COMMENT 'Date Of Birth',
  `address` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `tel` varchar(25) DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL COMMENT '	',
  `level` int(11) NOT NULL DEFAULT '0',
  `company` varchar(256) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `active` smallint(1) DEFAULT '1',
  `alias_name` varchar(15) DEFAULT NULL,
  `cities_id` int(11) NOT NULL DEFAULT '1',
  `joined_date` datetime DEFAULT NULL,
  `avatar` varchar(256) DEFAULT NULL,
  `is_openid` tinyint(4) DEFAULT NULL,
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0',
  `trademark_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Contenu de la table `itnl_users`
--

INSERT INTO `itnl_users` (`id`, `fullname`, `DOB`, `address`, `email`, `tel`, `username`, `password`, `level`, `company`, `role_id`, `active`, `alias_name`, `cities_id`, `joined_date`, `avatar`, `is_openid`, `creator`, `editor`, `trademark_id`) VALUES
(1, 'Administrator', NULL, NULL, '', '', 'admin', '0cc175b9c0f1b6a831c399e269772661', 1, NULL, 1, 1, NULL, 1, '2014-11-11 08:37:00', NULL, NULL, 1, 1, 0),
(3, 'Trang kế toán', NULL, NULL, 'trang@ebaohiem.com', '936487846', 'trang', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 3, 1, NULL, 1, '2016-04-25 11:20:15', NULL, NULL, 1, 1, 0),
(4, 'Long', NULL, NULL, '', '', 'Long', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 4, 1, NULL, 1, '2016-04-29 10:43:58', NULL, NULL, 1, 1, 0),
(5, 'Kiên', NULL, NULL, '', '', 'Kiên', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 5, 1, NULL, 1, '2016-04-29 11:12:15', NULL, NULL, 1, 1, 29),
(6, 'thanh', NULL, NULL, '', '', 'thanh', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 5, 1, NULL, 1, '2016-04-29 11:16:19', NULL, NULL, 1, 1, 28),
(7, 'Trung', NULL, NULL, 'trung@yahoo.com', '09646878874', 'trung', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, 6, 1, NULL, 1, '2016-05-06 10:10:21', NULL, NULL, 1, 1, -1);

-- --------------------------------------------------------

--
-- Structure de la table `itnl_videos`
--

CREATE TABLE IF NOT EXISTS `itnl_videos` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` varchar(500) NOT NULL,
  `content` text NOT NULL,
  `cat_id` int(11) NOT NULL,
  `avatar` varchar(500) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `viewed` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `home` tinyint(1) NOT NULL DEFAULT '0',
  `meta_title` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_videos_categories`
--

CREATE TABLE IF NOT EXISTS `itnl_videos_categories` (
`id` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `lang` varchar(20) DEFAULT 'vi',
  `status` tinyint(1) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `creator` int(11) NOT NULL DEFAULT '0',
  `editor` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `itnl_videos_items`
--

CREATE TABLE IF NOT EXISTS `itnl_videos_items` (
`id` int(11) NOT NULL,
  `url` varchar(500) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `youtube_video_id` varchar(255) NOT NULL,
  `summary` varchar(500) NOT NULL,
  `content` text NOT NULL,
  `position` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `itnl_advs`
--
ALTER TABLE `itnl_advs`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_advs_categories`
--
ALTER TABLE `itnl_advs_categories`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_advs_click`
--
ALTER TABLE `itnl_advs_click`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_ci_sessions`
--
ALTER TABLE `itnl_ci_sessions`
 ADD PRIMARY KEY (`session_id`), ADD KEY `last_activity_idx` (`last_activity`);

--
-- Index pour la table `itnl_clbh`
--
ALTER TABLE `itnl_clbh`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_configuration`
--
ALTER TABLE `itnl_configuration`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_contact`
--
ALTER TABLE `itnl_contact`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_customers`
--
ALTER TABLE `itnl_customers`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_download`
--
ALTER TABLE `itnl_download`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_download_categories`
--
ALTER TABLE `itnl_download_categories`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_emails`
--
ALTER TABLE `itnl_emails`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_faq`
--
ALTER TABLE `itnl_faq`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_faq_categories`
--
ALTER TABLE `itnl_faq_categories`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_faq_professionals`
--
ALTER TABLE `itnl_faq_professionals`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_gallery`
--
ALTER TABLE `itnl_gallery`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_gallery_categories`
--
ALTER TABLE `itnl_gallery_categories`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_gallery_images`
--
ALTER TABLE `itnl_gallery_images`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_menus`
--
ALTER TABLE `itnl_menus`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_menus_categories`
--
ALTER TABLE `itnl_menus_categories`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_news`
--
ALTER TABLE `itnl_news`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_news_categories` (`cat_id`);

--
-- Index pour la table `itnl_news_categories`
--
ALTER TABLE `itnl_news_categories`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_orders`
--
ALTER TABLE `itnl_orders`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_pages`
--
ALTER TABLE `itnl_pages`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_products`
--
ALTER TABLE `itnl_products`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_products_users` (`users_id`), ADD KEY `fk_products_categories` (`categories_id`), ADD FULLTEXT KEY `product_name` (`product_name`), ADD FULLTEXT KEY `product_name_2` (`product_name`);

--
-- Index pour la table `itnl_products_categories`
--
ALTER TABLE `itnl_products_categories`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_products_color`
--
ALTER TABLE `itnl_products_color`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_products_coupon`
--
ALTER TABLE `itnl_products_coupon`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_products_coupon_item`
--
ALTER TABLE `itnl_products_coupon_item`
 ADD KEY `code` (`code`), ADD KEY `coupon_id` (`coupon_id`);

--
-- Index pour la table `itnl_products_material`
--
ALTER TABLE `itnl_products_material`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_products_origin`
--
ALTER TABLE `itnl_products_origin`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_products_size`
--
ALTER TABLE `itnl_products_size`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_products_state`
--
ALTER TABLE `itnl_products_state`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_products_style`
--
ALTER TABLE `itnl_products_style`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_products_trademark`
--
ALTER TABLE `itnl_products_trademark`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_product_images`
--
ALTER TABLE `itnl_product_images`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_product_images_products` (`products_id`);

--
-- Index pour la table `itnl_roles`
--
ALTER TABLE `itnl_roles`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_roles_menus`
--
ALTER TABLE `itnl_roles_menus`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_slug`
--
ALTER TABLE `itnl_slug`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_supports`
--
ALTER TABLE `itnl_supports`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_units`
--
ALTER TABLE `itnl_units`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_users`
--
ALTER TABLE `itnl_users`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_videos`
--
ALTER TABLE `itnl_videos`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_videos_categories`
--
ALTER TABLE `itnl_videos_categories`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `itnl_videos_items`
--
ALTER TABLE `itnl_videos_items`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `itnl_advs`
--
ALTER TABLE `itnl_advs`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT pour la table `itnl_advs_categories`
--
ALTER TABLE `itnl_advs_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `itnl_advs_click`
--
ALTER TABLE `itnl_advs_click`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_clbh`
--
ALTER TABLE `itnl_clbh`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_configuration`
--
ALTER TABLE `itnl_configuration`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `itnl_contact`
--
ALTER TABLE `itnl_contact`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `itnl_customers`
--
ALTER TABLE `itnl_customers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ma KH',AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `itnl_download`
--
ALTER TABLE `itnl_download`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_download_categories`
--
ALTER TABLE `itnl_download_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_emails`
--
ALTER TABLE `itnl_emails`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pour la table `itnl_faq`
--
ALTER TABLE `itnl_faq`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_faq_categories`
--
ALTER TABLE `itnl_faq_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_faq_professionals`
--
ALTER TABLE `itnl_faq_professionals`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_gallery`
--
ALTER TABLE `itnl_gallery`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_gallery_categories`
--
ALTER TABLE `itnl_gallery_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_gallery_images`
--
ALTER TABLE `itnl_gallery_images`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_menus`
--
ALTER TABLE `itnl_menus`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=906;
--
-- AUTO_INCREMENT pour la table `itnl_menus_categories`
--
ALTER TABLE `itnl_menus_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `itnl_news`
--
ALTER TABLE `itnl_news`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pour la table `itnl_news_categories`
--
ALTER TABLE `itnl_news_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `itnl_orders`
--
ALTER TABLE `itnl_orders`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT pour la table `itnl_pages`
--
ALTER TABLE `itnl_pages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `itnl_products`
--
ALTER TABLE `itnl_products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=517;
--
-- AUTO_INCREMENT pour la table `itnl_products_categories`
--
ALTER TABLE `itnl_products_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT pour la table `itnl_products_color`
--
ALTER TABLE `itnl_products_color`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_products_coupon`
--
ALTER TABLE `itnl_products_coupon`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `itnl_products_material`
--
ALTER TABLE `itnl_products_material`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `itnl_products_origin`
--
ALTER TABLE `itnl_products_origin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `itnl_products_size`
--
ALTER TABLE `itnl_products_size`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `itnl_products_state`
--
ALTER TABLE `itnl_products_state`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `itnl_products_style`
--
ALTER TABLE `itnl_products_style`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `itnl_products_trademark`
--
ALTER TABLE `itnl_products_trademark`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT pour la table `itnl_product_images`
--
ALTER TABLE `itnl_product_images`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=533;
--
-- AUTO_INCREMENT pour la table `itnl_roles`
--
ALTER TABLE `itnl_roles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `itnl_roles_menus`
--
ALTER TABLE `itnl_roles_menus`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `itnl_slug`
--
ALTER TABLE `itnl_slug`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2145;
--
-- AUTO_INCREMENT pour la table `itnl_supports`
--
ALTER TABLE `itnl_supports`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `itnl_units`
--
ALTER TABLE `itnl_units`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `itnl_users`
--
ALTER TABLE `itnl_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `itnl_videos`
--
ALTER TABLE `itnl_videos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_videos_categories`
--
ALTER TABLE `itnl_videos_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `itnl_videos_items`
--
ALTER TABLE `itnl_videos_items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
